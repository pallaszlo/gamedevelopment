package com.putzi.game.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;

import com.putzi.game.PutziMain;
import com.putzi.util.Constants;


public class DesktopLauncher {


    // TAG for log messages
    private static final String TAG = DesktopLauncher.class.getName();


    /**
     * Starting point for the whole application,
     * creating a new application window and a new desktop application,
     * defining the viewport and
     * defining if and how textures have to be packed
     */
    public static void main(String[] args) {

        // Should Textures be repackt before starting the Game?
        boolean rebuildAtlas = Constants.REPACK_TEXTURES;

        // Should then every Texture Outline be drawn?
        boolean drawDebugOutline = false;

        if (rebuildAtlas) {

            // Settings for packaging textures
            Settings settings = new Settings();
            settings.maxWidth = 4096;
            settings.maxHeight = 4096;
            settings.duplicatePadding = false;
            settings.debug = drawDebugOutline;

            // Package Textures (Source- and target folder, package name)
            TexturePacker.process(settings, "../../desktop/assets-raw/images", "../assets/images", "putzi");
            TexturePacker.process(settings, "../../desktop/assets-raw/images-ui", "../assets/images", "putzi-ui");
        }

        // Configure Application Window
        LwjglApplicationConfiguration applicationConfiguration = new LwjglApplicationConfiguration();

        if (Constants.FULLSCREEN) {
            DisplayMode displayMode = LwjglApplicationConfiguration.getDesktopDisplayMode();
            applicationConfiguration.setFromDisplayMode(displayMode);
        } else {
            applicationConfiguration.width = 1600;
            applicationConfiguration.height = 900;
        }

        applicationConfiguration.addIcon("images/icon.png", Files.FileType.Internal);


        // Create new Desktopapplication (Main_Class, Configuration)
        new LwjglApplication(new PutziMain(), applicationConfiguration);

    }

}