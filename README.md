# README #

Putzi is an 2D-Arcade game based on java and libGDX.

### Changelog/Version ###

* Version 1.0

### How do I get set up? ###

* to be continued

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Jens Gäbeler, 
* Nils Kretschmer, 
* László Pál, 
* Jost Schmithals

### Bugs / Probs ###

#### Bei Update von Android 2.3.3 auf 3.0.0 ####

Es gibt es Probleme, da die von Android Studio 3.0 als Standard verwendete Gradle Version 
noch nicht von libGDX unterstützt wird.

https://stackoverflow.com/questions/46975883/error2-0-plugin-with-id-jetty-not-found

#### Assets werden nicht gefunden ####

Die Assets werden im Android Subprojekt gespeichert und daher beim Start der Desktop Application nicht gefunden.
Dazu kann das Working Directory entsprechend geändert werden.

https://stackoverflow.com/questions/28433012/how-do-i-link-assets-from-android-to-desktop-in-libgdx 
