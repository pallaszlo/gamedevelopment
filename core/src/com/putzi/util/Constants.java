package com.putzi.util;


import com.badlogic.gdx.math.Vector2;

/**
 * Constants for usage in full project
 */
public class Constants {

    // Screen/Viewport
    /** If set to true, the game will start in Fullscreen-mode and without title bar */
    public static final boolean FULLSCREEN = true;
    /** The horizontal viewport width */
    public static final float VIEWPORT_WIDTH = 1920;
    /** The vertical viewport hight */
    public static final float VIEWPORT_HEIGHT = 1080;
    /** The viewport width of the GUI - default should be the same like VIEWPORT_WIDTH. */
    public static final float VIEWPORT_GUI_WIDTH = 1920;
    /** The viewport height of the GUI - default should be the same like VIEWPORT_HEIGHT. */
    public static final float VIEWPORT_GUI_HEIGHT = 1080;

    // Intro Animations (MenuScreen)
    /** The duration of the slice animations in the intro */
    public static final float INTRO_SLICE_DURATION = 0.5f;
    /** The number of slices used for the intro animation */
    public static final int INTRO_SLICE_NUMBER = 20;
    /** Delay of the intro animation */
    private static final float INTRO_START_DELAY = 0.1f;
    /** Value representing the Delay of falling dirt number 1 */
    public static final float INTRO_DIRT_1_DELAY = INTRO_START_DELAY + 2.0f;
    /** Value representing the Delay of falling dirt number 2 */
    public static final float INTRO_DIRT_2_DELAY = INTRO_START_DELAY + 2.7f;
    /** Value representing the Delay of falling dirt number 3 */
    public static final float INTRO_DIRT_3_DELAY = INTRO_START_DELAY + 3.0f;
    /** Value representing the Delay of falling dirt number 4 */
    public static final float INTRO_DIRT_4_DELAY = INTRO_START_DELAY + 3.2f;
    /** Value representing the Delay of falling dirt number 5 */
    public static final float INTRO_DIRT_5_DELAY = INTRO_START_DELAY + 3.5f;
    /** Value representing the Delay of falling dirt number 6 */
    public static final float INTRO_DIRT_6_DELAY = INTRO_START_DELAY + 6.5f;
    /** Value representing the Delay of falling dirt number 7 */
    public static final float INTRO_DIRT_7_DELAY = INTRO_START_DELAY + 6.7f;
    /** The speed of dirt falling down at intro animation */
    public static final float INTRO_DIRT_SPEED = 0.3f;

    /** The delayed time for putzi in the intro animation */
    public static final float INTRO_PUTZI_DELAY = INTRO_START_DELAY + 4.0f;
    /** The speed of putzis intro animation */
    public static final float INTRO_PUTZI_SPEED = 2.0f;
    /** Delay of Menu Control Buttons in the intro animation */
    public static final float INTRO_MIDDLE_BUTTONS_DELAY = INTRO_START_DELAY + 8.1f;
    /** Delay of load player/snew player buttons in the intro animation */
    public static final float INTRO_LEFT_BUTTONS_DELAY = INTRO_START_DELAY + 9.1f;
    /** The speed of all button animations in the intro animation */
    public static final float INTRO_BUTTONS_SPEED = 0.75f;

    // Camera
    /** The speed of the in-game camera */
    public static final float CAMERA_MOVE_SPEED = 5.0f;
    /** The acceleration of the camera in-game. Controls how smooth the camera moves. */
    public static final float CAMERA_MOVE_SPEED_ACCELERATION = 3.0f;
    /** The zoom speed of the in-game camera. */
    public static final float CAMERA_ZOOM_SPEED = 0.05f;
    /** The zoom acceleration of the in-game camera. Higher values result in smooth zooms. */
    public static final float CAMERA_ZOOM_SPEED_ACCELERATION = 5.0f;

    // Levels
    /** The width of the Level. Used for Shark creation and Level calculation. */
    static final float LEVEL_MAP_WIDTH = 80.0f;
    /** The height of the Level. Used for Shark creation and Level calculation */
    public static final float LEVEL_MAP_HEIGHT = 48.0f;
    /** Change this to start on a different level (development usage). */
    public static final int STARTING_LEVEL = 1;
    /** Change this if new levels are created. */
    public static final int MAX_LEVEL_NUMBER = 3;
    /** Default is 1. After min value is lost, the game ends. */
    public static final int MIN_PUTZI_LIVES = 1;
    /** Putzis maximum lives at game start. */
    public static final int PUTZI_LIVES_START = 3;
    /** The value of the highscore update after completing a level successfully .*/
    public static final int LEVEL_COMPLETED_BONUS = 1000;

    // Putzi
    /** A vector representing Putzi's maximum speed used for rendering */
    public static final Vector2 PUTZI_MAX_SPEED = new Vector2(2.0f, 2.0f);
    /** A vector for rendering Putzi's movements with friction. */
    public static final Vector2 PUTZI_FRICTION = new Vector2(1.0f, 1.0f);
    /** A vector for rendering Putzi's movements with acceleration. */
    public static final Vector2 PUTZI_ACCELERATION = new Vector2(2.0f, 2.0f);
    /** The speed of Putzi's rotation animation */
    public static final float PUTZI_ROTATION_SPEED = 50;
    /** The frame duration of Putzi's slow swim movements */
    public static final float PUTZI_LONG_FRAMEDURATION = 0.05f;
    /** The frame duration of Putzi's fast swim movements */
    public static final float PUTZI_SHORT_FRAMEDURATION = 0.03f;
    /** The maximum inner rotation of Putzi. */
    public static final float PUTZI_ROTATION_INNER_MAX_ANGLE = 30;
    /** The minimum inner rotation of Putzi. */
    public static final float PUTZI_ROTATION_INNER_MIN_ANGLE = -30;
    /** The maximum outer rotation of Putzi. */
    public static final float PUTZI_ROTATION_OUTER_MAX_ANGLE = 60;
    /** The minimum outer rotation of Putzi. */
    public static final float PUTZI_ROTATION_OUTER_MIN_ANGLE = -60;
    /** Width can be changed to control collision with Putzi's mouth. */
    static final float PUTZI_MOUTH_WIDTH = 0.4f;
    /** Height can be changed to control collision with Putzi's mouth. */
    static final float PUTZI_MOUTH_HEIGHT = 0.4f;
    /** Radius can be changed to control collision with Putzi's mouth. */
    static final float PUTZI_MOUTH_RADIUS = 0.6f;
    /** Smaller Values result in faster oscillation. */
    public static final double PUTZI_WAVE_FREQ = 500.0;
    /** Higher values result in more upward trend. */
    public static final double PUTZI_WAVE_HEIGHT = 0.2;

    // Sharks
    /** Sharks are moving with random speeds from min to max velocity */
    public static final float SHARK_MIN_VELOCITY = 2.0f;
    /** Sharks are moving with random speeds from min to max velocity */
    public static final float SHARK_MAX_VELOCITY = 5.0f;
    /** Is used for the free-camera-bonus-item slow motion effect. */
    public static final float SLOW_MOTION_FACTOR = 0.15f;
    /** x-Position of starting sharks from left side */
    public static final float LEFT_SHARK_STARTING_POSITION_X = -6;
    /** x-Position of starting sharks from right side */
    public static final float RIGHT_SHARK_STARTING_POSITION_X = LEVEL_MAP_WIDTH;
    /** If Sharks are outside this Range, they will be reset **/
    public static final int VALID_SHARK_RANGE_MIN = -6;
    /** If Sharks are outside this Range, they will be reset **/
    public static final int VALID_SHARK_RANGE_MAX = (int) LEVEL_MAP_WIDTH;
    /** Number of Sharks in the levels. */
    public static final int[] NUMBER_OF_SHARKS = {5, 10, 20};
    /** Change this to control level difficulty. */
    public static final int MIN_LEVEL_FOR_SHARKS_FROM_BOTH_DIRECTIONS = 1;

    // Assets
    /** Repack textures before starting the application (only needed if images have changed) */
    public static final boolean REPACK_TEXTURES = false;
    /** Textures of Sharks, Putzi, Rocks, Items. */
    public static final String TEXTURE_ATLAS_OBJECTS = "images/putzi.atlas";
    /** Textures of MenuScreen (Putzi, Logo, Background, Dirt, Buttons..) */
    public static final String TEXTURE_ATLAS_UI = "images/putzi-uineup.atlas";
    /** Textures for windows and buttons in the menuscreen. */
    public static final String TEXTURE_ATLAS_LIBGDX_UI = "images/uiskin.atlas";
    /** Textures used for the UI-elements in the option window */
    public static final String TEXTURE_ATLAS_WINDOW_UI = "images/OptionWindow/windows.txt";
    /** Stored values as .json for option window ui rendering. */
    public static final String SKIN_WINDOW_UI = "images/OptionWindow/uiWindowsSkin.json";
    /** JSON-representation of window-ui skins. */
    public static final String SKIN_LIBGDX_UI = "images/uiskin.json";
    /** JSON-representation of main menu skins. */
    public static final String SKIN_PUTZI_UI = "images/putzi-ui.json";
    /** JSON-representation of buttons used for the menus */
    public static final String SKIN_BUTTONS_UI = "images/buttons/buttons-ui.json";
    /** Atlas file of buttons used for the menus */
    public static final String TEXTURE_BUTTONS = "images/buttons/buttons.atlas";

    // Miscellaneous
    /** Controls the animation after the game has ended. */
    public static final float TIME_DELAY_GAME_FINISHED = 5;
    /** Controls the animation after the level has ended. */
    public static final float TIME_DELAY_LEVEL_FINISHED = TIME_DELAY_GAME_FINISHED;
    /** Highscore value increment when collecting a shell. */
    public static final int SHELL_SCORE_INCREMENT = 500;
    /** Highscore value decrement when bitten by shark. */
    public static final int SHARK_BITE_SCORE_DECREMENT = -1000;
    /** Maximum time to speed up putzi after collecting a snail.*/
    public static final float POWER_BOOST_TIME = 2;
    /** The time of using a free camera before transition.*/
    public static final float PRE_TRANSITION_FREE_CAMERA_TIME = 5;
    /** The time of using a free camera after consuming a crab.*/
    public static final float CRAB_COLLECTION_FREE_CAMERA_TIME = 10;
    /** The time to show an animation when biten by a shark.*/
    public static final float BITE_ANIMATE_TIME = 0.3f;
    /** Time between wall collision Sounds. */
    static final float WALL_COLLISION_TIMER = 0.5f;
    /** The name of the local preferences file. */
    static final String PREFERENCES = "putzi.prefs";
    /** path to monochrome shader vertex. */
    public static final String SHADER_MONOCHROME_VERTEX = "shaders/monochrome.vs";
    /** path to monochrome fragment. */
    public static final String SHADER_MONOCHROME_FRAGMENT = "shaders/monochrome.fs";
    /** Live updates lifecycle on do rebuild */
    public static final float DEBUG_REBUILD_INTERVAL = 5.0f;
    /** static String for overall interrupted Exception Errors. */
    public static final String INTERRUPTED_EXCEPTION_ERROR = "InterruptedException Error: ";

    // Database
    /** Set the Timeout for the MongoDB-Server Response. */
    public static final int SERVER_TIMEOUT = 5000;

    public static final int HEARTBEAT_FREQUENCY = 2000;
}
