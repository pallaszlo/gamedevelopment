package com.putzi.util;

import com.badlogic.gdx.Gdx;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;

import com.mongodb.event.ServerHeartbeatFailedEvent;
import com.mongodb.event.ServerHeartbeatStartedEvent;
import com.mongodb.event.ServerHeartbeatSucceededEvent;
import com.mongodb.event.ServerMonitorListener;

import org.bson.Document;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * DatabaseConnector Singleton to connect to mongoDB.
 *
 * Provides some basic methods to connect, disconnect and to get
 * the users collection from the database
 *
 */

public class DatabaseConnector {

    private static final String TAG = DatabaseConnector.class.getName();
    /** The instance of the Singleton DatabaseConnector */
    public static final DatabaseConnector instance = new DatabaseConnector();
    /** Signals if the database is connected or not */
    private boolean connected = false;
    /** The mongoClient instance for the MongoDB */
    private MongoClient mongoClient;
    /** The MongoDB's pw */
    private String pw;
    /** The MongoDB-Server URL. */
    private String url;
    /** The mongoDB-User */
    private String user;
    /** The userCollection for the UserManagement */
    private MongoCollection<Document> userCollection;

    /**
     * Singleton.
     *
     * Instantiate credentials and DatabaseConnector itself.
     *
     */
    private DatabaseConnector() {
        url = "h2725643.stratoserver.net";
        pw = "putzi";
        user = "putziUser";
        Logger mongoLogger = Logger.getLogger( "org.mongodb.driver" );
        mongoLogger.setLevel(Level.SEVERE); // e.g. or Log.WARNING, etc.
    }

    /**
     * Connects to the MongoDB at the given url.
     */
    public void connectToMongoDB() {
        MongoCredential credential = MongoCredential.createCredential(user,
                "putzi", pw.toCharArray());
        try {
            ServerAddress address = new ServerAddress(url);
            int timeout = Constants.SERVER_TIMEOUT;
            int frequency = Constants.HEARTBEAT_FREQUENCY;
            MongoClientOptions options = MongoClientOptions.builder()
                    .connectTimeout(timeout)
                    .serverSelectionTimeout(timeout)
                    .heartbeatSocketTimeout(timeout)
                    .heartbeatFrequency(frequency)
                    .heartbeatConnectTimeout(timeout)
                    .addServerMonitorListener(new ServerMonitorListener() {
                        @Override
                        public void serverHearbeatStarted(ServerHeartbeatStartedEvent event) {
                        }

                        @Override
                        public void serverHeartbeatSucceeded(ServerHeartbeatSucceededEvent event) {
                            connected = true;
                        }

                        @Override
                        public void serverHeartbeatFailed(ServerHeartbeatFailedEvent event) {
                            Gdx.app.log(TAG, "Connection to Server failed");
                            connected = false;
                        }
                    })
                    .build();
            mongoClient = new MongoClient(address, credential, options);
            setUserCollection();
        } catch (Exception io) {
            Gdx.app.log(TAG, "No Connection pssible");
            connected = false;
        }
    }

    /**
     * Closes all connections to MongoDB.
     */
    void disconnectFromMongoDB() {
        if (connected) {
            mongoClient.close();
            Gdx.app.log(TAG, "Database Connection closed.");
        }
    }

    /**
     * Returns the (hard-coded) userCollection of the game.
     *
     * @return The userCollection of the game
     */
    MongoCollection<Document> getUserCollection() {
        return userCollection;
    }

    /**
     * Sets the Collection of the Database to "users"
     */
    private void setUserCollection() {
        this.userCollection = mongoClient.getDatabase("putzi").getCollection("usersNewApi");
    }

    /**
     * Signals if the Database is successfully connected.
     *
     * @return true if connected, false if not.
     */
    public boolean isConnected() {
        return connected;
    }
}
