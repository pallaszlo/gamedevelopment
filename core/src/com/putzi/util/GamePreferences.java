package com.putzi.util;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.MathUtils;


/**
 * This class offers functionality to save and load from preferences files. Either from Database
 * or from a local preferences file.
 */

public class GamePreferences {

	/** A Tag for Log-Messages */
	public static final String TAG = GamePreferences.class.getName();
	/** The instance of the class (because this class is a singelton) */
	public static final GamePreferences instance = new GamePreferences();
	/** Value represents the sound setting of the game (on/off) */
	public boolean sound;
	/** Value represents the music setting of the game (on/off) */
	public boolean music;
	/** Value represents the sounds volume */
	public double volSound;
	/** Value represents the music volume */
	public double volMusic;
	/** An int value representing the skin of putzi */
	public int charSkin;
	/** Indicates wether to show Fps Counter or not */
	public boolean showFpsCounter;
	/** Indicates wether to use a monochrome (greyscale) shader or not */
	public boolean useMonochromeShader;
	/** The local preferences File itself */
	private Preferences preferencesFile;

	/**
	 * Singleton GamePreferences
	 */
	private GamePreferences () {
		preferencesFile = Gdx.app.getPreferences(Constants.PREFERENCES);
	}

	/**
	 * Loads local preferences (or default values)
	 */
	public void loadPreferences () {
		setPreferencesFromFile();
	}

	/**
	 * Saves Preferences to File and, if possible, to Database
	 */
	public void savePreferences () {
		Gdx.app.log(TAG, "Saving preferences to file.");
		savePreferencesToFile();
		try {
			savePreferencesToDatabase();
		} catch (Exception e) {
			Gdx.app.log(TAG, "Couldn't save preferences to database.");
		}
	}

	/**
	 * Sets the Preferences from the Database.
	 * The values are null if the user doesn't exist!
	 *
	 */
	public void setPreferencesFromDatabase() {

		Gdx.app.log(TAG, "Set preferences from database ... ");

		UserManagement usrMgmt = UserManagement.instance;

		sound = ((Boolean) usrMgmt.getPreference("sound"));
		music = (Boolean) usrMgmt.getPreference("music");

		volSound = (Double) usrMgmt.getPreference("volSound");
		volMusic = (Double) usrMgmt.getPreference("volMusic");
		charSkin = (Integer) usrMgmt.getPreference("charSkin");

		showFpsCounter = (Boolean) usrMgmt.getPreference("showFpsCounter");
		useMonochromeShader = (Boolean) usrMgmt.getPreference("useMonochromeShader");
	}

	/**
	 * Sets the Preferences from the (local) Preferences File
	 */
	private void setPreferencesFromFile () {
		Gdx.app.log(TAG, "Load preferences from file or use default settings.");

		sound = preferencesFile.getBoolean("sound", true);
		music = preferencesFile.getBoolean("music", true);

		volSound = MathUtils.clamp(preferencesFile.getFloat("volSound", 1), 0.0f, 1.0f);
		volMusic = MathUtils.clamp(preferencesFile.getFloat("volMusic", 0.05f), 0.0f, 1.0f);
		charSkin = MathUtils.clamp(preferencesFile.getInteger("charSkin", 1), 0, 2);

		showFpsCounter = preferencesFile.getBoolean("showFpsCounter", true);
		useMonochromeShader = preferencesFile.getBoolean("useMonochromeShader", false);
	}

	/**
	 * Saves the Preferences to the Database
	 */
	private void savePreferencesToDatabase() {

		Gdx.app.log(TAG, "Try to save preferences to database.");
		DatabaseConnector dbConnector = DatabaseConnector.instance;
		if (dbConnector.isConnected()) {
			UserManagement usrMgmt = UserManagement.instance;

			usrMgmt.setPreference("sound", sound);
			usrMgmt.setPreference("volSound", volSound);
			usrMgmt.setPreference("music", music);
			usrMgmt.setPreference("volMusic", volMusic);
			usrMgmt.setPreference("charSkin", charSkin);
			usrMgmt.setPreference("showFpsCounter", showFpsCounter);
			usrMgmt.setPreference("useMonochromeShader", useMonochromeShader);
		} else {
			Gdx.app.log(TAG, "Couldn't save preferences to database.");
		}
	}

	/**
	 * Saves the current values to the local preferences file
	 */
	private void savePreferencesToFile () {

		// Voreinstellungen speichern

		preferencesFile.putBoolean("sound", sound);
		preferencesFile.putBoolean("music", music);
		preferencesFile.putFloat("volSound", (float) volSound);
		preferencesFile.putFloat("volMusic", (float) volMusic);
		preferencesFile.putInteger("charSkin", charSkin);
		preferencesFile.putBoolean("showFpsCounter", showFpsCounter);
		preferencesFile.putBoolean("useMonochromeShader", useMonochromeShader);

		preferencesFile.flush();

	}
}





