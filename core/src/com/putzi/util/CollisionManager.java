package com.putzi.util;


import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.putzi.game.Assets;
import com.putzi.game.Level;
import com.putzi.game.WorldController;
import com.putzi.game.objects.Crab;
import com.putzi.game.objects.Dirt;
import com.putzi.game.objects.Putzi;
import com.putzi.game.objects.Sharks;
import com.putzi.game.objects.Shell;
import com.putzi.game.objects.Snail;
import com.putzi.game.objects.Wall;


/**
 * Creates Collision Boxes around objects and manage Overlapping (Collisions) of Boxes
 */
public class CollisionManager {


    private WorldController worldController;
    private Level level;

    private Array<Boolean> notYetTouchedShark = new Array();
    private Array<Boolean> putziIsOutsideOfSharkHead = new Array();

    private Putzi putzi;

    private float putziPositionX;
    private float putziPositionY;
    private float putziWidth;
    private float putziHeight;

    private float putziMouthWidth;
    private float putziMouthHeight;
    private float radiusOfMouthPath;
    private float putziMouthOffsetX;
    private float putziMouthOffsetY;
    private float viewDirectionAngleOffset;
    private float putziMouthAngleOffset;
    private float putziMouthAngleRadians;

    private Rectangle putziBoundaries;
    private Rectangle putziMouthBoundaries;

    private Rectangle sharkHeadBoundaries;
    private Rectangle sharkBodyBoundaries;

    private Rectangle shellBoundaries;
    private Rectangle snailBoundaries;
    private Rectangle crabBoundaries;
    private Rectangle dirtBoundaries;
    private Rectangle wallBoundaries;

    private float distancePutziWallTop;
    private float distancePutziWallBottom;
    private float distancePutziWallLeft;
    private float distancePutziWallRight;

    /**
     * The Collision Manager generates Boundaries for each object
     * and tests for different conditions if these boundaries are overlapping
     * The world controller calls the collision manager for each level
     * @param worldController passes itself over when calling the collision manager
     * @param level the selected level
     */
    public CollisionManager(WorldController worldController, Level level) {

        this.worldController = worldController;
        this.level = level;

        init();
    }


    /**
     * Initialize variables and calculate values for collision detection.
     */
    private void init() {

        putzi = level.putzi;

        putziWidth = putzi.dimension.x;
        putziHeight = putzi.dimension.y;

        putziMouthWidth = Constants.PUTZI_MOUTH_WIDTH;
        putziMouthHeight = Constants.PUTZI_MOUTH_HEIGHT;
        radiusOfMouthPath = Constants.PUTZI_MOUTH_RADIUS;


        for (int i = 0; i < level.getNumberOfSharks(); i++) {
            notYetTouchedShark.add(true);
            putziIsOutsideOfSharkHead.add(true);
        }

        putziBoundaries = new Rectangle(
                0,
                0,
                putziWidth,
                putziHeight
        );

        putziMouthBoundaries = new Rectangle(
                0,
                0,
                putziMouthWidth,
                putziMouthHeight
        );

        sharkHeadBoundaries = new Rectangle(
                0,
                0,
                1.0f,
                0.7f
        );

        sharkBodyBoundaries = new Rectangle(
                0,
                0,
                4.0f,
                2.0f
        );

        shellBoundaries = new Rectangle(
                0,
                0,
                0.6f,
                0.6f
        );

        snailBoundaries = new Rectangle(
                0,
                0,
                0.6f,
                0.6f
        );

        crabBoundaries = new Rectangle(
                0,
                0,
                0.6f,
                0.6f
        );

        dirtBoundaries = new Rectangle(
                0,
                0,
                1.4f,
                1.4f
        );

        wallBoundaries = new Rectangle(
                0,
                0,
                1,
                1
        );

    }


    /**
     * Main method for collision detection. Tests for collisions with walls, sharks, seafood and dirt.
     */
    public void testCollisions() {


        // Putzis actual position
        putziPositionX = putzi.position.x;
        putziPositionY = putzi.position.y;


        // adjust Bounding Rectangle for putzi
        putziBoundaries.setX(putziPositionX);
        putziBoundaries.setY(putziPositionY);


        // Adjust bounding rectangle for putzis mouth

        // case distinction necessary for putzi rotation

        viewDirectionAngleOffset = putzi.viewDirection == Putzi.VIEW_DIRECTION.LEFT ? 180 : 0;
        putziMouthAngleOffset = putzi.viewDirection == Putzi.VIEW_DIRECTION.LEFT ? 15 : -15;

        putziMouthAngleRadians = (putzi.rotation + viewDirectionAngleOffset + putziMouthAngleOffset) * MathUtils.degreesToRadians;

        putziMouthOffsetX = 0.5f * (putziWidth - putziMouthWidth);
        putziMouthOffsetY = 0.5f * (putziHeight - putziMouthHeight);

        putziMouthBoundaries.setX(putziPositionX + putziMouthOffsetX + radiusOfMouthPath * MathUtils.cos(putziMouthAngleRadians));
        putziMouthBoundaries.setY(putziPositionY + putziMouthOffsetY + radiusOfMouthPath * MathUtils.sin(putziMouthAngleRadians));


        testCollisionsPutziVsSharks();
        testCollisionsSeaFood();
        testCollisionDirt();
        testCollisionWalls();

    }
    private void testCollisionsPutziVsSharks() {
        for (int i = 0; i < level.sharks.sharks.size; i++) {

            Sharks.Shark shark = level.sharks.sharks.get(i);

            if (shark.velocity.x < 0) {
                sharkHeadBoundaries.setX(shark.position.x + 1.0f);
                sharkHeadBoundaries.setY(shark.position.y + 1.0f);

                sharkBodyBoundaries.setX(shark.position.x + 2.0f);
                sharkBodyBoundaries.setY(shark.position.y + 0.5f);
            } else {
                sharkHeadBoundaries.setX(shark.position.x + 4.0f);
                sharkHeadBoundaries.setY(shark.position.y + 1.0f);

                sharkBodyBoundaries.setX(shark.position.x + 0.0f);
                sharkBodyBoundaries.setY(shark.position.y + 0.5f);

            }

            testFirstTimeSharkHeadCollisions(i);
            testSharkBodyCollisions(i);


        }
    }

    private void testSharkBodyCollisions(int i) {
        // collision with shark from behind or from aside (for putzi invulnerability)
        if (putziMouthBoundaries.overlaps(sharkBodyBoundaries) && putziIsOutsideOfSharkHead.get(i)) {
            onCollisionPutziWithSharkBody();
        }

        // no collision (& resetting values)
        if (!putziBoundaries.overlaps(sharkHeadBoundaries) && !putziMouthBoundaries.overlaps(sharkBodyBoundaries)) {
            onPutziOutsideOfShark(i);
        }
    }
    private void testFirstTimeSharkHeadCollisions(int i) {

        // Erstkollision mit Haifischkopf, nicht geschützt (grün oder blau)
        if (putziBoundaries.overlaps(sharkHeadBoundaries) && notYetTouchedShark.get(i) && !putzi.isProtectedGreen && !putzi.isProtectedBlue) {
            onCollisionPutziWithSharkHead(i);
        }

        // Erstkollision mit Haifischkopf, geschützt (grün)
        if (putziBoundaries.overlaps(sharkHeadBoundaries) && notYetTouchedShark.get(i) && putzi.isProtectedGreen) {
            onCollisionGreenPutziWithSharkHead(i);
        }
    }

    private void testCollisionWalls() {
        for (Wall wall : level.walls) {

            wallBoundaries.setX(wall.position.x);
            wallBoundaries.setY(wall.position.y);

            if (!putziBoundaries.overlaps(wallBoundaries)) {
                continue;
            }

            onCollisionPutziWithWall(wall);

        }
    }
    private void testCollisionDirt() {
        for (Dirt dirt : level.dirts) {

            if (dirt.isCollected()) {
                continue;
            }

            dirtBoundaries.setX(dirt.position.x + 0.3f);
            dirtBoundaries.setY(dirt.position.y + 0.3f);

            if (!putziMouthBoundaries.overlaps(dirtBoundaries)) {
                continue;
            }

            onCollisionPutziWithDirt(dirt);

            break;

        }
    }
    private void testCollisionsSeaFood() {
        for (Shell shell : level.shells) {

            if (shell.isCollected()) {
                continue;
            }

            shellBoundaries.setX(shell.position.x + 0.2f);
            shellBoundaries.setY(shell.position.y + 0.2f);

            if (!putziMouthBoundaries.overlaps(shellBoundaries)) {
                continue;
            }

            onCollisionPutziWithShell(shell);

            break;

        }


        // collision with snails
        for (Snail snail : level.snails) {

            if (snail.isCollected()) {
                continue;
            }

            snailBoundaries.setX(snail.position.x + 0.2f);
            snailBoundaries.setY(snail.position.y + 0.2f);

            if (!putziMouthBoundaries.overlaps(snailBoundaries)) {
                continue;
            }

            onCollisionPutziWithSnail(snail);

            break;

        }


        // collision with crabs

        for (Crab crab : level.crabs) {

            if (crab.isCollected()) {
                continue;
            }

            crabBoundaries.setX(crab.position.x + 0.2f);
            crabBoundaries.setY(crab.position.y + 0.2f);

            if (!putziMouthBoundaries.overlaps(crabBoundaries)) {
                continue;
            }

            onCollisionPutziWithCrab(crab);

            break;

        }
    }

    private void onCollisionPutziWithSharkHead(int i) {

        putziIsOutsideOfSharkHead.set(i, false);
        notYetTouchedShark.set(i, false);

        worldController.addScore(Sharks.Shark.getScore());
        worldController.reduceLivesByOne();
        worldController.setBiteAnimationTimer(Constants.BITE_ANIMATE_TIME);

        AudioManager.instance.play(Assets.instance.sounds.sharkBiteSound);
    }

    private void onCollisionGreenPutziWithSharkHead(int i) {

        putziIsOutsideOfSharkHead.set(i, false);
        notYetTouchedShark.set(i, false);

        putzi.isProtectedGreen = false;
    }
    private void onCollisionPutziWithSharkBody() {

        putzi.isProtectedGreen = true;
    }

    private void onPutziOutsideOfShark(int i) {

        notYetTouchedShark.set(i, true);
        putziIsOutsideOfSharkHead.set(i, true);

    }

    private void onCollisionPutziWithShell(Shell shell) {

        shell.setCollected(true);
        AudioManager.instance.play(Assets.instance.sounds.shellCollectSound);
        worldController.addScore(shell.getScore());

    }

    private void onCollisionPutziWithSnail(Snail snail) {

        snail.setCollected(true);
        AudioManager.instance.play(Assets.instance.sounds.shellCollectSound);
        worldController.setPowerBoostTimer(Constants.POWER_BOOST_TIME);

    }

    private void onCollisionPutziWithCrab(Crab crab) {

        crab.setCollected(true);
        AudioManager.instance.play(Assets.instance.sounds.shellCollectSound);
        worldController.setFreeCameraPowerUpTimer(Constants.CRAB_COLLECTION_FREE_CAMERA_TIME);

    }

    private void onCollisionPutziWithDirt(Dirt dirt) {

        dirt.setCollected(true);
        AudioManager.instance.play(Assets.instance.sounds.dirtCollectSound);
        level.dirts.removeValue(dirt, true);
        worldController.setDirtCount(level.dirts.size);
        if (worldController.getDirtCount() == 0) {
            worldController.setLevelWon(true);
        }
    }

    private void onCollisionPutziWithWall(Wall wall) {

        if (putzi.wallCollisionTimer <= 0) {
            AudioManager.instance.play(Assets.instance.sounds.wallBounceSound);
            putzi.wallCollisionTimer = Constants.WALL_COLLISION_TIMER;
        }

        worldController.addScore(wall.getScore());

        distancePutziWallTop = Math.abs(putzi.position.y - (wall.position.y + wall.dimension.y));
        distancePutziWallBottom = Math.abs(putzi.position.y - (wall.position.y - putzi.dimension.y));
        distancePutziWallLeft = Math.abs(putzi.position.x - (wall.position.x - putzi.dimension.x));
        distancePutziWallRight = Math.abs(putzi.position.x - (wall.position.x + wall.dimension.x));

        if (distancePutziWallTop < 0.1) {
            putzi.position.y = wall.position.y + wall.dimension.y;
        } else if (distancePutziWallBottom < 0.1) {
            putzi.position.y = wall.position.y - putzi.dimension.y;
        } else if (distancePutziWallLeft < 0.1) {
            putzi.position.x = wall.position.x - putzi.dimension.x;
        } else if (distancePutziWallRight < 0.1) {
            putzi.position.x = wall.position.x + wall.dimension.x;
        }
    }
}
