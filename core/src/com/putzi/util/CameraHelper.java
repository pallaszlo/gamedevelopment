package com.putzi.util;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import com.putzi.game.objects.Putzi;

/** offers functionality to set and get camera positions and sets offsets and limits */
public class CameraHelper {


    private static final float MAX_ZOOM_IN = 0.015f;
    private static final float MAX_ZOOM_OUT = 0.1f;
    private static final float FOLLOW_SPEED = 4.0f;

    private Vector2 position;
    private float zoom;
    private Putzi target;
    private Vector2 wavePosition;
    private float bottomLimit;
    private float topLimit;

    /**
     * Constructor for the CameraHelper. Sets position, zoom factor, wavepositions, bottom and toplimit.
     */
    public CameraHelper () {

        position = new Vector2(Constants.LEVEL_MAP_WIDTH/2, Constants.LEVEL_MAP_HEIGHT/2);
        zoom = MAX_ZOOM_IN;
        wavePosition = new Vector2();
        bottomLimit = 10.3f;
        topLimit = 39.7f;

    }


    /**
     * this method is called each frame
     * it provides the calculation of the camera movement
     * @param deltaTime Time between now and last time called
     */
    public void update (float deltaTime) {

        if (!hasTarget()) {
            return;
        }

        wavePosition.x = Math.min(Math.max(target.position.x, 15.5f), 64.5f);
        wavePosition.y = target.position.y;

        if (target.positionWithoutWaveY < bottomLimit) {
            wavePosition.y = target.position.y - target.positionWithoutWaveY + bottomLimit;
        }

        if (target.positionWithoutWaveY > topLimit) {
            wavePosition.y = target.position.y - target.positionWithoutWaveY + topLimit;
        }

        position.lerp(wavePosition, FOLLOW_SPEED * deltaTime);

    }


    /**
     * Sets the position of the camera,
     * is called at the for usage of camara control (Crab Powerup)
     * @param x x-position of level
     * @param y y-position of level
     */
    public void setPosition (float x, float y) {

        this.position.set(x, y);

    }


    /**
     * gets the current position of the camera
     * @return the vector
     */
    public Vector2 getPosition () {

        return position;

    }


    /**
     * Adjusting zoom by adding a value
     * @param amount value to be added to zoom value
     */
    public void addZoom (float amount) {

        setZoom(zoom + amount);

    }


    /**
     * sets the value of the zoom
     * @param zoom value
     */
    public void setZoom (float zoom) {

        this.zoom = MathUtils.clamp(zoom, MAX_ZOOM_IN, MAX_ZOOM_OUT);

    }


    /**
     * returns current zoom value
     * @return zoom value
     */
    public float getZoom () {

        return zoom;

    }


    /**
     * stes the camera target of Typ Putzi
     * @param target putzi
     */
    public void setTarget (Putzi target) {
        this.target = target;
    }


    /**
     * returns the current camera target
     * @return putzi
     */
    public Putzi getTarget () {
        return target;
    }


    /**
     * checks if a target is currently activated
     * @return true if a target is currently activated
     */
    public boolean hasTarget () {
        return target != null;
    }


    /**
     * checks if putzi is currently activated as target
     * @param target putzi
     * @return true if putzi is currently activated as target
     */
    public boolean hasTarget (Putzi target) {
        return hasTarget() && this.target.equals(target);
    }


    /**
     * updates the position of the camera with selected position and zoom
     * @param camera the given Orthographic Camera
     */
    public void applyTo (OrthographicCamera camera) {

        camera.position.x = position.x;
        camera.position.y = position.y;
        camera.zoom = zoom;
        camera.update();
    }
}
