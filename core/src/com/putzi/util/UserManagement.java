package com.putzi.util;

import com.badlogic.gdx.Gdx;

import com.mongodb.client.FindIterable;

import org.bson.Document;

import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;
import java.util.ArrayList;


/**
 * Class for UserManagement
 *
 * Provides methods to create, delete or update user data
 * in the mongoDB-Database.
 */

public class UserManagement {

    private static final String TAG = UserManagement.class.getName();
    /** Represents the MongoDB Connector instance */
    private DatabaseConnector dbConnector;
    /** The instance of the UserManagement (Singleton) */
    public static final UserManagement instance = new UserManagement();
    /** Represents the current username. Null if username is not set (= not logged in). */
    private static String username;

    /** Singleton-Constructor. Sets the DBConnector instance and connects to the User Database */
    private UserManagement() {
        this.dbConnector = DatabaseConnector.instance;
    }

    /**
     * Looks for the specific user in the userDatabase
     *
     * @param username The users username to search for
     * @return true if username is already in db, false if not
     */
    public boolean userAlreadyInDB(String username) {
        boolean inDB = false;
        if (null != username && dbConnector.isConnected()) {
            Document queryDoc = dbConnector.getUserCollection().find(eq("name", username)).first();
            inDB = (null != queryDoc);
        }
        return inDB;
    }

    /**
     * Creates a user with the provided username in the userDatabase.
     *
     * @param username The users username to be created
     */
    public void createUserInDatabase(String username) {
        if ((dbConnector.isConnected()) && (!userAlreadyInDB(username))) {
            // new user with standard preferences
            Document newUser = new Document("name", username)
                    .append("preferences", new Document("sound", true)
                            .append("music", true)
                            .append("volSound", (double)1)
                            .append("volMusic", 0.05)
                            .append("charSkin", 1)
                            .append("showFpsCounter", true)
                            .append("useMonochromeShader", false))
                    .append("highscore", new Document("value", 0));
            dbConnector.getUserCollection().insertOne(newUser);
            Gdx.app.log(TAG, "User " + username + " created");
            UserManagement.username = username;
        } else {
            Gdx.app.log(TAG, "User " + username + " could not be created");
        }
    }

    /**
     * Deletes a user with the specific username from the userDatabase.
     *
     * @param username The users username to be deleted
     * @return True if user is successfully deleted, false if not
     */
    public boolean deleteUserFromDatabse(String username) {
        boolean removed = false;
        if ((userAlreadyInDB(username)) && (dbConnector.isConnected())) {
            dbConnector.getUserCollection().deleteOne(new Document().append("name", username));
            Gdx.app.log(TAG, "User " + username + " deleted.");
            removed = true;
        } else {
            Gdx.app.log(TAG, "User " + username + " could not be deleted.");
        }
        UserManagement.username = null;
        return removed;
    }

    /**
     * Method to return an Object of a specific Preference (key) of a user.
     * The Object represents the value of the preference and has to be cast.
     *
     * @param key the name of the specific preference (e.g. volSound)
     * @return an Object of the preferences value
     */
    public Object getPreference(String key) {
        Object preference = null;
        if (dbConnector.isConnected() && userAlreadyInDB(username)) {
            Document query = new Document("name", username);
            preference = ((Document)dbConnector.getUserCollection().find(query).first().get("preferences")).get(key);
            }
        return preference;
    }

    /**
     * Sets a value of a preference of a specific user.
     * @param key The Key of the value (Preference Name)
     * @param value The Value of the preference to set
     */
    void setPreference(String key, Object value) {
        if (dbConnector.isConnected() && userAlreadyInDB(username)) {
            dbConnector.getUserCollection().updateOne(eq("name" , username),set("preferences." + key, value));
            Gdx.app.log(TAG, key + " set to " + value.toString());
        } else {
            Gdx.app.log(TAG, "The value for " + key +" of user "
                    + username + " could not be changed.");
        }
    }

    /**
     * Sets a users Highscore to the provided value

     * @param value The new Highscore value
     */
    public void setHighscore(int value) {
        if (dbConnector.isConnected() && userAlreadyInDB(username)) {
            Document query = new Document("name", username);
            Document newValue = new Document();
            newValue.append("$set", new Document().append("highscore.value", value));
            dbConnector.getUserCollection().updateOne(query, newValue);
            Gdx.app.log(TAG, "Highscore of " + username + " set to " + value);
        } else {
            Gdx.app.log(TAG, "Highscore of " + username + " could not be set ");
        }
    }

    /**
     * Gets a users highscore as int
     * @param username The users name to get the highscore from
     * @return The users highscore as int
     */
    public int getHighscore(String username) {
        int highscore = -1;
        if (dbConnector.isConnected() && userAlreadyInDB(username)) {
            FindIterable<Document> userDoc = dbConnector.getUserCollection().find(eq("name", username));
            highscore =((Document)userDoc.first().get("highscore")).getInteger("value");
        }
        return highscore;
    }

    /**
     * Returns all the userNames in the Database. The names are returned sorted descending by
     * their highscores.
     * @return an ArrayList of Strings containing all usernames
     */
    public ArrayList<String> getUserNames() {
        ArrayList<String> userNames = new ArrayList<>();
        if (dbConnector.isConnected()) {
            MongoCursor<Document> queryCursor = dbConnector.getUserCollection().find().sort(new Document("highscore.value", -1)).iterator();
            try {
                while (queryCursor.hasNext()) {
                    String userName = queryCursor.next().get("name").toString();
                    userNames.add(userName);
                }
            } finally {
                queryCursor.close();
            }
        }
        return userNames;
    }

    /**
     * Gets the current user
     * @return The current user username. Null if the username is not set (= local game)
     */
    public static String getUser() {
        return UserManagement.username;
    }

    /**
     * Sets a username to be the current user. Needs to be set on every loading or saving
     * process initiated by other classes.
     * @param username The users username to be set for usermanagement
     */
    public static void setUser(String username) {
        UserManagement.username = username;
    }
}
