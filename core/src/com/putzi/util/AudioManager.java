package com.putzi.util;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;


/**
 * Manages Sounds and Music playing
 */
public class AudioManager {

	public static final AudioManager instance = new AudioManager();

	private Music playingMusic;


	/**
	 * Singleton!
	 */
	private AudioManager () {}


	/**
	 * plays a given sound one single time
	 * @param sound the sound asset to be played
	 */
	public void play (Sound sound) {

		play(sound, 1);

	}

	/**
	 * plays a given sound in a loop
	 * @param sound the sound asset to be played
	 */
	public void loop(Sound sound){

		sound.loop();
	}

	/**
	 * stops a given sound asset
	 * @param sound the sound asset to be stopped
	 */
	public void stop(Sound sound){
		sound.stop();
	}


	/**
	 * change the volume of a given sound asset
	 * @param sound the sound asset to be changed in volume
	 * @param volume the volume where to the sound asset has to be changed
	 */
	public void play (Sound sound, float volume) {

		play(sound, volume, 1);

	}

	/**
	 * change the volume and pitch of a given sound asset
	 * @param sound the sound asset to be changed in volume
	 * @param volume the volume where to the sound asset has to be changed
	 * @param pitch the pitch whereto the sound asset has to be changed
	 */
	public void play (Sound sound, float volume, float pitch) {

		play(sound, volume, pitch, 0);

	}



	/**
	 * change the volume and pitch of a given sound asset
	 * @param sound the sound asset to be changed in volume
	 * @param volume the volume where to the sound asset has to be changed
	 * @param pitch the pitch whereto the sound asset has to be changed
	 * @param pan the pan whereto the sound asset has to be changed
	 */
	public void play (Sound sound, float volume, float pitch, float pan) {

		if (!GamePreferences.instance.sound) {
			return;
		}

		sound.play((float)GamePreferences.instance.volSound * volume, pitch, pan);

	}


	/**
	 * plays a given music asset in a loop
	 * @param music the music asset tob be played
	 */
	public void play (Music music) {

		playingMusic = music;

		if (GamePreferences.instance.music) {

			music.setLooping(true);
			music.setVolume((float)GamePreferences.instance.volMusic);
			music.play();

		}

	}


    /**
     * stops music playing
     */
	public void stopMusic () {

		if (playingMusic != null) {

			playingMusic.stop();

		}
	}


	/**
	 * get the music, which currently is playing
	 * @return the music playing asset
	 */
	public Music getPlayingMusic () {

		return playingMusic;

	}


	/**
	 * when options in the settingsmenu updated this method is called,
     * e.g. if the music checkbox is unchecked, the music will be paused
	 */
	public void onSettingsUpdated () {

		if (playingMusic == null) {
			return;
		}

		playingMusic.setVolume((float)GamePreferences.instance.volMusic);

		if (GamePreferences.instance.music) {

			if (!playingMusic.isPlaying()) {

				play(playingMusic);

			}

		} else {

			playingMusic.pause();

		}

	}

}
