package com.putzi.util;

import com.badlogic.gdx.graphics.Color;


/** Represents a data model for putzis character skins. */
public enum CharacterSkin {


	WHITE("weiss", 1.0f, 1.0f, 1.0f),
	GRAY("gelb", 0.8f, 0.8f, 0.0f),
	BROWN("braun", 0.7f, 0.5f, 0.3f);

	private String name;
	private Color color = new Color();


	CharacterSkin(String name, float r, float g, float b) {

		this.name = name;
		color.set(r, g, b, 1.0f);

	}


	/**
	 * Returns the name of an object
	 * @return the name as a string
	 */
	@Override
	public String toString() {

		return name;

	}


	/**
	 * Returns the color in rgba of a characterskin
	 * @return the color in rgba
	 */
	public Color getColor() {
		return color;
	}
}
