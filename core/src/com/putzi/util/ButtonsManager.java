package com.putzi.util;

import com.badlogic.gdx.Gdx;


import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.putzi.game.Assets;

import java.util.concurrent.TimeUnit;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.touchable;

/**
 * Implements some Button Listeners for exiting the game and to show the options window.
 */
public class ButtonsManager {
    private static String TAG = ButtonsManager.class.getName();

    /**
     * Listener for exit-button in the controls-layer in menu screen and game over screen.
     * Leave the game
     */
    public static void onExitClicked() {
        AudioManager.instance.play(Assets.instance.sounds.buttonCancelClickedSound);
        DatabaseConnector.instance.disconnectFromMongoDB();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            Gdx.app.log(TAG, "UnknownHostException: " + e);
        }
        Gdx.app.exit();
    }

    /**
     * Method to show and animate the selected window.
     * @param visible if true show the window
     * @param animated if true animate the window
     * @param windowToShow name for selected window
     */
    public static void showOptionsWindow(boolean visible, boolean animated, Window windowToShow) {

        // amimated blending out and in for optionfield
        float alphaTo = visible ? 0.8f : 0.0f;
        float duration = animated ? 1.0f : 0.0f;

        Touchable touchEnabled = visible ? Touchable.enabled : Touchable.disabled;

        windowToShow.addAction(
                sequence(
                        touchable(touchEnabled),
                        alpha(
                                alphaTo,
                                duration
                        )
                )
        );

    }


}
