package com.putzi.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.math.Interpolation;
import com.putzi.screens.MenuScreen;
import com.putzi.screens.transitions.ScreenTransition;
import com.putzi.screens.transitions.ScreenTransitionSlice;
import com.putzi.util.AudioManager;
import com.putzi.util.Constants;
import com.putzi.util.GamePreferences;

/**
 * Initializing the Game
 * Other LibGDX-Lifecycle Methods (render, resize, resume, pause and dispose)
 * are implemented together with setScreen() in ExtendedGame
 */
public class PutziMain extends ExtendedGame {

    /**
     * Called when the PutziMain is first created.
     */
    @Override
    public void create () {

        Gdx.app.setLogLevel(Application.LOG_DEBUG);

        Assets.instance.init(new AssetManager());

        GamePreferences.instance.loadPreferences();

        ScreenTransition screenTransition = ScreenTransitionSlice.init(
                Constants.INTRO_SLICE_DURATION,
                ScreenTransitionSlice.UP_DOWN,
                Constants.INTRO_SLICE_NUMBER,
                Interpolation.pow2Out);

        setScreen(new MenuScreen(this), screenTransition);
        AudioManager.instance.play(Assets.instance.sounds.menuFirstStartSound, 0.5f);
        AudioManager.instance.play(Assets.instance.music.bubbleMusic);
    }

}


