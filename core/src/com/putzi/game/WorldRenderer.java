package com.putzi.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.putzi.util.Constants;
import com.putzi.util.GamePreferences;


/**
 * Class for world rendering, initializes Objectsprites (SpriteBatch), Camera, Shader,
 * HUD and Messages and updates these every frame
 */
public class WorldRenderer implements Disposable {

    private OrthographicCamera gameCamera;
    private OrthographicCamera displayCamera;
    private SpriteBatch spriteBatch;
    private WorldController worldController;

    private ShaderProgram shaderMonochrome;

    /**
     * Initialize controller and renderer
     * @param worldController the worldController to use
     */
    public WorldRenderer (WorldController worldController) {

        this.worldController = worldController;
        init();
    }


    private void init() {

        // Object Sprites
        spriteBatch = new SpriteBatch();

        // Game Layer Camera
        gameCamera = new OrthographicCamera(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
        gameCamera.position.set(0, 0, 0);
        gameCamera.update();

        // HUD Camera
        displayCamera = new OrthographicCamera(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        displayCamera.position.set(0, 0, 0);
        displayCamera.setToOrtho(true);             // Ursprung unten
        displayCamera.update();

        // Shader (grayscale)
        shaderMonochrome = new ShaderProgram(
            Gdx.files.internal(Constants.SHADER_MONOCHROME_VERTEX),
            Gdx.files.internal(Constants.SHADER_MONOCHROME_FRAGMENT)
        );

        if (!shaderMonochrome.isCompiled()) {

            String msg = "Could not compile shader program: " + shaderMonochrome.getLog();
            throw new GdxRuntimeException(msg);
        }
    }


    /**
     * renders game layer and head up display
     */
    public void render () {
        
        renderGameWorld(spriteBatch);
        renderDisplays(spriteBatch);

    }

    /**
     * renders the game layer
     * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
     */
    private void renderGameWorld (SpriteBatch spriteBatch) {

        worldController.cameraHelper.applyTo(gameCamera);
        spriteBatch.setProjectionMatrix(gameCamera.combined);

        spriteBatch.begin();

        if (GamePreferences.instance.useMonochromeShader) {
            spriteBatch.setShader(shaderMonochrome);
            shaderMonochrome.setUniformf("u_amount", 1.0f);
        }

        worldController.level.render(spriteBatch);
        spriteBatch.setShader(null);
        spriteBatch.end();
    }


    /**
     * renders the head up display in the game Screen
     * including score, lives, levelnumber, dirtcount and powerup timers
     * also renders the text
     * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
     */
    private void renderDisplays (SpriteBatch spriteBatch) {

        spriteBatch.setProjectionMatrix(displayCamera.combined);

        spriteBatch.begin();

        renderScoreDisplay(spriteBatch);        
        renderDirtDisplay(spriteBatch);
        renderLevelDisplay(spriteBatch);
        renderLivesDisplay(spriteBatch);

        if (worldController.getPowerBoostTimer() > 0) {
            renderPowerTimeDisplay(spriteBatch);
        }

        if (worldController.getFreeCameraPowerUpTimer() > 0) {
            renderCameraTimeDisplay(spriteBatch);
        }

        if (GamePreferences.instance.showFpsCounter) {
            renderFpsDisplay(spriteBatch);
        }

        renderMessages(spriteBatch);

        spriteBatch.end();

    }

    /**
     * Renders messages for different conditions
     * Level won, game lost, pause
     * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
     */
    private void renderMessages(SpriteBatch spriteBatch) {
        if (worldController.isGameOver() && !worldController.isLevelPaused()) {
            renderMessageDisplay(spriteBatch, "LEIDER VERLOREN!");
        }

        if (worldController.isLevelPaused()) {
            renderMessageDisplay(spriteBatch, "PAUSE \n zur Hauptseite mit ESC \n weiterspielen mit ENTER");
        }

        if (worldController.isLevelWon() && !worldController.isGameWon() && !worldController.isLevelPaused()) {
            renderMessageDisplay(spriteBatch, "LEVEL " + (worldController.getLevelNumber()) + " GEWONNEN!");
        }

        if (worldController.isGameWon() && !worldController.isLevelPaused()) {
            renderMessageDisplay(spriteBatch, "SPIEL GEWONNEN!");
        }
    }

    /**
     * Renders the display of the score
     * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
     */
    private void renderScoreDisplay (SpriteBatch spriteBatch) {

        float positionX = 15;
        float positionY = 90;
        float offsetX = 0;
        float offsetY = 0;

        if (worldController.getScoreVisual() < worldController.getScore()) {

            // Calculation offset for die shake-Animation
            long currentTime = System.currentTimeMillis() % 360;
            float amplitude = 1.5f;
            offsetX += MathUtils.sinDeg(currentTime * 2.2f) * amplitude;
            offsetY += MathUtils.sinDeg(currentTime * 2.9f) * amplitude;
        }

        spriteBatch.draw(
            Assets.instance.shell.shell,
            positionX,
            positionY,
            offsetX,
            offsetY,
            80,
            80,
            1,
            -1,
            0
        );

        Assets.instance.fonts.defaultBig.draw(
            spriteBatch,
            (int) worldController.getScoreVisual() + " Bonuspunkte",
            positionX + 100,
            positionY - 50
        );
    }

    /**
     * Renders the display of the Dirt items to collect
     * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
     */
    private void renderDirtDisplay (SpriteBatch spriteBatch) {

        float positionX = 15;
        float positionY = 210;

        spriteBatch.draw(
                Assets.instance.dirt.dirt,
                positionX,
                positionY,
                0,
                0,
                80,
                80,
                1,
                -1,
                0
        );

        Assets.instance.fonts.defaultBig.draw(
                spriteBatch,
                "noch " + worldController.getDirtCount() + "x Schmutz einsammeln",
                positionX + 100,
                positionY - 70
        );
    }

    /**
     * Renders the display of the time left for powerboost
     * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
     */
    private void renderPowerTimeDisplay (SpriteBatch spriteBatch) {

        float positionX = 15;
        float positionY = 300;

        spriteBatch.draw(
                Assets.instance.snail.snail,
                positionX,
                positionY,
                0,
                0,
                80,
                80,
                1,
                -1,
                0
        );

        Assets.instance.fonts.defaultBig.draw(
                spriteBatch,
                "Powerboost noch " + String.format("%.1f", worldController.getPowerBoostTimer()) + " Sekunden",
                positionX + 100,
                positionY - 60
        );
    }

    /**
     * Renders the display of the Lives with Putzi Icons for each, with animation when one life is lost
     * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
     */
    private void renderLivesDisplay(SpriteBatch spriteBatch) {

        float positionX = displayCamera.viewportWidth - 15 - Constants.PUTZI_LIVES_START * 80;
        float positionY = 90;

        Assets.instance.fonts.defaultBig.draw(
                spriteBatch,
                "noch " + Math.max(worldController.getLives(), 0) + " Leben",
                positionX - 200,
                positionY - 50
        );


        for (int i = 0; i < Constants.PUTZI_LIVES_START; i++) {

            if (worldController.getLives() <= i) {
                spriteBatch.setColor(0.5f, 0.5f, 0.5f, 0.5f);
            }

            spriteBatch.draw(
                    Assets.instance.putzi.putzi,
                    positionX + i * 80,
                    positionY,
                    0,
                    0,
                    80,
                    80,
                    1,
                    -1,
                    0
            );

            spriteBatch.setColor(1, 1, 1, 1);
        }

        // Animation for loosing one life

        if (worldController.getLives() > 0 && worldController.getLivesVisual() > worldController.getLives()) {

            int lives = worldController.getLives();
            float livesVisual = worldController.getLivesVisual();

            float alphaColor = Math.max(0, livesVisual - lives - 0.5f);
            float alphaScale = 0.35f * (2 + lives - livesVisual) * 2;
            float alphaRotate = -45 * alphaColor;

            spriteBatch.setColor(1.0f, 0.7f, 0.7f, alphaColor);

            spriteBatch.draw(
                    Assets.instance.putzi.putzi,
                    positionX + lives * 80,
                    positionY,
                    0,
                    0,
                    100,
                    100,
                    alphaScale,
                    -alphaScale,
                    alphaRotate
            );
            spriteBatch.setColor(1, 1, 1, 1);
        }
    }

    /**
     * Renders the display of the time left for Free Camera Powerup
     * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
     */
    private void renderCameraTimeDisplay (SpriteBatch spriteBatch) {

        // Verbleibende Kamerazeit anzeigen

        float positionX = displayCamera.viewportWidth - 95;
        float positionY = 190;

        if (worldController.getFreeCameraPowerUpTimer() < 3 &&
                ((int)(worldController.getFreeCameraPowerUpTimer() * 5) % 2) != 0) {
               spriteBatch.setColor(1, 1, 1, 0.5f);
        }

        spriteBatch.draw(
                Assets.instance.crab.crab,
                positionX,
                positionY,
                0,
                0,
                80,
                80,
                1,
                -1,
                0
        );

        Assets.instance.fonts.defaultBig.draw(
                spriteBatch,
                "Freie Kamera noch " + String.format("%.1f", worldController.getFreeCameraPowerUpTimer()) + " sec",
                positionX - 380,
                positionY - 50
        );
        spriteBatch.setColor(1, 1, 1, 1);

    }

    /**
     * Renders the FPS(Frames per Seconds) Display if activated
     * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
     */
    private void renderFpsDisplay (SpriteBatch spriteBatch) {

        // Anzeige der Frames pro Sekunde rendern

        float positionX = displayCamera.viewportWidth - 150;
        float positionY = displayCamera.viewportHeight - 50;

        int fps = Gdx.graphics.getFramesPerSecond();

        BitmapFont fpsFont = Assets.instance.fonts.defaultBig;

        if (fps >= 45) {
            fpsFont.setColor(0, 1, 0, 1); // grün
        } else if (fps >= 30) {
            fpsFont.setColor(1, 1, 0, 1); // gelb
        } else {
            fpsFont.setColor(1, 0, 0, 1); // rot
        }

        fpsFont.draw(
            spriteBatch,
            fps + " FPS",
            positionX,
            positionY
        );

        fpsFont.setColor(1, 1, 1, 1); // auf weiß zurücksetzen

    }

    /**
     * Renders a centered Display Messages with the given String
     * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
     */
    private void renderMessageDisplay (SpriteBatch spriteBatch, String message) {

        float positionX = displayCamera.viewportWidth / 2;
        float positionY = displayCamera.viewportHeight / 2;

            BitmapFont fontGameOver = Assets.instance.fonts.defaultBig;

            fontGameOver.draw(
                spriteBatch,
                message,
                positionX,
                positionY,
                1,
                Align.center,
                true
            );

    }

    /**
     * Renders the display current level
     * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
     */
    private void renderLevelDisplay (SpriteBatch spriteBatch) {

        float positionX = displayCamera.viewportWidth / 2;
        float positionY = 40;

        BitmapFont fontLevel = Assets.instance.fonts.defaultBig;

        fontLevel.setColor(1, 1, 1, 1);

        fontLevel.draw(
                spriteBatch,
                "LEVEL " + worldController.getLevelNumber(),
                positionX,
                positionY,
                1,
                Align.center,
                true
        );

    }

    /**
     * resize the Screen, depending on Viewportsizes
     * @param width width to calculate with
     * @param height height to calculate with
     */
    public void resize (int width, int height) {
        
        // Auf Änderung der Viewportgröße reagieren
        
        gameCamera.viewportWidth = (Constants.VIEWPORT_HEIGHT/(float)height) * (float)width;

        gameCamera.update();

        displayCamera.viewportHeight = Constants.VIEWPORT_GUI_HEIGHT;
        displayCamera.viewportWidth = (Constants.VIEWPORT_GUI_HEIGHT/(float)height) * (float)width;

        displayCamera.position.set(
            displayCamera.viewportWidth/2,
            displayCamera.viewportHeight/2,
            0
        );

        displayCamera.update();

    }


    /**
     * free memory
     * disposes the Sprite Batch and the Shader
     */
    @Override
    public void dispose () {

        spriteBatch.dispose();
        shaderMonochrome.dispose();

    }

}

