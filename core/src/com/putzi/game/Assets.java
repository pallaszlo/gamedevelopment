package com.putzi.game;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import com.putzi.util.Constants;


/**
 * Uses the Assetmanager to Create and Manage all Assets
 * like Fonts, Sounds and Sprites for Game Objects
 */
public class Assets implements Disposable, AssetErrorListener {

    private static final String TAG = Assets.class.getName();

    /**
     * Instanciate Complete set of Assets
     */
    public static final Assets instance = new Assets();


    /**
     * Helps loading and managing assets, it is the recommended way to load your assets,
     * benefits due to:
     * - Loading of most resources is done asynchronously
     * - dependencies are acknowledged when disposing assets
     */
    private AssetManager assetManager;


    /**
     * Declaration of AssetPutzi, the Textures and Animations of the main character in the game
     */
    public AssetPutzi putzi;

    /**
     * Declaration of shells
     */
    public AssetShell shell;

    /**
     * Declaration of snails
     */
    public AssetSnail snail;

    /**
     * Declaration of sharks
     */
    public AssetShark shark;

    /**
     * Declaration of crabs
     */
    public AssetCrab crab;

    /**
     * Declaration of dirt parts
     */
    public AssetDirt dirt;

    /**
     * Declaration of wall items
     */
    public AssetWall wall;

    /**
     * Declaration of decorations
     */
    public AssetLevelDecoration levelDecoration;

    /**
     * Declaration of sounds
     */
    public AssetSounds sounds;

    /**
     * Declaration of music
     */
    AssetMusic music;

    /**
     * Declaration of fonts
     */
    AssetFonts fonts;


    /**
     * All Assets will be packed in one location
     */
    public Assets() { /* Singleton !! */}

    /**
     * Assets Loading
     *
     * @param assetManager single point of usage
     */
    public void init(AssetManager assetManager) {

        this.assetManager = assetManager;

        assetManager.setErrorListener(this);

        assetManager.load(Constants.TEXTURE_ATLAS_OBJECTS, TextureAtlas.class);

        assetManager.load("sounds/water-sounds-underwater-underwater-motion-fienup-015.mp3", Sound.class);
        assetManager.load("sounds/biting-biting-celery-fast-02.mp3", Sound.class);
        assetManager.load("sounds/shell.wav", Sound.class);
        assetManager.load("sounds/dirt.wav", Sound.class);
        assetManager.load("sounds/video-game-success-success-elegant-03.wav", Sound.class);
        assetManager.load("sounds/alerts-ascend-descend-alert-01-descending.wav", Sound.class);
        assetManager.load("music/start-up-logos-logins-harmonious-04.mp3", Sound.class);
        assetManager.load("music/start-up-logos-logins-magic-chime-alert-02.mp3", Sound.class);

        assetManager.load("music/water-sounds-bubbles-bubble-liquid-10.mp3", Music.class);

        assetManager.finishLoading();

        TextureAtlas atlas = assetManager.get(Constants.TEXTURE_ATLAS_OBJECTS);

        for (Texture texture : atlas.getTextures()) {
            texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        }

        fonts = new AssetFonts();
        putzi = new AssetPutzi(atlas);
        shell = new AssetShell(atlas);
        snail = new AssetSnail(atlas);
        crab = new AssetCrab(atlas);
        dirt = new AssetDirt(atlas);
        shark = new AssetShark(atlas);
        wall = new AssetWall(atlas);
        levelDecoration = new AssetLevelDecoration(atlas);
        sounds = new AssetSounds(assetManager);
        music = new AssetMusic(assetManager);

    }

    /**
     * Inherited Class for disposal
     */
    @Override
    public void dispose() {

        assetManager.dispose();
        fonts.defaultSmall.dispose();
        fonts.defaultNormal.dispose();
        fonts.defaultBig.dispose();

    }

    /**
     * Inherited Class for error handling
     *
     * @param asset     the asset where an error occured
     * @param throwable the error
     */
    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {

        Gdx.app.error(
                TAG,
                "Couldn't load asset '" + asset.fileName + "'",
                throwable
        );

    }

    /**
     * Inner Assetclass for fonts used in the menu and in the HUD of the game
     * assign the font and provides settings for different usage
     */
    public class AssetFonts {

        final BitmapFont defaultSmall;
        final BitmapFont defaultNormal;
        final BitmapFont defaultBig;

        final FreeTypeFontGenerator generator;

        AssetFonts() {

            generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/SharkSoftBites.ttf"));

            FreeTypeFontParameter parameterN = new FreeTypeFontParameter();
            parameterN.size = 22;
            parameterN.color = Color.CORAL;

            FreeTypeFontParameter parameterB = new FreeTypeFontParameter();
            parameterB.size = 48;
            parameterB.color = Color.GOLD;
            parameterB.shadowColor = Color.BLACK;
            parameterB.shadowOffsetX = 5;
            parameterB.shadowOffsetY = 5;
            parameterB.spaceX = -1;

            FreeTypeFontParameter parameterS = new FreeTypeFontParameter();
            parameterS.size = 25;
            parameterS.color = Color.FIREBRICK;

            defaultNormal = generator.generateFont(parameterN);
            defaultBig = generator.generateFont(parameterB);
            defaultSmall = generator.generateFont(parameterS);

            defaultNormal.getData().setScale(1, -1);
            defaultBig.getData().setScale(1, -1);
            defaultSmall.getData().setScale(1, -1);

        }

    }

    /**
     * Inner Assetclass of Putzi the Main Character in this game
     * will assign graphics from TextureAtlas for display and animations
     */
    public class AssetPutzi {

        public final AtlasRegion putzi;
        public final Animation putziAnimationSwim;
        public final Animation putziAnimationTurnLeftHorizontal;
        public final Animation putziAnimationTurnLeft60DegUp;
        public final Animation putziAnimationTurnLeft60DegDown;
        public final Animation putziAnimationTurnLeft30DegUp;
        public final Animation putziAnimationTurnLeft30DegDown;


        AssetPutzi(TextureAtlas atlas) {

            putzi = atlas.findRegion("putzi");

            Array<AtlasRegion> keyframeTexturesPutziSwim = atlas.findRegions("PutziSwimHorizontal");
            Array<AtlasRegion> keyframeTexturesPutziTurnLeftHorizontal = atlas.findRegions("PutziTurnLeftHorizontal");
            Array<AtlasRegion> keyframeTexturesPutziTurnLeft60DegUp = atlas.findRegions("PutziTurnLeft60DegUp");
            Array<AtlasRegion> keyframeTexturesPutziTurnLeft60DegDown = atlas.findRegions("PutziTurnLeft60DegDown");
            Array<AtlasRegion> keyframeTexturesPutziTurnLeft30DegUp = atlas.findRegions("PutziTurnLeft30DegUp");
            Array<AtlasRegion> keyframeTexturesPutziTurnLeft30DegDown = atlas.findRegions("PutziTurnLeft30DegDown");

            putziAnimationSwim = new Animation<AtlasRegion>(
                    Constants.PUTZI_LONG_FRAMEDURATION, keyframeTexturesPutziSwim,
                    Animation.PlayMode.LOOP
            );

            putziAnimationTurnLeftHorizontal = new Animation<AtlasRegion>(
                    0.3f / 10.0f, keyframeTexturesPutziTurnLeftHorizontal
            );

            putziAnimationTurnLeft60DegUp = new Animation<AtlasRegion>(
                    0.7f / 10.0f, keyframeTexturesPutziTurnLeft60DegUp
            );

            putziAnimationTurnLeft60DegDown = new Animation<AtlasRegion>(
                    0.7f / 10.0f, keyframeTexturesPutziTurnLeft60DegDown
            );

            putziAnimationTurnLeft30DegUp = new Animation<AtlasRegion>(
                    0.5f / 10.0f, keyframeTexturesPutziTurnLeft30DegUp
            );

            putziAnimationTurnLeft30DegDown = new Animation<AtlasRegion>(
                    0.5f / 10.0f, keyframeTexturesPutziTurnLeft30DegDown
            );

        }

    }

    /**
     * Inner Assetclass of Shells in the game
     * assign graphics from TextureAtlas for display and animations
     */
    public class AssetShell {

        final AtlasRegion shell;

        public final Animation animShell;

        AssetShell(TextureAtlas atlas) {

            shell = atlas.findRegion("shell");

            Array<AtlasRegion> keyFrameTextures = atlas.findRegions("anim_shell");
            AtlasRegion firstKeyFrameTexture = keyFrameTextures.first();

            for (int i = 0; i < 10; i++) {

                keyFrameTextures.insert(0, firstKeyFrameTexture);

            }

            animShell = new Animation<AtlasRegion>(
                    1.0f / 20.0f,
                    keyFrameTextures,
                    Animation.PlayMode.LOOP_PINGPONG
            );

        }
    }

    /**
     * Inner Assetclass of Snailhouses in the game
     * assign graphics from TextureAtlas for display and animations
     */
    public class AssetSnail {

        final AtlasRegion snail;

        public final Animation animSnail;

        AssetSnail(TextureAtlas atlas) {

            snail = atlas.findRegion("snail");

            Array<AtlasRegion> keyFrameTextures = atlas.findRegions("anim_snail");
            AtlasRegion firstKeyFrameTexture = keyFrameTextures.first();

            for (int i = 0; i < 10; i++) {

                keyFrameTextures.insert(0, firstKeyFrameTexture);

            }

            animSnail = new Animation<AtlasRegion>(
                    1.0f / 17.0f,
                    keyFrameTextures,
                    Animation.PlayMode.LOOP_PINGPONG
            );

        }
    }

    /**
     * Inner Assetclass of Crabs in the game
     * assign graphics from TextureAtlas for display and animations
     */
    public class AssetCrab {

        final AtlasRegion crab;

        public final Animation animCrab;

        AssetCrab(TextureAtlas atlas) {

            crab = atlas.findRegion("crab");

            Array<AtlasRegion> keyFrameTextures = atlas.findRegions("anim_crab");
            AtlasRegion firstKeyFrameTexture = keyFrameTextures.first();

            for (int i = 0; i < 10; i++) {

                keyFrameTextures.insert(0, firstKeyFrameTexture);

            }

            animCrab = new Animation<AtlasRegion>(
                    1.0f / 20.0f,
                    keyFrameTextures,
                    Animation.PlayMode.LOOP_PINGPONG
            );

        }
    }

    /**
     * Inner Assetclass of Dirt in the game
     * assign graphics from TextureAtlas for display and animations
     */
    public class AssetDirt {

        final AtlasRegion dirt;
        public final Animation animDirt;

        AssetDirt(TextureAtlas atlas) {

            dirt = atlas.findRegion("dirt");

            Array<AtlasRegion> keyFrameTextures = atlas.findRegions("anim_dirt");

            animDirt = new Animation<AtlasRegion>(
                    1.0f / 20.0f,
                    keyFrameTextures,
                    Animation.PlayMode.LOOP_PINGPONG
            );

        }

    }

    /**
     * Inner Assetclass of Shells in the game
     * assign graphics from TextureAtlas for display and animations
     * differences in three alternative speeds of animation
     */
    public class AssetShark {

        public final Animation animSharkSlow;
        public final Animation animSharkMedium;
        public final Animation animSharkFast;

        AssetShark(TextureAtlas atlas) {

            Array<AtlasRegion> keyFrameTextures = atlas.findRegions("HaiFrames");

            animSharkSlow = new Animation<AtlasRegion>(
                    0.7f / 10.0f,
                    keyFrameTextures,
                    Animation.PlayMode.LOOP
            );

            animSharkMedium = new Animation<AtlasRegion>(
                    0.5f / 10.0f,
                    keyFrameTextures,
                    Animation.PlayMode.LOOP
            );

            animSharkFast = new Animation<AtlasRegion>(
                    0.3f / 10.0f,
                    keyFrameTextures,
                    Animation.PlayMode.LOOP
            );

        }

    }

    /**
     * Inner Assetclass of Walls in the game
     * assign graphics from TextureAtlas for display
     */
    public class AssetWall {

        public final AtlasRegion wall;

        AssetWall(TextureAtlas atlas) {

            wall = atlas.findRegion("wall");

        }

    }

    /**
     * Inner Assetclass of Decoration Graphics in the game
     * assign graphics from TextureAtlas for display
     * These graphics are used for the parallax/3D-Effect
     */
    public class AssetLevelDecoration {

        public final AtlasRegion background_0_game;
        public final AtlasRegion background_1_game;
        public final AtlasRegion background_2_game;
        public final AtlasRegion background_3_game;

        public final AtlasRegion foreground_0_game;
        public final AtlasRegion foreground_1_game;
        public final AtlasRegion foreground_2_game;

        public final AtlasRegion level_1_overlay;
        public final AtlasRegion level_2_overlay;
        public final AtlasRegion level_3_overlay;

        /**
         * assigning Level Decoration Pictures from Atlas to Variables
         *
         * @param atlas Texture Atlas, where all picture content is saved
         */
        AssetLevelDecoration(TextureAtlas atlas) {

            background_0_game = atlas.findRegion("background_0_game");
            background_1_game = atlas.findRegion("background_1_game");
            background_2_game = atlas.findRegion("background_2_game");
            background_3_game = atlas.findRegion("background_3_game");

            foreground_0_game = atlas.findRegion("foreground_0_game");
            foreground_1_game = atlas.findRegion("foreground_1_game");
            foreground_2_game = atlas.findRegion("foreground_2_game");

            level_1_overlay = atlas.findRegion("level_1_overlay");
            level_2_overlay = atlas.findRegion("level_2_overlay");
            level_3_overlay = atlas.findRegion("level_3_overlay");

        }

    }

    /**
     * Assign Soundobjects preloaded by Assetmanager
     */
    public class AssetSounds {

        public final Sound wallBounceSound;
        public final Sound sharkBiteSound;
        public final Sound shellCollectSound;
        public final Sound dirtCollectSound;
        public final Sound buttonOkClickedSound;
        public final Sound buttonCancelClickedSound;
        final Sound menuFirstStartSound;
        public final Sound gameStartUpSound;

        /**
         * Load Soundobjects to variables
         * @param assetManager Soundfiles ar preloaded here
         */
        AssetSounds(AssetManager assetManager) {

            wallBounceSound = assetManager.get("sounds/water-sounds-underwater-underwater-motion-fienup-015.mp3", Sound.class);
            sharkBiteSound = assetManager.get("sounds/biting-biting-celery-fast-02.mp3", Sound.class);
            shellCollectSound = assetManager.get("sounds/shell.wav", Sound.class);
            dirtCollectSound = assetManager.get("sounds/dirt.wav", Sound.class);
            buttonOkClickedSound = assetManager.get("sounds/video-game-success-success-elegant-03.wav");
            buttonCancelClickedSound = assetManager.get("sounds/alerts-ascend-descend-alert-01-descending.wav");
            menuFirstStartSound = assetManager.get("music/start-up-logos-logins-harmonious-04.mp3", Sound.class);
            gameStartUpSound = assetManager.get("music/start-up-logos-logins-magic-chime-alert-02.mp3", Sound.class);

        }

    }

    /**
     * Loading Background Music Asset
     */
    public class AssetMusic {

        final Music bubbleMusic;

        AssetMusic(AssetManager assetManager) {

            bubbleMusic = assetManager.get("music/water-sounds-bubbles-bubble-liquid-10.mp3", Music.class);
        }

    }

}
