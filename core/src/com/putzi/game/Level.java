package com.putzi.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import com.putzi.game.objects.AbstractGameObject;
import com.putzi.game.objects.Crab;
import com.putzi.game.objects.Dirt;
import com.putzi.game.objects.Sharks;
import com.putzi.game.objects.Shell;
import com.putzi.game.objects.Putzi;
import com.putzi.game.objects.Snail;
import com.putzi.game.objects.Wall;
import com.putzi.game.objects.ParallaxImages;
import com.putzi.util.Constants;


/**
 * Creates level objects by parsing a picture file,
 * the picture file needs to have specific colored
 * pixels to be transformed to objects
 */
public class Level {

	private static final String TAG = Level.class.getName();

	/**
	 * Objects with corresponding color codes in RGB for level parsing
	 */
	public enum BLOCK_TYPE {

		EMPTY(0, 0, 0),                    // black: background
		PLAYER(255, 255, 255),             // white: Putzi
		DIRT(255, 0, 255),                 // purple: dirt
		SHELL(255, 255, 0),                // yellow: shell
		SNAIL(0, 255, 0),                  // green: snail
		CRAB(255, 0, 0),                   // red: crab
		WALL(0, 0, 255);                   // blue: wall

		private int color;

		BLOCK_TYPE (int r, int g, int b) {
			color = r << 24 | g << 16 | b << 8 | 0xff;
		}

		/**
		 * Checks if given Color is the same like currently selected
		 * @param color color in rgb value
		 * @return true, if colors match in rgb value
		 */
		public boolean sameColor (int color) {
			return this.color == color;
		}
	}

    /**
     * Declaration of Putzi for the current level
     */
    public Putzi putzi;

    /**
     * Declaration of sharks for the current level
     */
    public Sharks sharks;

    /**
     * Declaration of shell power ups for the current level
     */
    public Array<Shell> shells;

    /**
     * Declaration of snail power ups for the current level
     */
    public Array<Snail> snails;

    /**
     * Declaration of crab power ups for the current level
     */
    public Array<Crab> crabs;

    /**
     * Declaration of dirt items for the current level
     */
    public Array<Dirt> dirts;

    /**
     * Declaration of wall blocks for the current level
     */
    public Array<Wall> walls;

    /**
     * Declaration of fore- and background
     */
    public ParallaxImages parallaxImages;

	private int levelNumber;

	private boolean isEnhancedLevel;
	private int numberOfSharks;


	/**
	 * Method to initialize a Level, depending on the delivered levelnumber
	 * @param levelNumber the Level Number which will be loaded
	 */
	public Level (int levelNumber) {

		this.levelNumber = levelNumber;
		this.setEnhancedLevel(levelNumber >= Constants.MIN_LEVEL_FOR_SHARKS_FROM_BOTH_DIRECTIONS);
		init();

	}

	/**
     * initializing the Level by instantiating all objects and parsing the corresponding level picture
     */
	private void init () {

		putzi = null;

		setNumberOfSharks(Constants.NUMBER_OF_SHARKS[levelNumber - 1]);

		shells = new Array<>();
		snails = new Array<>();
		crabs = new Array<>();
		dirts = new Array<>();
		walls = new Array<>();
		sharks = new Sharks(getNumberOfSharks(), getEnhancedLevel());

		parallaxImages = new ParallaxImages();


		parseLevel();

	}


	/**
	 * parsing the level with the current levelnumber
     * for each Pixel Color the corresponding BLOCKTYPE Object will be created
     * and positioned regarding the position in levelgraphic
	 */
	private void parseLevel() {

		String filename = "levels/level-0" + levelNumber + ".png";

		Pixmap pixmap = new Pixmap(Gdx.files.internal(filename));

		for (int pixelY = 0; pixelY < pixmap.getHeight(); pixelY++) {

			for (int pixelX = 0; pixelX < pixmap.getWidth(); pixelX++) {

				AbstractGameObject obj;

				float pixelYInv = (float) pixmap.getHeight() - pixelY;
				int currentPixel = pixmap.getPixel(pixelX, pixelY);

				if (BLOCK_TYPE.EMPTY.sameColor(currentPixel)) {

					// (background)

				}

				else if (BLOCK_TYPE.PLAYER.sameColor(currentPixel)) {

					obj = new Putzi();
					obj.position.set(pixelX, pixelYInv);
					putzi = (Putzi) obj;

				}

				else if (BLOCK_TYPE.DIRT.sameColor(currentPixel)) {

					obj = new Dirt();
					obj.position.set(pixelX, pixelYInv);
					dirts.add((Dirt) obj);

				}

				else if (BLOCK_TYPE.WALL.sameColor(currentPixel)) {

					obj = new Wall();
					obj.position.set(pixelX, pixelYInv);
					walls.add((Wall) obj);

				}

				else if (BLOCK_TYPE.SHELL.sameColor(currentPixel)) {

					obj = new Shell();
					obj.position.set(pixelX, pixelYInv);
					shells.add((Shell) obj);

				}

				else if (BLOCK_TYPE.SNAIL.sameColor(currentPixel)) {

					obj = new Snail();
					obj.position.set(pixelX, pixelYInv);
					snails.add((Snail) obj);

				}

				else if (BLOCK_TYPE.CRAB.sameColor(currentPixel)) {

					obj = new Crab();
					obj.position.set(pixelX, pixelYInv);
					crabs.add((Crab) obj);

				}

				else {

					int r = 0xff & (currentPixel >>> 24);
					int g = 0xff & (currentPixel >>> 16);
					int b = 0xff & (currentPixel >>> 8);
					int a = 0xff & currentPixel;

					Gdx.app.error(TAG, "Unknown object at x<" + pixelX + "> y<" + pixelY + ">: r<" + r + "> g<" + g + "> b<" + b + "> a<" + a + ">");

				}

			}

		}

		pixmap.dispose();

	}


	/**
	 * Updating Objects each Frame
	 * @param deltaTime time a frame lasts
	 */
	public void update (float deltaTime) {

		putzi.update(deltaTime);

		for (Shell shell : shells)	{
			shell.update(deltaTime);
		}
		for (Dirt dirt : dirts) {
		    dirt.update(deltaTime);
        }
		for (Snail snail : snails)	{
		    snail.update(deltaTime);
        }
		for (Crab crab : crabs) {
		    crab.update(deltaTime);
        }

		sharks.update(deltaTime);

	}


	/**
     * Abstract method to render the game objects (Decoration, Items, Putzi and Sharks)
     * on each call of the game loop
     * @param batch The sprite batch for all actions and data, sent together to the GPU
	 */
	public void render (SpriteBatch batch) {

		parallaxImages.renderParallaxBackground(batch);

		for (Shell shell : shells)	{
		    shell.render(batch);
        }
		for (Snail snail : snails) {
            snail.render(batch);
        }
		for (Crab crab : crabs) {
            crab.render(batch);
        }
		for (Dirt dirt : dirts) {
            dirt.render(batch);
        }
		for (Wall wall : walls) {
            wall.render(batch);
        }

		putzi.render(batch);
		sharks.render(batch);

		parallaxImages.renderParallaxForeground(batch, levelNumber);

	}


	/**
	 * getter method returning isEnhancedLevel
	 * @return true or false depending on whether sharks are coming from both sides
	 */
	public boolean getEnhancedLevel() {

		return isEnhancedLevel;

	}


	/**
	 * setter method for isEnhancedLevel
	 * @param enhancedLevel boolean that indicates whether sharks are coming from both sides
	 */
	public void setEnhancedLevel(boolean enhancedLevel) {

		isEnhancedLevel = enhancedLevel;

	}

	/**
	 * getter method returning numberOfSharks
	 * @return the number of randomly generated sharks
	 */
	public int getNumberOfSharks() {

		return numberOfSharks;

	}


	/**
	 * setter method for numberOfSharks
	 * @param numberOfSharks the number of randomly generated sharks
	 */
	public void setNumberOfSharks(int numberOfSharks) {

		this.numberOfSharks = numberOfSharks;

	}

}
