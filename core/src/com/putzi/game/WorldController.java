package com.putzi.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Disposable;

import com.putzi.game.objects.Putzi;
import com.putzi.screens.GameOverScreen;
import com.putzi.screens.transitions.ScreenTransition;
import com.putzi.screens.transitions.ScreenTransitionFade;
import com.putzi.screens.transitions.ScreenTransitionSlide;
import com.putzi.util.CameraHelper;
import com.putzi.util.CollisionManager;
import com.putzi.util.Constants;
import com.putzi.util.DatabaseConnector;
import com.putzi.util.UserManagement;


/**
 * The WorldController runs and defines everything game related,
 * e.g. Levelinitialization, Key Interactions, Camera, ...
 */
public class WorldController extends InputAdapter implements Disposable {


    private static final String TAG = WorldController.class.getName();

    private ExtendedGame game;

    /**
     * Declaration of Level
     */
    public Level level;
    private int levelNumber;

    private int lives;
    private float livesVisual;

    private int score;
    private float scoreVisual;

    private int dirtCount;

    private boolean levelPaused;
    private boolean levelWon;

    private boolean levelFinishingStarted;
    private boolean gameFinishingStarted;
    private boolean transitionStarted;
    private boolean preTransitionStarted;

    private float levelFinishedDelay;
    private float gameWinningDelay;
    private float gameLoosingDelay;

    private float accelerationFactor;
    private float terminalVelocityFactor;

    private float freeCameraPowerUpTimer;
    private float powerBoostTimer;
    private float biteAnimationTimer;

    private boolean leftKeyPressed;
    private boolean rightKeyPressed;
    private boolean upKeyPressed;
    private boolean downKeyPressed;

    /**
     * Declaration of CameraHelper, used for navigation in the level
     */
    public CameraHelper cameraHelper;
    private CollisionManager collisionManager;

    private static boolean gameIsRunning = false;

    /**
     * WorldController Constructor
     * @param game forwarded Game from AbstractScreen
     */
    public WorldController (ExtendedGame game) {
        this.game = game;
        init();
    }


    /**
     * Worldcontroller Initialization
     */
    private void init () {

        cameraHelper = new CameraHelper();
        resetLevelNumber();
        resetScore();
        setScoreVisual(getScore());
        initLevel(getLevelNumber());
    }

    /**
     * Initializing variables for new Level
     * @param levelNumber The upcoming Level as Integer
     */
    private void initLevel (int levelNumber) {

        if (levelNumber == Constants.STARTING_LEVEL) {
            setLives(Constants.PUTZI_LIVES_START);
        } else {
            incrementLevelNumber();
        }

        setLivesVisual(getLives());

        setGameIsRunning(true);

        levelFinishedDelay = Constants.TIME_DELAY_LEVEL_FINISHED;
        gameWinningDelay = Constants.TIME_DELAY_GAME_FINISHED;
        gameLoosingDelay = Constants.TIME_DELAY_GAME_FINISHED;

        accelerationFactor = 1;
        terminalVelocityFactor = 1;

        setFreeCameraPowerUpTimer(0.0f);
        setPowerBoostTimer(0.0f);

        setLevelPaused(false);
        setLevelWon(false);

        levelFinishingStarted = false;
        gameFinishingStarted = false;
        transitionStarted = false;
        preTransitionStarted = false;


        level = new Level(levelNumber);
        collisionManager = new CollisionManager(this, level);

        cameraHelper.setTarget(level.putzi);

        setDirtCount(level.dirts.size);

        Assets.instance.putzi.putziAnimationSwim.setFrameDuration(Constants.PUTZI_LONG_FRAMEDURATION);

    }


    /**
     * Conditions and Method Callings for each frame
     * @param deltaTime Time between now and last time called
     */
    public void update (float deltaTime) {

        setCameraControls(deltaTime);

        if (!isLevelPaused()) {
            updatePowerUpTimer(deltaTime);
            updateHUD(deltaTime);
            updateLevel(deltaTime);
            updatePutziInteraction(deltaTime);

            velocityChangeSharksBeforeTransition();

            handleLevelWinning(deltaTime);
            handleGameWinning(deltaTime);
            handleGameLoosing(deltaTime);

        }

    }


    /**
     * Calculates and updates Time leftovers of FreeCameraPowerUpTimer and biteAnimationTimer
     * @param deltaTime Time between now and last time called
     */
    private void updatePowerUpTimer(float deltaTime) {

        if (getFreeCameraPowerUpTimer() > 0) {
            cameraHelper.setTarget(null);
            setFreeCameraPowerUpTimer(getFreeCameraPowerUpTimer() - deltaTime);
            level.putzi.isProtectedBlue = true;
            level.sharks.slowDownSharks();
        } else {
            cameraHelper.setTarget(level.putzi);
            level.putzi.isProtectedBlue = false;
            level.sharks.speedUpSharks();
        }

        if (getBiteAnimationTimer() > 0) {
            setBiteAnimationTimer(getBiteAnimationTimer() - deltaTime);
            level.putzi.isBitten = true;
        } else {
            level.putzi.isBitten = false;
        }
    }

    /**
     * Update Level, Camera- and Parallax Positions
     * @param deltaTime Time between now and last time called
     */
    private void updateLevel(float deltaTime) {

        level.update(deltaTime);
        cameraHelper.update(deltaTime);
        level.parallaxImages.updateScrollPosition(cameraHelper.getPosition());
    }


    /**
     * Update Head Up Display
     * @param deltaTime Time between now and last time called
     */
    private void updateHUD(float deltaTime) {

        // Helpervariables for Animations
        // (delayed change of values)
        if (getLivesVisual() > getLives()) {
            setLivesVisual(Math.max(getLives(), getLivesVisual() - 1 * deltaTime));
        }

        if (getScoreVisual() < getScore()) {
            setScoreVisual(Math.min(getScore(), getScoreVisual() + 250 * deltaTime));
        } else if (scoreVisual > getScore()) {
            setScoreVisual(Math.max(getScore(), getScoreVisual() - 250 * deltaTime));
        } else {
            setScoreVisual(getScore());
        }

        if (getScore() < 0) {
            resetScore();
        }
    }


    /**
     * Update Collisiontests and PlayerControls
     * @param deltaTime Time between now and last time called
     */
    private void updatePutziInteraction(float deltaTime) {
        if (!isLevelWon() && !isGameWon() && !isGameOver()) {
            setPlayerControls(deltaTime);
            collisionManager.testCollisions();
        }
    }

    /**
     * Handles Sharks to slow down when Putzis Interactions are disabled
     */
    private void velocityChangeSharksBeforeTransition() {
        if ((isLevelWon() || isGameWon() || isGameOver()) && !preTransitionStarted ) {
            preTransitionStarted = true;
            setFreeCameraPowerUpTimer(Constants.PRE_TRANSITION_FREE_CAMERA_TIME);
        }
    }


    /**
     * Handling if level is finished, results in next level initiation or finishing the game
     * @param deltaTime Time between now and last time called
     */
    private void handleLevelWinning(float deltaTime) {
        if (isLevelWon() && !isFinalLevel()) {

            levelFinishingOneTimeRun();

            levelFinishedDelay -= deltaTime;
            if (levelFinishedDelay < 0) {
                initLevel(getLevelNumber()+1);
            }
        }
    }


    private void levelFinishingOneTimeRun() {

        if (!levelFinishingStarted) {
            levelFinishingStarted = true;
            addScore(Constants.LEVEL_COMPLETED_BONUS);
        }
    }

    private void handleGameWinning(float deltaTime) {
        if (isLevelWon() && isFinalLevel()) {

            gameWinningOneTimeRun();
            gameWinningDelay -= deltaTime;
            if (gameWinningDelay < 0 && !transitionStarted) {
                transitionStarted = true;
                transitionToGameOverScreen();
            }
        }
    }

    private void gameWinningOneTimeRun() {
        if (!gameFinishingStarted) {
            gameFinishingStarted = true;
            addScore(Constants.LEVEL_COMPLETED_BONUS);
            if (DatabaseConnector.instance.isConnected() &&
                    null != UserManagement.getUser() &&
                    UserManagement.instance.getHighscore(UserManagement.getUser()) < getScore()) {
                UserManagement.instance.setHighscore(getScore());
            }
        }
    }


    /**
     * If all lives are used up or the last level is finished, the Game switches to GameOverScreen
     * @param deltaTime Time between now and last time called
     */
    private void handleGameLoosing(float deltaTime) {

        if (isGameOver()) {

            gameLoosingDelay -= deltaTime;
            if (gameLoosingDelay < 0 && !transitionStarted) {
                transitionStarted = true;
                transitionToGameOverScreen();
            }
        }
    }


    /**
     * KeyMapping for Camera Control
     * @param deltaTime Time between now and last time called
     */
    private void setCameraControls (float deltaTime) {

        if (!cameraHelper.hasTarget(level.putzi)) {
            // Direkte Kamerasteuerung (wenn umgeschaltet mit Enter-Taste oder im cameraView-Modus):
            // mit Pfeiltasten oder WASD (5fache Geschwindigkeit bei gedrückter Umschalttaste)

            float camMoveSpeed = deltaTime * Constants.CAMERA_MOVE_SPEED;
            float camMoveSpeedAccelerationFactor = Constants.CAMERA_MOVE_SPEED_ACCELERATION;

            if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)) {
                camMoveSpeed *= camMoveSpeedAccelerationFactor;
            }

            if (Gdx.input.isKeyPressed(Keys.LEFT) || Gdx.input.isKeyPressed(Keys.A)) {
                moveCamera(-camMoveSpeed, 0);
            }

            if (Gdx.input.isKeyPressed(Keys.RIGHT) || Gdx.input.isKeyPressed(Keys.D)) {
                moveCamera(camMoveSpeed, 0);
            }

            if (Gdx.input.isKeyPressed(Keys.UP) || Gdx.input.isKeyPressed(Keys.W)) {
                moveCamera(0, camMoveSpeed);
            }

            if (Gdx.input.isKeyPressed(Keys.DOWN) || Gdx.input.isKeyPressed(Keys.S)) {
                moveCamera(0, -camMoveSpeed);
            }

            if (Gdx.input.isKeyPressed(Keys.BACKSPACE)) {
                cameraHelper.setPosition(0, 0);
            }
        }

        float camZoomSpeed = deltaTime * Constants.CAMERA_ZOOM_SPEED;
        float camZoomSpeedAccelerationFactor = Constants.CAMERA_ZOOM_SPEED_ACCELERATION;

        // Faster Zoom with pressed SHIFT_LEFT)
        if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))   {
            camZoomSpeed *= camZoomSpeedAccelerationFactor;
        }

        if (Gdx.input.isKeyPressed(Keys.COMMA)) {
            cameraHelper.addZoom(camZoomSpeed);
        }

        if (Gdx.input.isKeyPressed(Keys.PERIOD)) {
            cameraHelper.addZoom(-camZoomSpeed);
        }

        if (Gdx.input.isKeyPressed(Keys.SLASH)) {
            cameraHelper.setZoom(1);
        }
    }

    /**
     * Set the Camera to a new given Position
     * @param positionX x-value of Position
     * @param positionY y-value of Position
     */
    private void moveCamera (float positionX, float positionY) {

        float newX = Math.min(Math.max(positionX + cameraHelper.getPosition().x, 15.5f), 64.5f);
        float newY = Math.min(Math.max(positionY + cameraHelper.getPosition().y, 10.2f), 39.7f);

        cameraHelper.setPosition(newX, newY);

    }


    /**
     * Checking for Key-Interactions and call concerning methods or set concerning variables
     * @param deltaTime Time between now and last time called
     */
    private void setPlayerControls (float deltaTime) {

        leftKeyPressed = false;
        rightKeyPressed = false;
        upKeyPressed = false;
        downKeyPressed = false;

        if (cameraHelper.hasTarget(level.putzi)) {

            handlePowerBoost(deltaTime);

            if (Gdx.input.isKeyPressed(Keys.LEFT) || Gdx.input.isKeyPressed(Keys.A)) {
                leftKeyPressed = true;
                level.putzi.velocity.x -= accelerationFactor * level.putzi.acceleration.x * deltaTime;
                level.putzi.velocity.x = Math.max(level.putzi.velocity.x, - terminalVelocityFactor * level.putzi.terminalVelocity.x);
                level.putzi.viewDirection = Putzi.VIEW_DIRECTION.LEFT;
            }

            if (Gdx.input.isKeyPressed(Keys.RIGHT) || Gdx.input.isKeyPressed(Keys.D)) {
                rightKeyPressed = true;
                level.putzi.velocity.x += accelerationFactor * level.putzi.acceleration.x * deltaTime;
                level.putzi.velocity.x = Math.min(level.putzi.velocity.x, terminalVelocityFactor * level.putzi.terminalVelocity.x);
                level.putzi.viewDirection = Putzi.VIEW_DIRECTION.RIGHT;
            }

            if (Gdx.input.isKeyPressed(Keys.UP) || Gdx.input.isKeyPressed(Keys.W)) {
                upKeyPressed = true;
                level.putzi.velocity.y += accelerationFactor * level.putzi.acceleration.y * deltaTime;
                level.putzi.velocity.y = Math.min(level.putzi.velocity.y, terminalVelocityFactor * level.putzi.terminalVelocity.y);
            }

            if (Gdx.input.isKeyPressed(Keys.DOWN) || Gdx.input.isKeyPressed(Keys.S)) {
                downKeyPressed = true;
                level.putzi.velocity.y -= accelerationFactor * level.putzi.acceleration.y * deltaTime;
                level.putzi.velocity.y = Math.max(level.putzi.velocity.y, - terminalVelocityFactor * level.putzi.terminalVelocity.y);
            }
        }
    }

    private void handlePowerBoost(float deltaTime) {
        if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT) && getPowerBoostTimer() > 0) {
            accelerationFactor = 20;
            terminalVelocityFactor = 1.5f;
            setPowerBoostTimer(getPowerBoostTimer() - deltaTime);

        } else {
            accelerationFactor = 1;
            terminalVelocityFactor = 1;
        }
    }


    /**
     * Event-Calling when specific keys are released
     * Rotation of Putzi and InGame Shortcut Keymappings (exit, reset, pause, ...)
     * @param keycode Codes for Mapping Keyboardkeys
     * @return false always
     */
    @Override
    public boolean keyUp (int keycode) {

        // Verschachtelte Logik, da das Loslassen einer Taste unterschiedliche Animationen zur Folge hat,
        // je nachdem, welche weitere Taste gerade gedrückt ist

        deactivateAllRotations();

        setRotationFromUpKey(keycode);
        setRotationFromDownKey(keycode);
        setRotationFromLeftKey(keycode);
        setRotationFromRightKey(keycode);

        // reset game
        if (keycode == Keys.R) {
            init();
            Gdx.app.debug(TAG, "Game world reset");
        }

        // couple/decouple Camera when unpaused, back to Menu when paused
        else if (keycode == Keys.ENTER) {
            if (isLevelPaused()) {
                setLevelPaused(false);
            }
        }

        // (Un)Pause Game
        else if (keycode == Keys.ESCAPE) {
            if (isLevelPaused()) {
                transitionToMenuScreen();
            }
            if (!isLevelPaused()) {
               setLevelPaused(true);
            }
        }


        // Developer Tools: Skip Level
        else if (keycode == Keys.K) {
            setLevelWon(true);
        }

        // Developer Tools: Reduce Putzis lives by One
        else if (keycode == Keys.L) {
            reduceLivesByOne();
        }


        return false;
    }

    private void setRotationFromRightKey(int keycode) {
        if (keycode == Keys.RIGHT || keycode == Keys.D) {

            if (upKeyPressed && leftKeyPressed) {
                level.putzi.rotateUpDiagonally = true;
            }

            if (upKeyPressed && !leftKeyPressed) {
                level.putzi.rotateTop = true;
            }

            if (downKeyPressed && leftKeyPressed) {
                level.putzi.rotateDownDiagonally = true;
            }

            if (downKeyPressed && !leftKeyPressed) {
                level.putzi.rotateBottom = true;
            }

            if (!upKeyPressed && !downKeyPressed && leftKeyPressed) {
                level.putzi.rotateLeft = true;
            }
        }
    }

    private void setRotationFromLeftKey(int keycode) {
        if (keycode == Keys.LEFT || keycode == Keys.A) {

            if (upKeyPressed && rightKeyPressed) {
                level.putzi.rotateUpDiagonally = true;
            }

            if (upKeyPressed && !rightKeyPressed) {
                level.putzi.rotateTop = true;
            }

            if (downKeyPressed && rightKeyPressed) {
                level.putzi.rotateDownDiagonally = true;
            }

            if (downKeyPressed && !rightKeyPressed) {
                level.putzi.rotateBottom = true;
            }

            if (!upKeyPressed && !downKeyPressed && rightKeyPressed) {
                level.putzi.rotateRight = true;
            }
        }
    }

    private void setRotationFromDownKey(int keycode) {
        if (keycode == Keys.DOWN || keycode == Keys.S) {

            if (leftKeyPressed) {
                level.putzi.rotateLeft = true;
            } else if (rightKeyPressed) {
                level.putzi.rotateRight = true;
            } else if (upKeyPressed) {
                level.putzi.rotateTop = true;
            }
        }
    }

    private void setRotationFromUpKey(int keycode) {
        if (keycode == Keys.UP || keycode == Keys.W) {

            if (leftKeyPressed) {
                level.putzi.rotateLeft = true;
            } else if (rightKeyPressed) {
                level.putzi.rotateRight = true;
            } else if (downKeyPressed) {
                level.putzi.rotateBottom = true;
            }
        }
    }

    /**
     * Event-Calling when specific keys are pressed
     * Used to define the Rotation of Putzi
     * @param keycode Codes for Mapping Keyboardkeys
     * @return false always
     */
    @Override
    public boolean keyDown (int keycode) {

        if (keycode == Keys.UP || keycode == Keys.W ||
            keycode == Keys.DOWN || keycode == Keys.S ||
            keycode == Keys.LEFT || keycode == Keys.A ||
            keycode == Keys.RIGHT || keycode == Keys.D) {

            deactivateAllRotations();

        }

        if (keycode == Keys.UP || keycode == Keys.W) {

            keyDownRotationUp();

        } else if (keycode == Keys.DOWN || keycode == Keys.S){

            keyDownRotationDown();

        } else if (keycode == Keys.LEFT || keycode == Keys.A){

            keyDownRotationLeft();

        } else if (keycode == Keys.RIGHT || keycode == Keys.D){

            keyDownRotationRight();
        }

        return false;

    }

    private void keyDownRotationRight() {
        if (upKeyPressed) {
            level.putzi.rotateUpDiagonally = true;
        } else if (downKeyPressed) {
            level.putzi.rotateDownDiagonally = true;
        } else {
            level.putzi.rotateRight = true;
        }
    }

    private void keyDownRotationLeft() {
        if (upKeyPressed) {
            level.putzi.rotateUpDiagonally = true;
        } else if (downKeyPressed) {
            level.putzi.rotateDownDiagonally = true;
        } else {
            level.putzi.rotateLeft = true;
        }
    }

    private void keyDownRotationDown() {
        if (leftKeyPressed || rightKeyPressed) {
            level.putzi.rotateDownDiagonally = true;
        } else {
            level.putzi.rotateBottom = true;
        }
    }

    private void keyDownRotationUp() {
        if (leftKeyPressed || rightKeyPressed) {
            level.putzi.rotateUpDiagonally = true;
        } else {
            level.putzi.rotateTop = true;
        }
    }

    private void deactivateAllRotations() {

        level.putzi.rotateTop = false;
        level.putzi.rotateBottom = false;
        level.putzi.rotateUpDiagonally = false;
        level.putzi.rotateDownDiagonally = false;
        level.putzi.rotateLeft = false;
        level.putzi.rotateRight = false;

    }

    /**
     * Do Transition and set Screen to GameOverScreen
     */
    private void transitionToGameOverScreen () {

        ScreenTransition transition = ScreenTransitionFade.init(0.75f);
        Gdx.app.log("Action", "Trying to load profile from " + UserManagement.getUser());
        if (DatabaseConnector.instance.isConnected() &&
                UserManagement.instance.userAlreadyInDB(UserManagement.getUser())) {
            game.setScreen(new GameOverScreen(game), transition);
            Gdx.app.log("Action", "Profile from " + UserManagement.getUser() + " successfully loaded!");
        } else {
            game.setScreen(new GameOverScreen(game), transition);
        }
    }


    /**
     * Do Transition and set Screen to MenuScreen
     */
    private void transitionToMenuScreen() {

        ScreenTransition transition = ScreenTransitionSlide.init(
                0.55f,
                ScreenTransitionSlide.DOWN,
                false,
                Interpolation.bounceOut);
        game.setScreen(new com.putzi.screens.MenuScreen(game), transition);
        game.pause();
    }

    /**
     * Mandatory Method implemented from Disposable
     */
    @Override
    public void dispose () {
        // overridden mandatory Method
    }

    /**
     * Checks if value for Putzi lives is below the Minimun
     * @return boolean
     */
    boolean isGameOver () {
        return getLives() < Constants.MIN_PUTZI_LIVES;
    }


    /* Some small Helper Methods */

    /**
     * Adds value to Score
     * @param score value to add
     */
    public void addScore(int score) {
        this.score += score;
    }

    /**
     * Set Score to initial 0
     */
    private void resetScore() {
        this.score = 0;
    }

    /**
     * Set Level Number to initial Starting Level Value from Constants
     */
    private void resetLevelNumber() {
        this.levelNumber = Constants.STARTING_LEVEL;
    }

    /**
     * Increases the LevelNumber Value by 1
     */
    private void incrementLevelNumber() {
        this.levelNumber++;
    }

    /**
     * Decreases the Lives Value by 1
     */
    public void reduceLivesByOne() {
        lives--;
    }


    /**
     * Checks if the current Level is the maximum Level (corresponding to setting in constants)
     * @return true if level is final level
     */
    private boolean isFinalLevel() { return levelNumber == Constants.MAX_LEVEL_NUMBER;}

    /**
     * Checks if the Game is won, by checking if the level is won and it is the final level
     * @return true if Game is won
     */
    public boolean isGameWon() {
        return isLevelWon() && isFinalLevel();
    }

	/* Getter and Setter */
    public int getLevelNumber() {
        return levelNumber;
    }

    public int getLives() {
        return lives;
    }
    public void setLives(int lives) {
        this.lives = lives;
    }

    public int getScore() {
        return score;
    }
    public float getScoreVisual() {
        return scoreVisual;
    }
    public void setScoreVisual(float scoreVisual) {
        this.scoreVisual = scoreVisual;
    }

    public float getLivesVisual() {
        return livesVisual;
    }
    public void setLivesVisual(float livesVisual) {
        this.livesVisual = livesVisual;
    }

    public boolean isLevelWon() {
        return levelWon;
    }
    public void setLevelWon(boolean levelWon) {
        this.levelWon = levelWon;
    }

    public boolean isLevelPaused() {
        return levelPaused;
    }
    public void setLevelPaused(boolean levelPaused) {
        this.levelPaused = levelPaused;
    }

    public int getDirtCount() {
        return dirtCount;
    }
    public void setDirtCount(int dirtCount) {
        this.dirtCount = dirtCount;
    }

    public float getFreeCameraPowerUpTimer() {
        return freeCameraPowerUpTimer;
    }
    public void setFreeCameraPowerUpTimer(float freeCameraPowerUpTimer) {
        this.freeCameraPowerUpTimer = freeCameraPowerUpTimer;
    }

    public float getPowerBoostTimer() {
        return powerBoostTimer;
    }
    public void setPowerBoostTimer(float powerBoostTimer) {
        this.powerBoostTimer = powerBoostTimer;
    }

    public float getBiteAnimationTimer() {
        return biteAnimationTimer;
    }
    public void setBiteAnimationTimer(float biteAnimationTimer) {
        this.biteAnimationTimer = biteAnimationTimer;
    }

    public static boolean isGameIsRunning() {
        return gameIsRunning;
    }
    public static void setGameIsRunning(boolean gameIsRunning) {
        WorldController.gameIsRunning = gameIsRunning;
    }
}
