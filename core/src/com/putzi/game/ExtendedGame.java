package com.putzi.game;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.GLFrameBuffer;
import com.putzi.screens.AbstractGameScreen;
import com.putzi.screens.transitions.ScreenTransition;

/**
 * This superclass of PutziMain enables Transition between Screens (compared with its AbstractGameScreen)
 */
public abstract class ExtendedGame implements ApplicationListener {

	private boolean isInitialized;

	private AbstractGameScreen currentScreen;
	private AbstractGameScreen nextScreen;

	private FrameBuffer currentFrameBuffer;
	private FrameBuffer nextFrameBuffer;

	private SpriteBatch spriteBatch;
	private ScreenTransition screenTransition;
	private float transitionTime;

	/**
	 * Instantiation of the Assetmanager, which loads and manages all assets
	 */
	public AssetManager manager = new AssetManager();

	/**
	 * Change to another screen without animation
	 * @param screen the screen where to go
	 */
	public void setScreen (AbstractGameScreen screen) {
		setScreen(screen, null);
	}

	/**
	 * Change to another screen with animation
	 * @param screen the screen where to go
	 * @param screenTransition the transition used while switching to the next screen
	 */
	public void setScreen (AbstractGameScreen screen, ScreenTransition screenTransition) {

		int viewportWidth = Gdx.graphics.getWidth();
		int viewportHeight = Gdx.graphics.getHeight();

		// initialize frame buffer
		if (!isInitialized) {

			GLFrameBuffer.FrameBufferBuilder frameBufferBuilder = new GLFrameBuffer.FrameBufferBuilder(viewportWidth, viewportHeight);
			frameBufferBuilder.addColorTextureAttachment(GL30.GL_RGB8, GL30.GL_RGB, GL30.GL_UNSIGNED_BYTE);

			currentFrameBuffer = frameBufferBuilder.build();
			nextFrameBuffer = frameBufferBuilder.build();

			spriteBatch = new SpriteBatch();
			isInitialized = true;
			
		}

		// start transition
		nextScreen = screen;
		nextScreen.show();
		nextScreen.resize(viewportWidth, viewportHeight);
		nextScreen.render(0);                                    // nextScreen: update once
		
		if (currentScreen != null) {
			currentScreen.pause();
		}
		
		nextScreen.pause();
		Gdx.input.setInputProcessor(null);                       // deactivate inputs for safety
		this.screenTransition = screenTransition;
		transitionTime = 0;
		
	}


	/**
	 * Method called by the game loop from the application every time rendering should be performed.
	 * Game logic updates are usually also performed in this method.
	 */
	@Override
	public void render () {
		
        // limit FPS to 60
        float deltaTime = Math.min(Gdx.graphics.getDeltaTime(), 1.0f / 60.0f);

		if (nextScreen == null) {
			if (currentScreen != null) {
				currentScreen.render(deltaTime);
			}

		} else {
			float duration = 0;
			if (screenTransition != null) {
				duration = screenTransition.getDuration();
			}

			transitionTime = Math.min(transitionTime + deltaTime, duration);
			if (screenTransition == null || transitionTime >= duration) {
				if (currentScreen != null) {
					currentScreen.hide();
				}

				Gdx.input.setInputProcessor(nextScreen.getInputProcessor());

				currentScreen = nextScreen;
				nextScreen = null;
				screenTransition = null;

			} else {

				// first render screens into FrameBuffer objects ...
				currentFrameBuffer.begin();
				if (currentScreen != null) {
					currentScreen.render(deltaTime);
				}

				currentFrameBuffer.end();
				nextFrameBuffer.begin();
				nextScreen.render(deltaTime);
				nextFrameBuffer.end();

				// ... then render the transition

				float alpha = transitionTime / duration;
				screenTransition.render(
					spriteBatch,
					currentFrameBuffer.getColorBufferTexture(),
					nextFrameBuffer.getColorBufferTexture(),
					alpha
				);
			}
		}
	}

	/**
	 * This method is called every time the game screen is re-sized and the game is not in the paused state.
	 * The parameters are the new width and height the screen has been resized to in pixels.
	 * @param width is the new width the screen has been resized to in pixels
	 * @param height is the new height the screen has been resized to in pixels
	 */
	@Override
	public void resize (int width, int height) {

		if (currentScreen != null) {
			currentScreen.resize(width, height);
		}

		if (nextScreen != null) {
			nextScreen.resize(width, height);
		}
	}

	/**
	 * Called when the Application is paused, usually when it's not active or visible on screen
	 */
	@Override
	public void pause () {

		if (currentScreen != null) {
			currentScreen.pause();
		}
	}

	/**
	 * Called when the Application is resume after a pause (screen unactive or not visible)
	 */
	@Override
	public void resume () {

		if (currentScreen != null) {
			currentScreen.resume();
		}
	}

	/**
	 * Called to free the memory
	 */
	@Override
	public void dispose () {

		if (currentScreen != null) {
			currentScreen.hide();
		}

		if (nextScreen != null) {
			nextScreen.hide();
		}

		if (isInitialized) {

			currentFrameBuffer.dispose();
			currentScreen = null;

			nextFrameBuffer.dispose();
			nextScreen = null;

			spriteBatch.dispose();

			isInitialized = false;

		}

	}

}
