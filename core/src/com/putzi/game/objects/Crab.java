package com.putzi.game.objects;


import com.putzi.game.Assets;



/**
 * Class for the Crab object
 */
public class Crab extends BonusObject {



    /**
     * Constructor for the Crab object
     * Sets the crab's specific animation
     */
    public Crab() {

        super();
        setAnimation(Assets.instance.crab.animCrab);

    }

}
