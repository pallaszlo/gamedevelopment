package com.putzi.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import com.putzi.game.Assets;


/**
 * Class for the foreground and background planes with parallax movement
 */
public class ParallaxImages {


	private static final String TAG = ParallaxImages.class.getName();

	private TextureRegion region;
	private Vector2 position;
	private Array<TextureRegion> foregroundTextureRegions;
	private Array<TextureRegion> backgroundTextureRegions;



	public ParallaxImages() {

		position = new Vector2();

		foregroundTextureRegions = new Array<TextureRegion>(3);
		foregroundTextureRegions.add(Assets.instance.levelDecoration.foreground_0_game);
		foregroundTextureRegions.add(Assets.instance.levelDecoration.foreground_1_game);
		foregroundTextureRegions.add(Assets.instance.levelDecoration.foreground_2_game);

		backgroundTextureRegions = new Array<TextureRegion>(4);
		backgroundTextureRegions.add(Assets.instance.levelDecoration.background_0_game);
		backgroundTextureRegions.add(Assets.instance.levelDecoration.background_1_game);
		backgroundTextureRegions.add(Assets.instance.levelDecoration.background_2_game);
		backgroundTextureRegions.add(Assets.instance.levelDecoration.background_3_game);

	}



	/**
	 * Method for updating the scroll position of the parallax planes
	 * @param cameraPosition The camera position as a reference point for the plane's positions
	 */
	public void updateScrollPosition (Vector2 cameraPosition) {

		position.set(cameraPosition.x, cameraPosition.y);

	}



	private void drawOverlay (SpriteBatch spriteBatch, int gameLevelNumber) {

		switch (gameLevelNumber) {

			case 1: region = Assets.instance.levelDecoration.level_1_overlay;
				break;

			case 2: region = Assets.instance.levelDecoration.level_2_overlay;
				break;

			case 3: region = Assets.instance.levelDecoration.level_3_overlay;
				break;

			default: Gdx.app.log(TAG, "No overlay image for " + gameLevelNumber + " available!");

		}

		spriteBatch.draw(
				region.getTexture(),
				0,
				1,
				0,
				0,
				80,
				48,
				1,
				1,
				0,
				region.getRegionX(),
				region.getRegionY(),
				region.getRegionWidth(),
				region.getRegionHeight(),
				false,
				false
		);

	}



	private void drawParallaxImage (SpriteBatch spriteBatch,
									Array<TextureRegion> textureRegions,
									int textureLevel,
									float parallaxRelativeToWorld) {

		float parallaxRelativeToCamera = 1 - parallaxRelativeToWorld;

		spriteBatch.draw(
				textureRegions.get(textureLevel),
				parallaxRelativeToWorld * (position.x - 15),
				parallaxRelativeToWorld * (position.y - 10.3f) + 1,
				30.7f + parallaxRelativeToCamera * 49.5f,
				19 + parallaxRelativeToCamera * 29.5f
		);

	}


	/**
	 * Method to render the foreground planes on each call of the game loop,
	 * according to the current game level
	 * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
	 * @param gameLevelNumber The number of the current game level
	 */
	public void renderParallaxForeground (SpriteBatch spriteBatch, int gameLevelNumber) {

		drawOverlay(spriteBatch, gameLevelNumber);
		drawParallaxImage(spriteBatch, foregroundTextureRegions, 0, -0.2f);
		drawParallaxImage(spriteBatch, foregroundTextureRegions, 1, -0.4f);
		drawParallaxImage(spriteBatch, foregroundTextureRegions, 2, -0.6f);

	}


	/**
	 * Method to render the background planes on each call of the game loop
	 * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
	 */
	public void renderParallaxBackground (SpriteBatch spriteBatch) {

		drawParallaxImage(spriteBatch, backgroundTextureRegions, 0, 0.8f);
		drawParallaxImage(spriteBatch, backgroundTextureRegions, 1, 0.75f);
		drawParallaxImage(spriteBatch, backgroundTextureRegions, 2, 0.6f);
		drawParallaxImage(spriteBatch, backgroundTextureRegions, 3, 0.3f);

	}

}
