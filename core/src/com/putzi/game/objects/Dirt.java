package com.putzi.game.objects;


import com.putzi.game.Assets;



/**
 * Class for the Dirt object
 */
public class Dirt extends BonusObject {


    /**
     * Constructor for the Dirt object
     * Sets the dirt's specific animation
     */
    public Dirt() {

        super();
        dimension.set(2, 2);
        setAnimation(Assets.instance.dirt.animDirt);

    }

}
