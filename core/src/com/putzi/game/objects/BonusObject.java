package com.putzi.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;


/**
 * Class for all bonus objects
 *
 * Provides methods for the common wave animations
 */
public abstract class BonusObject extends AbstractGameObject {

    private boolean collected;

    private static final float FLOAT_CYCLE_TIME_X = 3.0f;
    private static final float FLOAT_CYCLE_TIME_Y = 2.9f;

    private float amplitudeX;
    private float amplitudeY;
    private float floatCycleTimeLeftX;
    private float floatCycleTimeLeftY;
    private boolean floatingDownwards;
    private boolean floatingRight;
    private Vector2 floatTargetPosition;



    BonusObject() {

        dimension.set(1, 1);
        stateTime = MathUtils.random(0.0f, 1.0f);

        floatingDownwards = false;
        floatingRight = false;
        floatCycleTimeLeftX = MathUtils.random(0, FLOAT_CYCLE_TIME_X);
        floatCycleTimeLeftY = MathUtils.random(0, FLOAT_CYCLE_TIME_Y);
        amplitudeX = MathUtils.random(0.3f, 1);
        amplitudeY = MathUtils.random(0.3f, 1);
        floatTargetPosition = null;

        setCollected(false);

    }


    /**
     * Method for updating the wave animation of the object
     * @param deltaTime Time between now and last time called
     */
    @Override
    public void update (float deltaTime) {

        super.update(deltaTime);
        wave(deltaTime);

    }


    private void wave(float deltaTime) {

        floatCycleTimeLeftX -= deltaTime;
        floatCycleTimeLeftY -= deltaTime;

        if (floatTargetPosition == null) {

            floatTargetPosition = new Vector2(position);

        }

        if (floatCycleTimeLeftX <= 0) {

            floatCycleTimeLeftX = FLOAT_CYCLE_TIME_X;
            floatingRight = !floatingRight;
            floatTargetPosition.x += amplitudeX * (floatingRight ? -1 : 1);

        }

        if (floatCycleTimeLeftY <= 0) {

            floatCycleTimeLeftY = FLOAT_CYCLE_TIME_Y;
            floatingDownwards = !floatingDownwards;
            floatTargetPosition.y += amplitudeY * (floatingDownwards ? -1 : 1);

        }

        position.lerp(floatTargetPosition, deltaTime);

    }


    /**
     * Abstract method to render the game object's animations on each call of the game loop
     * @param batch The sprite batch for all actions and data, sent together to the GPU
     */
    @Override
    public void render (SpriteBatch batch) {

        if (isCollected()) {
            return;
        }

        TextureRegion region;

        region = (TextureRegion) animation.getKeyFrame(stateTime, true);

        batch.draw(
                region.getTexture(),
                position.x,
                position.y,
                origin.x,
                origin.y,
                dimension.x,
                dimension.y,
                scale.x,
                scale.y,
                rotation,
                region.getRegionX(),
                region.getRegionY(),
                region.getRegionWidth(),
                region.getRegionHeight(),
                false,
                false
        );

    }


    /**
     * Getter method for the collected status of the bonus object
     * @return true if the bonus object was collected
     */
    public boolean isCollected() {

        return collected;

    }


    /**
     /**
     * Setter method for the collected status of the bonus object
     * @param collected A boolean indicating the collected status
     */
    public void setCollected(boolean collected) {

        this.collected = collected;

    }

}
