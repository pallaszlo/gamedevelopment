package com.putzi.game.objects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;


/**
 * Abstract class for game objects
 *
 * Provides some common properties and methods
 */
public abstract class AbstractGameObject {

	/** The x/y position of the game object in game coordinates */
	public Vector2 position;
	/** The x/y dimension of the game object in game coordinates */
	public Vector2 dimension;
	/** The game object's origin relative to its position */
	public Vector2 origin;
	/** The game object's scaling factor */
	public Vector2 scale;
	/** The game object's rotation around the camera axis */
	public float rotation;
	/** The game object's velocity */
	public Vector2 velocity;
	/** A limit for the game object's velocity */
	public Vector2 terminalVelocity;
	/** The game object's friction */
	public Vector2 friction;
	/** The game object's acceleration */
	public Vector2 acceleration;
	/** The current animation of this game object */
	public Animation animation;
	/** The elapsed time in the current animation of this game object */
	public float stateTime;



	protected AbstractGameObject () {

		position = new Vector2();
		dimension = new Vector2(1, 1);
		origin = new Vector2();
		scale = new Vector2(1, 1);
		rotation = 0;
		velocity = new Vector2();
		terminalVelocity = new Vector2(1, 1);
		friction = new Vector2();
		acceleration = new Vector2();

	}


	/**
	 * Method for updating the elapsed time in the current animation (stateTime)
     * @param deltaTime Time between now and last time called
	 */
	public void update (float deltaTime) {

		stateTime += deltaTime;

	}


	/**
	 * Method for setting an animation, starting with its first keyframe
	 * @param animation The name of the animation
	 */
	public void setAnimation (Animation animation) {

		this.animation = animation;
		stateTime = 0;

	}


	/**
     * Abstract method to render the game object on each call of the game loop
	 * @param batch The sprite batch for all actions and data, sent together to the GPU
	 */
	public abstract void render (SpriteBatch batch);

}
