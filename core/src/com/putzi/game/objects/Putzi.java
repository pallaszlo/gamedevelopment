package com.putzi.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import com.putzi.game.Assets;
import com.putzi.util.CharacterSkin;
import com.putzi.util.Constants;
import com.putzi.util.GamePreferences;


/**
 * Class for the Putzi object
 * Provides methods for Putzi's animations
 */
public class Putzi extends AbstractGameObject {


    /**
     * Enum which indicates view direction left or right
     */
    public enum VIEW_DIRECTION {
        LEFT,
        RIGHT
    }

    /** Holds the current view direction */
    public VIEW_DIRECTION viewDirection;
    /** Indicates that Putzi should move up if true */
    public boolean rotateTop;
    /** Indicates that Putzi should move down if true */
    public boolean rotateBottom;
    /** Indicates that Putzi should move up diagonally (according to the view direction) if true */
    public boolean rotateUpDiagonally;
    /** Indicates that Putzi should move down diagonally (according to the view direction) if true */
    public boolean rotateDownDiagonally;
    /** Indicates that Putzi should move left if true */
    public boolean rotateLeft;
    /** Indicates that Putzi should move right if true */
    public boolean rotateRight;
    /** Indicates that Putzi is protected after he has touched a shark from behind */
    public boolean isProtectedGreen;
    /** Indicates that Putzi is protected after collecting a crab - free camera view */
    public boolean isProtectedBlue;
    /** Indicates that Putzi was bitten by a shark */
    public boolean isBitten;
    /** A timer used to debounce Putzi's wall collision sounds */
    public float wallCollisionTimer;
    /** Putzi's raw position without wave motion (used for camera calculations in border areas) */
    public float positionWithoutWaveY;

    private float rotationSpeed;
    private float innerMinAngle;
    private float innerMaxAngle;
    private float outerMaxAngle;
    private float outerMinAngle;

    private float previousWaveValue;
    private float sumWaveDifferences;
    private int previousViewDirectionFactor;
    private boolean isNotTurning;
    private boolean flipSprite;
    private float storedRotation;
    private boolean freeRotationAngle;
    private int numberOfViewDirectionChanges;

    private boolean isMoving;
    private boolean wasMovingInPreviousFrame;


    public Putzi() {

        init();

    }


    /**
     *
     */
    private void init () {

        dimension.set(2, 2);
        origin.set(dimension.x/2, dimension.y/2);

        setAnimation(Assets.instance.putzi.putziAnimationSwim);

        terminalVelocity.set(Constants.PUTZI_MAX_SPEED);
        friction.set(Constants.PUTZI_FRICTION);
        acceleration.set(Constants.PUTZI_ACCELERATION);

        rotationSpeed = Constants.PUTZI_ROTATION_SPEED;
        innerMaxAngle = Constants.PUTZI_ROTATION_INNER_MAX_ANGLE;
        innerMinAngle = Constants.PUTZI_ROTATION_INNER_MIN_ANGLE;
        outerMaxAngle = Constants.PUTZI_ROTATION_OUTER_MAX_ANGLE;
        outerMinAngle = Constants.PUTZI_ROTATION_OUTER_MIN_ANGLE;

        viewDirection = VIEW_DIRECTION.RIGHT;
        previousViewDirectionFactor = 1;
        previousWaveValue = 0;

        rotateTop = false;
        rotateBottom = false;
        rotateUpDiagonally = false;
        rotateDownDiagonally = false;
        rotateLeft = false;
        rotateRight = false;
        isNotTurning = true;
        flipSprite = false;
        storedRotation = 0;
        freeRotationAngle = true;
        numberOfViewDirectionChanges = 0;
        isMoving = false;
        wasMovingInPreviousFrame = false;

        isProtectedGreen = false;
        isProtectedBlue = false;
        isBitten = false;

    }


    /**
     * This method updates Putzi's animations in each render frame
     * @param deltaTime Time between now and last time called
     */
    @Override
    public void update (float deltaTime) {


        super.update(deltaTime);


        if (rotateTop || rotateUpDiagonally || rotateLeft || rotateDownDiagonally || rotateBottom || rotateRight ) {

            isMoving = true;

        } else {

            isMoving = false;

        }


        if (wallCollisionTimer > 0) {

            wallCollisionTimer -= deltaTime;

        }


        // position changes (velocity determined by keystrokes)
        

        calculateFrictionX(deltaTime);
        calculateFrictionY(deltaTime);

        position.x += velocity.x * deltaTime;
        position.y += velocity.y * deltaTime;

        positionWithoutWaveY = position.y - sumWaveDifferences;


        // add sinus waves (up/down)
        

        long currentTime = System.currentTimeMillis();

        float currentWaveValue = (float) (Math.sin(currentTime / Constants.PUTZI_WAVE_FREQ) * Constants.PUTZI_WAVE_HEIGHT);

        float waveDifference = currentWaveValue - previousWaveValue;

        position.y = position.y + waveDifference;
        previousWaveValue = currentWaveValue;
        sumWaveDifferences += waveDifference;


        // handle rotations (camera axis) and turns (vertical axis)


        int viewDirectionFactor = viewDirection == VIEW_DIRECTION.LEFT ? -1 : 1;

        if (viewDirectionFactor != previousViewDirectionFactor) {

            numberOfViewDirectionChanges++;

        }


        if (numberOfViewDirectionChanges > 0) {

            replaceAnimations();

        }


        if (isNotTurning) {

            handleRotationsAroundCameraAxis (deltaTime, viewDirectionFactor);

            if (!wasMovingInPreviousFrame && isMoving) {

                changeSwimMovementSpeed(Constants.PUTZI_SHORT_FRAMEDURATION);

            }

            if (wasMovingInPreviousFrame && !isMoving) {

                changeSwimMovementSpeed(Constants.PUTZI_LONG_FRAMEDURATION);

            }

            wasMovingInPreviousFrame = isMoving;

        }


        previousViewDirectionFactor = viewDirectionFactor;


    }



    private void replaceAnimations() {

        if (isNotTurning) {

            setRotationsAroundVerticalAxis();

        } else {

            if (animation.isAnimationFinished(stateTime)) {

                if (numberOfViewDirectionChanges % 2 == 0) {

                    if (!freeRotationAngle) {
                        rotation = -storedRotation;
                    }

                    setRotationsAroundVerticalAxis();
                    flipSprite = !flipSprite;
                    numberOfViewDirectionChanges = 1;

                } else {

                    setAnimation(Assets.instance.putzi.putziAnimationSwim);
                    numberOfViewDirectionChanges = 0;
                    flipSprite = !flipSprite;
                    isNotTurning = true;

                    if (!freeRotationAngle) {
                        rotation = -storedRotation;
                    }

                }

            }

        }

    }



    private void setRotationsAroundVerticalAxis() {

        if (rotation > 55 && viewDirection == VIEW_DIRECTION.LEFT || rotation < -55 && viewDirection == VIEW_DIRECTION.RIGHT ) {

            setAnimation(Assets.instance.putzi.putziAnimationTurnLeft60DegUp);
            storeAndSetRotations();

        } else if (rotation > 55 && viewDirection == VIEW_DIRECTION.RIGHT || rotation < -55 && viewDirection == VIEW_DIRECTION.LEFT ){

            setAnimation(Assets.instance.putzi.putziAnimationTurnLeft60DegDown);
            storeAndSetRotations();

        } else if ((rotation > 25 && rotation < 35) && viewDirection == VIEW_DIRECTION.LEFT || (rotation < -25 && rotation > -35) && viewDirection == VIEW_DIRECTION.RIGHT) {

            setAnimation(Assets.instance.putzi.putziAnimationTurnLeft30DegUp);
            storeAndSetRotations();

        } else if ((rotation > 25 && rotation < 35) && viewDirection == VIEW_DIRECTION.RIGHT || (rotation < -25 && rotation > -35) && viewDirection == VIEW_DIRECTION.LEFT) {

            setAnimation(Assets.instance.putzi.putziAnimationTurnLeft30DegDown);
            storeAndSetRotations();

        } else {

            setAnimation(Assets.instance.putzi.putziAnimationTurnLeftHorizontal);
            freeRotationAngle = true;

        }

        isNotTurning = false;

    }

    
    
    private void storeAndSetRotations() {
        
        storedRotation = rotation;
        freeRotationAngle = false;
        rotation = 0;
        
    }



    private void handleRotationsAroundCameraAxis (float deltaTime, int viewDirectionFactor) {

        if (rotateUpDiagonally) {

            if (rotation >= innerMinAngle && rotation <= innerMaxAngle) {

                rotation += rotationSpeed * deltaTime * viewDirectionFactor;
                if (rotation < innerMinAngle) {
                    rotation = innerMinAngle;
                }
                if (rotation > innerMaxAngle) {
                    rotation = innerMaxAngle;
                }

            } else if (rotation >= outerMinAngle && rotation <= innerMinAngle) {

                rotation += rotationSpeed * deltaTime;

            } else if (rotation >= innerMaxAngle && rotation <= outerMaxAngle) {

                rotation -= rotationSpeed * deltaTime;
                if (rotation < innerMaxAngle) {
                    rotation = innerMaxAngle;
                }

            }

        }


        if (rotateTop) {

            if (rotation >= outerMinAngle && rotation <= outerMaxAngle) {

                rotation += rotationSpeed * deltaTime * viewDirectionFactor;
                if (rotation < outerMinAngle) {
                    rotation = outerMinAngle;
                }
                if (rotation > outerMaxAngle) {
                    rotation = outerMaxAngle;
                }

            }

        }


        if (rotateDownDiagonally) {

            if (rotation >= innerMinAngle && rotation <= innerMaxAngle) {

                rotation -= rotationSpeed * deltaTime * viewDirectionFactor;
                if (rotation < innerMinAngle) {
                    rotation = innerMinAngle;
                }
                if (rotation > innerMaxAngle) {
                    rotation = innerMaxAngle;
                }

            } else if (rotation >= outerMinAngle && rotation <= innerMinAngle) {

                rotation += rotationSpeed * deltaTime;

            } else if (rotation >= innerMaxAngle && rotation <= outerMaxAngle) {

                rotation -= rotationSpeed * deltaTime;
                if (rotation < innerMaxAngle) {
                    rotation = innerMaxAngle;
                }

            }

        }


        if (rotateBottom) {

            if (rotation >= outerMinAngle && rotation <= outerMaxAngle) {

                rotation -= rotationSpeed * deltaTime * viewDirectionFactor;
                if (rotation < outerMinAngle) {
                    rotation = outerMinAngle;
                }
                if (rotation > outerMaxAngle) {
                    rotation = outerMaxAngle;
                }

            }

        }


        if (rotateLeft) {

            if (rotation > 0) {

                rotation -= rotationSpeed * deltaTime;

            } else if (rotation < 0) {

                rotation += rotationSpeed * deltaTime;

            }

        }


        if (rotateRight) {

            if (rotation > 0) {

                rotation -= rotationSpeed * deltaTime;

            } else if (rotation < 0) {

                rotation += rotationSpeed * deltaTime;

            }

        }
        
    }



    private void changeSwimMovementSpeed(float newFrameDuration) {

            int currentIndex = animation.getKeyFrameIndex(stateTime);

            animation.setFrameDuration(newFrameDuration);
            stateTime = newFrameDuration * currentIndex;

    }



    private void calculateFrictionX (float deltaTime) {

        if (velocity.x != 0) {

            if (velocity.x > 0) {

                velocity.x = Math.max(velocity.x - friction.x * deltaTime, 0);

            } else {

                velocity.x = Math.min(velocity.x + friction.x * deltaTime, 0);

            }

        }

    }



    private void calculateFrictionY (float deltaTime) {

        if (velocity.y != 0) {

            if (velocity.y > 0) {

                velocity.y = Math.max(velocity.y - friction.y * deltaTime, 0);

            } else {

                velocity.y = Math.min(velocity.y + friction.y * deltaTime, 0);

            }

        }


    }


    /**
     * Method to render Putzi's animations on each call of the game loop
     * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
     */
    @Override
    public void render (SpriteBatch spriteBatch) {


        TextureRegion texture;

        spriteBatch.setColor(CharacterSkin.values()[GamePreferences.instance.charSkin].getColor());

        if (isProtectedGreen) {

            spriteBatch.setColor(0, 1, 0, 1);

        }

        if (isProtectedBlue) {

            spriteBatch.setColor(0, 0, 1, 1);

        }

        if (isBitten) {

            spriteBatch.setColor(1, 0, 0, 1);

        }

        texture = (TextureRegion) animation.getKeyFrame(stateTime, false);

        spriteBatch.draw(
                texture.getTexture(),
                position.x,
                position.y,
                origin.x,
                origin.y,
                dimension.x,
                dimension.y,
                scale.x,
                scale.y,
                rotation,
                texture.getRegionX(),
                texture.getRegionY(),
                texture.getRegionWidth(),
                texture.getRegionHeight(),
                flipSprite,
                false
        );

        spriteBatch.setColor(1, 1, 1, 1);

    }

}
