package com.putzi.game.objects;


import com.putzi.game.Assets;



/**
 * Class for the Snail object
 */
public class Snail extends BonusObject {


    /**
     * Constructor for the Snail object
     * Sets the snail's specific animation
     */
    public Snail() {

        super();
        setAnimation(Assets.instance.snail.animSnail);

    }

}
