package com.putzi.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import com.putzi.game.Assets;
import com.putzi.util.Constants;


/**
 * Class for randomly created sharks
 * Holds an inner Shark class
 */
public class Sharks extends AbstractGameObject {

	/** The array of sharks*/
	public Array<Shark> sharks;
	private int numberOfSharks;
	private boolean isEnhancedLevel;


	/**
	 * Inner class for a single shark
	 */
	public static class Shark extends AbstractGameObject {

		private boolean slowDowned = false;
		private float tintColorRed;
		private float tintColorGreen;

		public Shark () {

            stateTime = MathUtils.random(0.0f, 1.0f);
			tintColorRed = MathUtils.random(0.3f, 0.5f);
			tintColorGreen = tintColorRed + 0.1f;

        }


		/**
		 * Getter Method returning the score decrement if Putzi is bitten by a shark
		 * @return The score decrement if Putzi is bitten by a shark
		 */
		public static int getScore() {

			return Constants.SHARK_BITE_SCORE_DECREMENT;

		}


		/**
		 * Method for updating the shark position
		 * @param deltaTime Time between now and last time called
		 */
		@Override
        public void update(float deltaTime) {

			super.update(deltaTime);
			position.x += velocity.x * deltaTime;
			position.y += velocity.y * deltaTime;

		}


		/**
		 * Abstract method to render a shark on each call of the game loop
		 * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
		 */
		@Override
		public void render (SpriteBatch spriteBatch) {

			spriteBatch.setColor(tintColorRed, tintColorGreen, 1, 1);

			TextureRegion region = (TextureRegion) animation.getKeyFrame(stateTime, true);

			spriteBatch.draw(
				region.getTexture(),
				position.x + origin.x,
				position.y + origin.y,
				origin.x,
				origin.y,
				dimension.x,
				dimension.y,
				scale.x,
				scale.y,
				rotation,
				region.getRegionX(),
				region.getRegionY(),
				region.getRegionWidth(),
				region.getRegionHeight(),
				velocity.x > 0,
				false
			);

			spriteBatch.setColor(1, 1, 1, 1);

		}

	}

	/**
	 * Constructor for the (outer) Sharks class
	 * @param numberOfSharks Number of sharks
	 * @param isEnhancedLevel Indicates whether sharks are coming from both sides
	 */
    public Sharks(int numberOfSharks, boolean isEnhancedLevel) {

		this.numberOfSharks = numberOfSharks;
		this.isEnhancedLevel = isEnhancedLevel;
		init();

	}

	/**
	 *
	 */
	private void init () {

		dimension.set(6.0f, 2.55f);

		sharks = new Array<Shark>(numberOfSharks);
		for (int index = 0; index < numberOfSharks; index++) {
			Shark shark = createShark();
			sharks.add(shark);
		}

	}


	/**
	 * Method for creating a single shark with random position, speed and direction
	 * @return The created shark
	 */
	public Shark createShark () {

		Shark shark = new Shark();
		shark.dimension.set(dimension);

		// Randomizes Shark-Creation. In lower Level (not enhanced) Sharks will set on the left side.
		boolean sharkFromLeft = isEnhancedLevel && MathUtils.randomBoolean();

		float xPos;
		float yPos;
		float xVel;
		float yVel = 0;
        float rand;
		float speedRange;

		if (sharkFromLeft) {
			xPos = Constants.LEFT_SHARK_STARTING_POSITION_X;
			xVel = 1;
		} else {
			xPos = Constants.RIGHT_SHARK_STARTING_POSITION_X;
			xVel = -1;
		}

		rand = MathUtils.random(Constants.SHARK_MIN_VELOCITY, Constants.SHARK_MAX_VELOCITY);
        speedRange = Constants.SHARK_MAX_VELOCITY - Constants.SHARK_MIN_VELOCITY;

        xVel *= rand;
		yPos = MathUtils.random(2.0f, Constants.LEVEL_MAP_HEIGHT - 2.0f);

		shark.position = new Vector2(xPos, yPos);
		shark.velocity.set(xVel, yVel);

		if (rand < Constants.SHARK_MIN_VELOCITY + 0.33f * speedRange) {
			shark.setAnimation(Assets.instance.shark.animSharkSlow);
		} else if (rand < Constants.SHARK_MIN_VELOCITY + 0.66f * speedRange) {
			shark.setAnimation(Assets.instance.shark.animSharkMedium);
		} else {
			shark.setAnimation(Assets.instance.shark.animSharkFast);
		}

        return shark;

	}


	/**
	 * Method for slowing down the shark motion (used in cameraView mode, when Putzi blue)
	 */
    public void slowDownSharks () {
	    for (Shark shark : sharks) {
	    	if (!shark.slowDowned) {
				shark.velocity.set(shark.velocity.x * Constants.SLOW_MOTION_FACTOR, 0);
				shark.slowDowned = true;
			}
        }
    }


	/**
	 *	Method for speeding up the shark motion (after exiting from cameraView)
	 */
    public void speedUpSharks() {
        for (Shark shark : sharks) {
        	if (shark.slowDowned) {
				shark.velocity.set(shark.velocity.x / Constants.SLOW_MOTION_FACTOR, 0);
				shark.slowDowned = false;
			}
        }
    }


	/**
	 * Method for updating the positions, replacing sharks by new ones when they leave the viewport
	 * @param deltaTime Time between now and last time called
	 */
	@Override
	public void update (float deltaTime) {

		// traverse in reverse order, so that changes don't cause  any problems

		for (int index = sharks.size - 1; index >= 0; index--) {

			Shark shark = sharks.get(index);
			shark.update(deltaTime);

			if (shark.position.x < Constants.VALID_SHARK_RANGE_MIN ||
					shark.position.x > Constants.VALID_SHARK_RANGE_MAX) {
				// Shark is removed from Screen (and Index) after leaving Screen on the left Side
				// a new Shark will be added directly after removal at the right Side with the same Index as removed one
				sharks.removeIndex(index);
				sharks.insert(index, createShark());
			}

		}

	}


	/**
	 * Method to render the sharks on each call of the game loop
	 * @param spriteBatch The sprite batch for all actions and data, sent together to the GPU
	 */
	@Override
	public void render (SpriteBatch spriteBatch) {

		for (Shark shark : sharks)
			shark.render(spriteBatch);

	}

}
