package com.putzi.game.objects;


import com.putzi.game.Assets;
import com.putzi.util.Constants;

/**
 * Class for the Shell object
 */
public class Shell extends BonusObject {


    /**
     * Constructor for the Shell object
     * Sets the shell's specific animation
     */
    public Shell() {

        super();
        setAnimation(Assets.instance.shell.animShell);

    }


    /**
     * Getter Method returning the score increment for a collected shell
     * @return The score increment for a collected shell
     */
    public int getScore () {

        return Constants.SHELL_SCORE_INCREMENT;

    }

}
