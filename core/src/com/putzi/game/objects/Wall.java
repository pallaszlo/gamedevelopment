package com.putzi.game.objects;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import com.putzi.game.Assets;



/**
 * Class for the wall objects, used for collision detection with the environment
 */
public class Wall extends AbstractGameObject {


    private TextureRegion regionWall;


    public Wall () {

        init();

    }


    private void init () {

        dimension.set(1, 1);
        regionWall = Assets.instance.wall.wall;

    }


    /**
     * Method to render the wall objects
     * @param batch The sprite batch for all actions and data, sent together to the GPU
     */
    @Override
    public void render (SpriteBatch batch) {

        TextureRegion region;

        region = regionWall;

        batch.draw(
                region.getTexture(),
                position.x,
                position.y,
                origin.x,
                origin.y,
                dimension.x,
                dimension.y,
                scale.x,
                scale.y,
                rotation,
                region.getRegionX(),
                region.getRegionY(),
                region.getRegionWidth(),
                region.getRegionHeight(),
                false,
                false
        );
    }


    /**
    * Getter Method returning the score decrement for a wall collision
    * @return The score decrement for a wall collision
    */
    public int getScore () {

        return -1;

    }

}

