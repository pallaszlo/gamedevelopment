package com.putzi.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.putzi.game.ExtendedGame;
import com.putzi.screens.transitions.ScreenTransition;
import com.putzi.screens.transitions.ScreenTransitionFade;

import com.putzi.util.ButtonsManager;
import com.putzi.util.Constants;
import com.putzi.util.UserManagement;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Class for game over screen
 * Provides methods to create stage, windows, buttons for new game, load game, exit game, options, load highscores
 * Either from Database or from a local preferences file.
 */

public class GameOverScreen extends AbstractGameScreen {

    private Stage stage;
    private Skin skinButtons;
    private Skin skinWindows;
    private Skin skinLibgdx;

    private Image gameOverText;

    private Button btnMenuPlay;
    private Button btnExitGame;
    private Button btnHighScores;
    private Button btnBackToMenu;
    private Button btnOk;

    private Window winHighscore;
    private Label playerNameLabel;

    private boolean debugEnabled = false;
    private float debugRebuildStage;

    /** constructor for game over screen class
     *  @param game instance of ExtendedGame
     */
    public GameOverScreen (ExtendedGame game){
        super(game);

    }

    /**
     * Method to rebuild the stage in the game over screen
     */
    private  void rebuildStage(){

        // Skin for the buttons
        skinButtons = new Skin(
                Gdx.files.internal(Constants.SKIN_BUTTONS_UI),
                new TextureAtlas(Constants.TEXTURE_BUTTONS)
        );
        //skin for windows
        skinWindows = new Skin(Gdx.files.internal(Constants.SKIN_WINDOW_UI),
                new TextureAtlas(Constants.TEXTURE_ATLAS_WINDOW_UI));

        // Skin for buttons from Ressourcen/Texturatlas
        skinLibgdx = new Skin(
                Gdx.files.internal(Constants.SKIN_LIBGDX_UI),
                new TextureAtlas(Constants.TEXTURE_ATLAS_LIBGDX_UI)
        );

        //create Layer for TableLayout
        Table layerBackground = buildBackgroundLayer();
        Table layerControls = buildControlsLayer();
        Table layerObjects = buildObjectsLayer();
        Table layerText = buildTextLayer();
        Table layerHighscoreWindow = buildHighscoreWindowLayer();

        // create stage with two nodes: stack for layers and stacks for highscore-window
        stage.clear();
        Stack stack = new Stack();
        stack.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        stack.add(layerBackground);
        stack.add(layerControls);
        stack.add(layerObjects);
        stack.add(layerText);
        stage.addActor(stack);
        stage.addActor(layerHighscoreWindow);


    }

    /**
     * Method to build/create window-layer for show the list of highscores
     * @return window layer for highscores
     */

    private Table buildHighscoreWindowLayer() {

        winHighscore = new Window("Highscore", skinWindows);
        winHighscore.getTitleTable().pad(60, 10, 25, 0);

        winHighscore.add(buildHighscore()).pad(50,10,25,0).row();
        winHighscore.add(buildHighscoreWinButtons()).pad(20, 0, 20, 0).row();

        ButtonsManager.showOptionsWindow(false, false,winHighscore);

        if (debugEnabled) {
            winHighscore.debug();
        }

        winHighscore.pack();

        winHighscore.setPosition(Constants.VIEWPORT_GUI_WIDTH/2 - winHighscore.getWidth()/2,
                Constants.VIEWPORT_GUI_HEIGHT/2-winHighscore.getHeight()/2);

        return winHighscore;

    }

    /**
     * Method to build/create table with buttons to return to the game over screen
     * from the highscore layer
     * @return a table-layer with ok-button
     */
    private Table buildHighscoreWinButtons() {

        Table table = new Table();
        table.row();

        btnOk = new TextButton("OK", skinWindows);
        table.add(btnOk).center().width(75).height(40).row();

        btnOk.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onOkClicked();
            }
        });

        table.row();

        return table;

    }
    /**
     * Method to build/create window-layer for show the list of highscores
     * the list is loading from the database, list size is max 10
     * @return a window layer
     */
    private Table buildHighscore(){

        //create a list with nam an scores

        Table table = new Table();

        UserManagement usrMgmt = UserManagement.instance;

        Label nameLabel = new Label("Name: ", skinLibgdx);
        Label scoreLabel = new Label("Score: ", skinLibgdx);
        table.add(nameLabel).colspan(2).left();
        table.add(scoreLabel).colspan(2).right();
        table.row();
        for (int i = 0; i < usrMgmt.getUserNames().size(); i++) {
            // break out of loop, if too much players are in Database
            if (i > 9) {
                break;
            }
            String username = usrMgmt.getUserNames().get(i);
            Label newPlayerLabel = new Label(username, skinLibgdx);
            Label newHighscoreLabel = new Label(String.valueOf(usrMgmt.getHighscore(username)), skinLibgdx);
            if (UserManagement.getUser() != null && username.equals(UserManagement.getUser())) {
                newPlayerLabel.setColor(Color.GOLD);
                newHighscoreLabel.setColor(Color.GOLD);
            }
            table.add(newPlayerLabel).colspan(2).left().padRight(50);
            table.add(newHighscoreLabel).colspan(2).right().padLeft(50).padRight(10);
            table.row();
        }

        return table;
    }

    /**
     * Listener for ok-button in the options-layer.
     * Show menu buttons and hide the windows.
     */
    private void onOkClicked() {
        showMenuButtons(true);
        ButtonsManager.showOptionsWindow(false, true, winHighscore);

    }

    /**
     * Method to show/animate the control buttons and the label for the user name
     * @param visible is true when visibility is setted
     */
    private void showMenuButtons(boolean visible) {

        // animate bleding for label/buttons
        float moveDuration = 1.0f;
        Interpolation moveEasing = Interpolation.swing;
        float delayOptionsButton = 0.25f;

        float moveX = 0.0f * (visible ? -1 : 1);
        float moveY = -1000.0f * (visible ? -1 : 1);

        final Touchable touchEnabled = visible ? Touchable.enabled : Touchable.disabled;
        //buttons and animation
        //buttons for gameplay
        playerNameLabel.addAction(
                sequence(
                        delay(delayOptionsButton),
                        moveBy(
                                moveX,
                                moveY,
                                moveDuration,
                                moveEasing
                        )
                )
        );
        btnMenuPlay.addAction(
                moveBy(
                        moveX,
                        moveY,
                        moveDuration,
                        moveEasing
                )
        );
        //button for return to the menu screen
        btnBackToMenu.addAction(
                sequence(
                        delay(delayOptionsButton),
                        moveBy(
                                moveX,
                                moveY,
                                moveDuration,
                                moveEasing
                        )
                )
        );
        //button to show the highscore
        btnHighScores.addAction(
                moveBy(
                        moveX,
                        moveY,
                        moveDuration,
                        moveEasing
                )
        );
        //button to exit the game
        btnExitGame.addAction(
                sequence(
                        delay(delayOptionsButton),
                        moveBy(
                                moveX,
                                moveY,
                                moveDuration,
                                moveEasing
                        )
                )
        );

        SequenceAction seq = sequence();

        if (visible) {
            seq.addAction(
                    delay(
                            delayOptionsButton + moveDuration
                    )
            );
        }


        stage.addAction(seq);

    }

    /**
     * Method to build/create and animate  game over text
     * @return a layer for object
     */
    private Table buildObjectsLayer() {

        Table layer = new Table();
        gameOverText = new Image(skinButtons, "gameoverText");
        layer.addActor(gameOverText);

        gameOverText.addAction(
                sequence(
                        moveTo(Constants.VIEWPORT_GUI_WIDTH/2-gameOverText.getWidth()/2-1500, Constants.VIEWPORT_GUI_HEIGHT/2+200),
                        delay(0.9f),
                        moveTo(Constants.VIEWPORT_GUI_WIDTH/2-gameOverText.getWidth()/2, Constants.VIEWPORT_GUI_HEIGHT/2+200, 0.75f, Interpolation.swingOut)
                )
        );

        return layer;

    }
    /**
     * Method to build/create the control-layer with buttons and user name if logged in
     * @return a layer for controls
     */
    private Table buildControlsLayer() {

        //create buttonlayer
        Table layer = new Table();

        layer.center().bottom().padBottom(-350);
        layer.addAction(
                sequence(
                        delay(2.6f),
                        moveTo( 0, 600, 0.75f, Interpolation.swingOut)
                )
        );
        //new game-Button
        btnMenuPlay = new Button(skinButtons, "newgame");

        layer.add(btnMenuPlay);
        btnMenuPlay.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onPlayClicked();
            }
        });
        layer.row();

        //button back to menu
        btnBackToMenu = new Button(skinButtons, "menuscreen");
        layer.add(btnBackToMenu);
        btnBackToMenu.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onBackToMenuClicked();
            }
        });
        layer.row();

        /*
         * button for load/save high scores
		 * Only available if username is set
		 */
        if (null != UserManagement.getUser()){
           btnHighScores = new Button(skinButtons, "scores");
           layer.add(btnHighScores);
           btnHighScores.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    onHighscoreClicked();
                }
           });
           layer.row();
        }

        //exit game button
        btnExitGame = new Button(skinButtons, "exit");
        layer.add(btnExitGame);
        btnExitGame.addListener(new ChangeListener(){
            @Override
            public  void changed(ChangeEvent event, Actor actor){
                ButtonsManager.onExitClicked();
            }
        });

        if (debugEnabled) {
            layer.debug();
        }

        return layer;

    }

    /**
     * Method to build/create and question-text
     * @return a layer for object
     */
    private Table buildTextLayer() {

        //create buttonlayer
        Table layer = new Table();
        layer.bottom().padBottom(-200);
        layer.addAction(
                sequence(
                        moveTo(450, 1200),
                        delay(1.8f),
                        moveTo(0, 750, 0.75f, Interpolation.swingOut)
                )
        );

        if (null != UserManagement.getUser()) {
            playerNameLabel = new Label(" Wie soll's weitergehen, " + UserManagement.getUser() +
                    "?", skinLibgdx, "big-font", Color.GOLD);
        }else{
            playerNameLabel = new Label(" Wie soll's weitergehen?", skinLibgdx, "big-font", Color.GOLD);
        }

        layer.add(playerNameLabel).padBottom(80);

        layer.row();

        if (debugEnabled) {
            layer.debug();
        }

        return layer;

    }
    /**
     * Method to build/create the background layer
     * @return a layer for background
     */
    private Table buildBackgroundLayer() {

        Table layer = new Table();
        layer.add(new Image(skinButtons, "gameover"));
        return layer;

    }

    /**
     * Listener for option-button in the show highscore-layer.
     * Hide menu buttons and show highscore-window.
     */
    private  void onHighscoreClicked(){
        showMenuButtons(false);
        ButtonsManager.showOptionsWindow(true, true, winHighscore);
    }
    /**
     * Listener for play-button in the controls-layer.
     * Change the screen to game screen.
     */
    private void onPlayClicked() {

        ScreenTransition transition = ScreenTransitionFade.init(0.75f);
        game.setScreen(new com.putzi.screens.GameScreen(game), transition);

    }

    /**
     * Listener for backtomenu-button in the controls-layer.
     * Change the screen to the menu screen.
     */
    private void onBackToMenuClicked(){
        ScreenTransition transition = ScreenTransitionFade.init(0.75f);
        game.setScreen(new com.putzi.screens.MenuScreen(game), transition);
    }

    /**
     * Method called by the game loop from the application every time rendering should be performed.
     * Game logic updates are usually also performed in this method.
     * @param deltaTime time passed between last call
     */
    @Override
    public void render(float deltaTime) {

        Gdx.gl.glClearColor(1,1,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //if Live-Updates lifecycle on do rebuild
        if (debugEnabled) {
            debugRebuildStage -= deltaTime;
            if (debugRebuildStage <= 0) {
                debugRebuildStage = Constants.DEBUG_REBUILD_INTERVAL;
                rebuildStage();
            }
        }
        stage.act(deltaTime);
        stage.draw();
    }

    /**
     * This method is called every time the game screen is re-sized and the game is not in the paused state.
     * @param width is the new width
     * @param height is the new height
     */
    @Override
    public void resize(int width, int height) {

        stage.getViewport().update(width, height, true);

    }

    @Override
    public void pause() {
        // must inherite
    }

    /**
     * Method to create an show the stage with the viewport width and heigth
     * Called when this screen becomes the current screen for a Game.
     */
    @Override
    public void show() {
        //create stage
        stage = new Stage(
                new StretchViewport(
                        Constants.VIEWPORT_GUI_WIDTH,
                        Constants.VIEWPORT_GUI_HEIGHT
                )
        );
        //rebuild stage
        rebuildStage();

    }

    /**
     * Called when this screen is no longer the current screen for a Game
     */
    @Override
    public void hide() {

        stage.dispose();
        skinButtons.dispose();
        skinLibgdx.dispose();
        skinWindows.dispose();
    }

    /**
     * Method to set the stage as input listener
     * @return a stage for inputs
     */
    @Override
    public InputProcessor getInputProcessor() {
        return stage;
    }
}
