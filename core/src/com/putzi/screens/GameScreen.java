package com.putzi.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.putzi.game.ExtendedGame;
import com.putzi.game.WorldController;
import com.putzi.game.WorldRenderer;


/**
 * Class for in-Game Screen
 */
public class GameScreen extends AbstractGameScreen {

	private com.putzi.game.WorldController worldController;
	private com.putzi.game.WorldRenderer worldRenderer;


	/** constructor for menu screen class
	 *  @param game instance of ExtendedGame
	 */
	public GameScreen (ExtendedGame game) {

		super(game);
		Gdx.input.setCursorPosition(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

	}

	/**
	 * Method called by the game loop from the application every time rendering should be performed.
	 * Game logic updates are usually also performed in this method.
	 * @param deltaTime time passed between last call
	 */
	@Override
	public void render (float deltaTime) {

		worldController.update(deltaTime);

		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		worldRenderer.render();

	}

	/**
	 * This method is called every time the game screen is re-sized and the game is not in the paused state.	 *
	 * @param width is the new width
	 * @param height is the new height
	 */
	@Override
	public void resize (int width, int height) {

		// Auf Änderungen der Fenstergröße reagieren

		worldRenderer.resize(width, height);

	}

	/**
	 * Method to create an show the stage with the viewport width and heigth
	 * Called when this screen becomes the current screen for a Game.
	 */
	@Override
	public void show () {

		worldController = new WorldController(game);
		worldRenderer = new WorldRenderer(worldController);

		// Android
		Gdx.input.setCatchBackKey(true);
	}


	/**
	 * Called when this screen is no longer the current screen for a Game
	 */
	@Override
	public void hide () {

		// Clean Storage

		worldRenderer.dispose();

		// Android
		Gdx.input.setCatchBackKey(false);

	}


	/**
	 * Called when the Application is paused, usually when it's not active or visible on screen
	 */
	@Override
	public void pause () {
		// must inherite
	}


	/**
	 * Method to set the stage as input listener
	 * @return world controller
	 */
	@Override
	public InputProcessor getInputProcessor () {

		return worldController;

	}

}
