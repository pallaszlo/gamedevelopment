package com.putzi.screens.transitions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.utils.Array;


/**
 * Transition with slices going up and down
 */
public class ScreenTransitionSlice implements ScreenTransition {
	
	
	/** Moving all slices up */
	public static final int UP = 1;
	/** Moving all slices down */
	public static final int DOWN = 2;
	/** Moving the slices alternately up and down */
	public static final int UP_DOWN = 3;

	private static final ScreenTransitionSlice instance = new ScreenTransitionSlice();

	private float duration;
	private int direction;
	private Interpolation easing;
	private Array<Integer> sliceIndex = new Array<Integer>();


	/**
	 * initialisation for slice transition
	 * @param duration variable for during of transition
	 * @param direction variable for slice direction
	 * @param numSlices varable for slice number
	 * @param easing object for interpolation
	 * @return instance of ScreenTransitionSlice
	 */
	public static ScreenTransitionSlice init (float duration, int direction, int numSlices, Interpolation easing) {

		instance.duration = duration;
		instance.direction = direction;
		instance.easing = easing;

		// create list from slice indices
		
		instance.sliceIndex.clear();
		
		for (int i = 0; i < numSlices; i++) {

			instance.sliceIndex.add(i);
			
		}
		
		instance.sliceIndex.shuffle();
		
		return instance;
		
	}

	/**
	 * get-method for duration
	 * @return duration of the transition
	 */
	@Override
	public float getDuration () {
		
		return duration;
		
	}

	/** method called by the game loop from the application every time rendering should be performed.
	 * Game logic updates are usually also performed in this method.
	 * @param batch object for draw sprites
	 * @param currentScreen object for current screen
	 * @param nextScreen object for next screen
	 * @param alpha variable for fade
	 */
	@Override
	public void render (SpriteBatch batch, Texture currentScreen, Texture nextScreen, float alpha) {

		float width = currentScreen.getWidth();
		float height = currentScreen.getHeight();
		float positionX = 0;
		float positionY = 0;
		int sliceWidth = (int)(width/sliceIndex.size);

		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();

		batch.draw(
			currentScreen,
			0,
			0,
			0,
			0,
			width,
			height,
			1,
			1,
			0,
			0,
			0,
			currentScreen.getWidth(),
			currentScreen.getHeight(),
			false,
			true
		);

		if (easing != null) {

			alpha = easing.apply(alpha);

		}

		for (int i = 0; i < sliceIndex.size; i++) {

			positionX = i * sliceWidth;

			// verticale slice
			float offsetY = height * (1 + sliceIndex.get(i)/(float)sliceIndex.size);

			switch (direction) {

			case UP:

				positionY = -offsetY + offsetY * alpha;

				break;

			case DOWN:

				positionY = offsetY - offsetY * alpha;

				break;

			case UP_DOWN:

				if (i % 2 == 0) {

					positionY = -offsetY + offsetY * alpha;

				} else {

					positionY = offsetY - offsetY * alpha;

				}

				break;
			}

			batch.draw(
				nextScreen,
				positionX,
				positionY,
				0,
				0,
				sliceWidth,
				height,
				1,
				1,
				0,
				i * sliceWidth,
				0,
				sliceWidth,
				nextScreen.getHeight(),
				false,
				true
			);

		}

		batch.end();

	}

}
