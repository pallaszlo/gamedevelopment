package com.putzi.screens.transitions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;


/**
 * Transition to next Screen, new Screen falls down
 */
public class ScreenTransitionSlide implements ScreenTransition {


	/** Slide Screen from right to left */
	public static final int LEFT = 1;
	/** Slide Screen from left to right */
	public static final int RIGHT = 2;
	/** Slide Screen from bottom to top */
	public static final int UP = 3;
	/** Slide Screen from top to bottom */
	public static final int DOWN = 4;

	private static final ScreenTransitionSlide instance = new ScreenTransitionSlide();

	private float duration;
	private int direction;
	private boolean slideOut;
	private Interpolation easing;

	/**
	 * initialisation for slide transition
	 * @param duration variable for during of transition
	 * @param direction variable for slice direction
	 * @param slideOut varable for slice number
	 * @param easing object for interpolation
	 * @return instance of ScreenTransitionSlide
	 */
	public static ScreenTransitionSlide init (float duration, int direction, boolean slideOut, Interpolation easing) {

		instance.duration = duration;
		instance.direction = direction;
		instance.slideOut = slideOut;
		instance.easing = easing;
		return instance;

	}

	/**
	 * get-method for duration
	 * @return duration of the transition
	 */
	@Override
	public float getDuration () {

		return duration;

	}


	/** method called by the game loop from the application every time rendering should be performed.
	 * Game logic updates are usually also performed in this method.
	 * @param batch object for draw sprites
	 * @param currScreen object for current screen
	 * @param nextScreen object for next screen
	 * @param alpha variable for fade
	 */
	@Override
	public void render (SpriteBatch batch, Texture currScreen, Texture nextScreen, float alpha) {

		float width = currScreen.getWidth();
		float height = currScreen.getHeight();
		float positionX = 0;
		float positionY = 0;

		if (easing != null) {

			alpha = easing.apply(alpha);

		}

		// calculate offset

		switch (direction) {

		case LEFT:

			positionX = -width * alpha;

			if (!slideOut) {

				positionX += width;

			}

			break;

		case RIGHT:

			positionX = width * alpha;

			if (!slideOut) {

				positionX -= width;

			}

			break;

		case UP:

			positionY = height * alpha;

			if (!slideOut) {

				positionY -= height;

			}

			break;

		case DOWN:

			positionY = -height * alpha;

			if (!slideOut) {

				positionY += height;

			}

			break;

		}

		// succession for draw depend from typ (SlideIn or SlideOut)

		Texture texBottom = slideOut ? nextScreen : currScreen;
		Texture texTop = slideOut ? currScreen : nextScreen;

		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();

		batch.draw(
				texBottom,
				0,
				0,
				0,
				0,
				width,
				height,
				1,
				1,
				0,
				0,
				0,
				currScreen.getWidth(),
				currScreen.getHeight(),
				false,
				true
		);

		batch.draw(
				texTop,
				positionX,
				positionY,
				0,
				0,
				width,
				height,
				1,
				1,
				0,
				0,
				0,
				nextScreen.getWidth(),
				nextScreen.getHeight(),
				false,
				true
		);

		batch.end();

	}

}
