package com.putzi.screens.transitions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;


/**
 * Transiton with simple Fading to next Screen
 */
public class ScreenTransitionFade implements ScreenTransition {



	private static final ScreenTransitionFade instance = new ScreenTransitionFade();
	private float duration;


	/**
	 * initialisation for fade transition
	 * @param duration variable for during of transition
	 * @return instance of ScreenTransitionFade
	 */
	public static ScreenTransitionFade init (float duration) {

		instance.duration = duration;
		return instance;

	}


	/**
	 * get-method for duration
	 * @return duration of the transition
	 */
	@Override
	public float getDuration () {

		return duration;

	}


	/** method called by the game loop from the application every time rendering should be performed.
	 * Game logic updates are usually also performed in this method.
	 * @param spriteBatch object for draw sprites
	 * @param currentScreen object for current screen
	 * @param nextScreen object for next screen
	 * @param alpha variable for fade
	 */
	@Override
	public void render (SpriteBatch spriteBatch, Texture currentScreen, Texture nextScreen, float alpha) {

		float width = currentScreen.getWidth();
		float height = currentScreen.getHeight();
		alpha = Interpolation.fade.apply(alpha);

		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		spriteBatch.begin();

		spriteBatch.setColor(1, 1, 1, 1);

		spriteBatch.draw(
				currentScreen,			     // texture
				0,                           // x-position
				0,                           // y-position
				0,                           // x-coordinate from origin
				0,                           // y-coordinate from origin
				width,
				height,
				1,
				1,
				0,
				0,						     // x-coordinate in texel Space
				0,						     // y-coordinate in texel Space
				currentScreen.getWidth(),    // width in texels
				currentScreen.getHeight(),   // height in texels
				false,					     // horizontal not fliped
				true                       // vertical fliped
		);

		spriteBatch.setColor(1, 1, 1, alpha);

		spriteBatch.draw(
			nextScreen,
			0,
			0,
			0,
			0,
			width,
			height,
			1,
			1,
			0,
			0,
			0,
			nextScreen.getWidth(),
			nextScreen.getHeight(),
			false,
			true
		);

		spriteBatch.end();

	}

}
