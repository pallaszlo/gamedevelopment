package com.putzi.screens.transitions;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


/**
 * interface for transition between screens with method for get duration of the transition and
 * render
 */
public interface ScreenTransition {

	/**
	 * method for get duration
	 * @return the calculated duration
	 */
	public float getDuration ();

	/** abstract method called by the game loop from the application every time rendering should be performed.
	 * Game logic updates are usually also performed in this method.
	 * @param batch object for draw sprites
	 * @param currentScreen object for current screen
	 * @param nextScreen object for next screen
	 * @param alpha variable for fade
	 */
	public abstract void render (SpriteBatch batch, Texture currentScreen, Texture nextScreen, float alpha);

}
