package com.putzi.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.putzi.game.ExtendedGame;

/**
 * abstract class for game screens
 * Provides methods to render, resize, show, hide, pause, resume, dispose and get input processor
 */
public abstract class AbstractGameScreen implements Screen {


	/* instance of ExtendedGame*/
	protected ExtendedGame game;


	/**
	 * constructor for abstract game screen class
	 * @param game instance of ExtendedGame
	 */
	AbstractGameScreen (ExtendedGame game) {

		this.game = game;
		Gdx.input.setCursorPosition((int)(Gdx.graphics.getWidth()*0.5f), (int)(Gdx.graphics.getHeight()*0.5f));

	}

	/**
	 * Android only - called, if the Application gets focus
	 */
	public void resume () {}


	/**
	 * Could be called, if some resources should be disposed.
     * Note: It's not called automatically by LibGDX und not used in this Application
	 */
	public void dispose () {}


    /** Method to set the stage as input listener
	 * @return InputProcessor of the current screen
     */
    public abstract InputProcessor getInputProcessor ();

}
