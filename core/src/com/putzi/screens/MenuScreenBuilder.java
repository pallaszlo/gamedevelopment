package com.putzi.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.putzi.game.Assets;
import com.putzi.game.WorldController;
import com.putzi.util.AudioManager;
import com.putzi.util.ButtonsManager;
import com.putzi.util.CharacterSkin;
import com.putzi.util.Constants;
import com.putzi.util.GamePreferences;
import com.putzi.util.UserManagement;

import java.util.HashMap;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.rotateTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * This class offers methods to build the MenuScreen and its contents.
 */
class MenuScreenBuilder {

    private Skin skinPutzi;
    private Skin skinLibgdx;
    private Skin skinButtons;
    private Skin skinWindows;

    private MenuScreen attachedScreen;

    private HashMap<String, Label> labelMap;
    private HashMap<String, Button> buttonMap;
    private HashMap<String, Window> windowMap;
    private HashMap<String, TextField> tFieldMap;
    private HashMap<String, CheckBox> chBoxMap;
    private HashMap<String, Slider> sliderMap;

    private Image imgCharSkin;
    private SelectBox selCharSkin;

    MenuScreenBuilder(MenuScreen attachedScreen) {
        // Skin for buttons putzi style - old version
        this.skinPutzi = new Skin(
                Gdx.files.internal(Constants.SKIN_PUTZI_UI),
                new TextureAtlas(Constants.TEXTURE_ATLAS_UI)
        );
        // Skin for buttons from Ressourcen/Texturatlas
        this.skinLibgdx = new Skin(
                Gdx.files.internal(Constants.SKIN_LIBGDX_UI),
                new TextureAtlas(Constants.TEXTURE_ATLAS_LIBGDX_UI)
        );
        //Skin for buttons
        skinButtons = new Skin(Gdx.files.internal(Constants.SKIN_BUTTONS_UI),
                new TextureAtlas(Constants.TEXTURE_BUTTONS)
        );
        //Skin for windows
        skinWindows = new Skin(Gdx.files.internal(Constants.SKIN_WINDOW_UI),
                new TextureAtlas(Constants.TEXTURE_ATLAS_WINDOW_UI));
        this.attachedScreen = attachedScreen;
        labelMap = new HashMap<>();
        buttonMap = new HashMap<>();
        windowMap = new HashMap<>();
        tFieldMap = new HashMap<>();
        chBoxMap = new HashMap<>();
        sliderMap = new HashMap<>();
    }

    /**
     * Method to build/create the background layer
     * @return a layer for background
     */
    Table buildBackgroundLayer() {

        Table layer = new Table();

        layer.add(new Image(skinPutzi, "background"));
        return layer;
    }
    /**
     * Method to build/create and animate garbage-object layers and a Putzi-layer
     * @return a layer for objects
     */
    Table buildObjectsLayer() {

        Table layer = new Table();

        Image dirtImage1 = new Image(skinPutzi, "dirtImage");
        layer.addActor(dirtImage1);

        dirtImage1.addAction(
                sequence(
                        moveTo(700, 1200),
                        delay(Constants.INTRO_DIRT_1_DELAY),
                        moveTo(700, 50, Constants.INTRO_DIRT_SPEED, Interpolation.circleIn)
                )
        );

        Image dirtImage2 = new Image(skinPutzi, "dirtImage");
        layer.addActor(dirtImage2);

        dirtImage2.addAction(
                sequence(
                        moveTo(100, 1200),
                        delay(Constants.INTRO_DIRT_2_DELAY),
                        moveTo(100, 10, Constants.INTRO_DIRT_SPEED, Interpolation.circleIn)
                )
        );

        Image dirtImage3 = new Image(skinPutzi, "dirtImage");
        layer.addActor(dirtImage3);

        dirtImage3.addAction(
                sequence(
                        moveTo(400, 1200),
                        delay(Constants.INTRO_DIRT_3_DELAY),
                        moveTo(400, 100, Constants.INTRO_DIRT_SPEED, Interpolation.circleIn)
                )
        );

        Image dirtImage4 = new Image(skinPutzi, "dirtImage");
        layer.addActor(dirtImage4);

        dirtImage4.addAction(
                sequence(
                        moveTo(-50, 1200),
                        delay(Constants.INTRO_DIRT_4_DELAY),
                        moveTo(-50, 100, Constants.INTRO_DIRT_SPEED, Interpolation.circleIn)
                )
        );

        Image dirtImage5 = new Image(skinPutzi, "dirtImage");
        layer.addActor(dirtImage5);

        dirtImage5.addAction(
                sequence(
                        moveTo(450, 1200),
                        delay(Constants.INTRO_DIRT_5_DELAY),
                        moveTo(450, 200, Constants.INTRO_DIRT_SPEED, Interpolation.circleIn)
                )
        );

        Image putziImage = new Image(skinPutzi, "putziImage");
        layer.addActor(putziImage);

        putziImage.setOrigin(
                putziImage.getWidth()/2,
                putziImage.getHeight()/2
        );

        putziImage.addAction(
                sequence(
                        moveTo(2000, 900),
                        scaleTo(0.01f, 0.01f),
                        rotateTo(20),
                        delay(Constants.INTRO_PUTZI_DELAY),
                        parallel(
                                scaleTo(1.5f, 1.5f, Constants.INTRO_PUTZI_SPEED),
                                moveTo(1400, 250, Constants.INTRO_PUTZI_SPEED, Interpolation.circleOut),
                                rotateTo(0, Constants.INTRO_PUTZI_SPEED)
                        )
                )
        );

        Image dirtImage6 = new Image(skinPutzi, "dirtImage");
        layer.addActor(dirtImage6);

        dirtImage6.addAction(
                sequence(
                        moveTo(450, 1200),
                        delay(Constants.INTRO_DIRT_6_DELAY),
                        moveTo(400, 250, Constants.INTRO_DIRT_SPEED, Interpolation.circleIn)
                )
        );

        Image dirtImage7 = new Image(skinPutzi, "dirtImage");
        layer.addActor(dirtImage7);

        dirtImage7.addAction(
                sequence(
                        moveTo(450, 1200),
                        delay(Constants.INTRO_DIRT_7_DELAY),
                        moveTo(150, 200, Constants.INTRO_DIRT_SPEED, Interpolation.circleIn)
                )
        );

        return layer;
    }
    /**
     * Method to build/create the logo layer
     * @return a layer for logo
     */
    Table buildLogosLayer() {

        Table layer = new Table();
        layer.right().top().padTop(-60).padRight(-180);

        Image imgLogo = new Image(skinPutzi, "logogold");
        imgLogo.setScale(0.7f);
        layer.add(imgLogo);

        if (attachedScreen.getDebug()) {
            layer.debug();
        }
        return layer;
    }
    /**
     * Build the save layer to show saving process to user
     * @return The Table with the "saving"-label
     */
    Table buildSaveLayer() {
        Table layer = new Table();
        layer.bottom().left();

        Label saveLabel = new Label("Speichervorgang", skinLibgdx, "big-font", Color.GOLD);
        layer.add(saveLabel).padLeft(20).padBottom(20);
        saveLabel.setVisible(false);
        labelMap.put("save", saveLabel);
        return layer;
    }
    /**
     * analog to save layer this shows the process of loading to the user
     * @return The Table with the "loading"-label
     */
    Table buildLoadLayer() {
        Table layer = new Table();
        layer.bottom().left();
        Label loadLabel = new Label("Ladevorgang", skinLibgdx, "big-font", Color.GOLD);
        layer.add(loadLabel).padLeft(20).padBottom(20);
        loadLabel.setVisible(false);
        labelMap.put("load", loadLabel);
        return layer;
    }
    /**
     * Method to build/create the control-layer with buttons and user name
     * @return a layer for controls
     */
    Table buildControlsLayer() {

        Table layer = new Table();

        if (!WorldController.isGameIsRunning() && null == UserManagement.getUser()){
            layer.right().padRight(-500);
            layer.addAction(
                    sequence(
                            delay(Constants.INTRO_MIDDLE_BUTTONS_DELAY),
                            moveTo(-1250, 0, Constants.INTRO_BUTTONS_SPEED, Interpolation.swingOut)
                    )
            );
        }
        // show user name if username is set
        if (null != UserManagement.getUser()) {
            Label playerNameLabel = new Label("Hallo " + UserManagement.getUser(), skinLibgdx,
                    "big-font", Color.GOLD);
            layer.add(playerNameLabel).padBottom(45);
            labelMap.put("playername", playerNameLabel);
        }
        layer.row();

        //button and functionality for new game
        Button btnMenuPlay = new Button(skinButtons, "newgame");
        layer.add(btnMenuPlay);
        buttonMap.put("menuplay", btnMenuPlay);
        btnMenuPlay.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                attachedScreen.onPlayClicked();
            }
        });
        layer.row();

        //button and functionality for options
        Button btnMenuOptions = new Button(skinButtons, "options");
        layer.add(btnMenuOptions);
        buttonMap.put("menuoptions", btnMenuOptions);
        btnMenuOptions.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                attachedScreen.onOptionsClicked();
            }
        });
        layer.row();

        //only available if username is set
        if (null != UserManagement.getUser()) {
            Button btnHighScores = new Button(skinButtons, "scores");
            layer.add(btnHighScores);
            buttonMap.put("highscores", btnHighScores);
            btnHighScores.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    attachedScreen.onHighscoreClicked();
                }
            });
            layer.row();
        }

        //button  and functionality for impressum
        Button btnImpressum = new Button(skinButtons, "impressum");
        layer.add(btnImpressum);
        buttonMap.put("impressum", btnImpressum);
        layer.row();
        btnImpressum.addListener(new ChangeListener(){
            @Override
            public  void changed(ChangeEvent event, Actor actor){
                attachedScreen.onImpressumClicked();
            }
        });

        //button and functionality for gameinfo
        Button btnGameInfo = new Button(skinButtons, "information");
        layer.add(btnGameInfo);
        layer.row();
        buttonMap.put("gameinfo", btnGameInfo);
        btnGameInfo.addListener(new ChangeListener(){
            @Override
            public  void changed(ChangeEvent event, Actor actor){
                attachedScreen.onGameInfoClicked();
            }
        });

        //exit button and functionality for exit game
        Button btnExitGame = new Button(skinButtons, "exit");
        layer.add(btnExitGame);
        buttonMap.put("exitgame", btnExitGame);
        layer.row();
        btnExitGame.addListener(new ChangeListener(){
            @Override
            public  void changed(ChangeEvent event, Actor actor){
                ButtonsManager.onExitClicked();
            }
        });

        if (attachedScreen.getDebug()) {
            layer.debug();
        }
        return layer;
    }
    /**
     * Method to build/create and animate the layer with buttons for new player or load player
     * @return a layer for login controls
     */
    Table buildLoginControlsLayer() {

        Table layer = new Table();

        if (!WorldController.isGameIsRunning() && null == UserManagement.getUser()){
            layer.right().padRight(-200);
            layer.addAction(
                    sequence(
                            delay(Constants.INTRO_LEFT_BUTTONS_DELAY),
                            moveTo(-Constants.VIEWPORT_GUI_WIDTH, 0, Constants.INTRO_BUTTONS_SPEED, Interpolation.swingOut)
                    )
            );
        }else {
            layer.left();
        }
        //button and functionality for add a new player
        Button btnNewPlayer = new Button(skinButtons, "newplayer");
        layer.add(btnNewPlayer);
        layer.row();
        buttonMap.put("newplayer", btnNewPlayer);
        btnNewPlayer.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                attachedScreen.onLoginPlayerClicked();
            }
        });

        //button and functinality for load player
        Button btnLoadPlayer = new Button(skinButtons, "loadplayer");
        layer.add(btnLoadPlayer);
        layer.row();
        buttonMap.put("loadplayer", btnLoadPlayer);
        btnLoadPlayer.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                attachedScreen.onLoginPlayerClicked();
            }
        });

        if (attachedScreen.getDebug()) {
            layer.debug();
        }

        return layer;

    }
    /**
     * Method to build/create window-layer for new player
     * @return a window layer for set a new player
     */
    Table buildNewPlayerWindowLayer() {

        Window winNewPlayer = new Window("Neuer Spieler", skinWindows);
        winNewPlayer.getTitleTable().pad(60, 10, 25, 0);

        winNewPlayer.add(buildNewPlayerWinButtons()).pad(50, 0, 10, 0);

        ButtonsManager.showOptionsWindow(false, false, winNewPlayer);

        if (attachedScreen.getDebug()) {
            winNewPlayer.debug();
        }

        winNewPlayer.pack();

        winNewPlayer.setPosition(Constants.VIEWPORT_GUI_WIDTH/2 - winNewPlayer.getWidth()/2,
                Constants.VIEWPORT_GUI_HEIGHT/2-winNewPlayer.getHeight()/2);
        windowMap.put("newplayer", winNewPlayer);
        return winNewPlayer;

    }
    /**
     * Method to build/create table with textfield to enter the user name
     * and buttons to set a new player
     * @return table layer with textfield and buttons
     */
    private Table buildNewPlayerWinButtons() {

        Table table = new Table();

        table.add(new Label("Wie ist dein Name?", skinLibgdx, "default-font",
                Color.GREEN)).pad(0, 10,10,0).left();
        table.row();
        table.add(new Label("(4-10 Zeichen)", skinLibgdx, "default-font",
                Color.GREEN)).pad(0, 10,10,0).left();
        table.row();

        final TextField newPlayerNameField = new TextField("", skinLibgdx);
        newPlayerNameField.setMessageText("");
        newPlayerNameField.setMaxLength(10);
        tFieldMap.put("playername", newPlayerNameField);
        newPlayerNameField.setTextFieldListener(new TextField.TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char key) {
                if (key == 13) {
                    attachedScreen.onSaveClicked(newPlayerNameField.getText());
                }
            }
        });

        Label errorMsg = new Label("", skinLibgdx, "default-font", Color.RED);
        labelMap.put("error", errorMsg);

        table.add(newPlayerNameField).colspan(2).width(300).pad(0,10,0,10).left();
        table.row();
        table.add(errorMsg).colspan(2).pad(0, 10, 5, 0).left();
        table.row();

        // optionsfield: create save- and cancel-button
        TextButton btnWinOptSave = new TextButton("Speichern", skinWindows);
        table.add(btnWinOptSave).width(120).height(45).pad(0,10,0,0).left();

        btnWinOptSave.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                attachedScreen.onSaveClicked(newPlayerNameField.getText());
            }
        });

        TextButton btnWinOptCancel = new TextButton("Abbrechen", skinWindows);
        table.add(btnWinOptCancel).width(120).height(45).pad(0,0,0,10);

        btnWinOptCancel.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.instance.play(Assets.instance.sounds.buttonCancelClickedSound);
                attachedScreen.onCancelClicked();
            }
        });

        return table;

    }
    /**
     * Method to build/create a  window-layer for loading a player
     * @return a window layer for load a player
     */
    Table buildLoadPlayerWindowLayer() {

        Window winLoadPlayer = new Window("Spieler laden", skinWindows);
        winLoadPlayer.getTitleTable().pad(60, 10, 25, 0);

        winLoadPlayer.add(buildLoadPlayerWinButtons()).pad(50, 0, 10, 0);

        ButtonsManager.showOptionsWindow(false, false, winLoadPlayer);

        if (attachedScreen.getDebug()) {
            winLoadPlayer.debug();
        }

        winLoadPlayer.pack();

        winLoadPlayer.setPosition(Constants.VIEWPORT_GUI_WIDTH/2 - winLoadPlayer.getWidth()/2,
                Constants.VIEWPORT_GUI_HEIGHT/2-winLoadPlayer.getHeight()/2);
        windowMap.put("loadplayer", winLoadPlayer);
        return winLoadPlayer;

    }
    /**
     * Method to build/create table with textfield to enter the user name
     * and buttons to load a player
     * @return table layer with textfield and buttons
     */
    private Table buildLoadPlayerWinButtons() {

        Table table = new Table();

        table.add(new Label("Dein Name", skinLibgdx, "default-font",
                Color.GREEN)).pad(0, 10,10,0).left();
        table.row();

        //create textfield
        final TextField loadPlayerNameField = new TextField("", skinLibgdx);
        loadPlayerNameField.setMessageText("");
        loadPlayerNameField.setMaxLength(10);
        tFieldMap.put("loadplayer", loadPlayerNameField);
        loadPlayerNameField.setTextFieldListener(new TextField.TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char key) {
                if (key == 13) {
                    attachedScreen.onLoadClicked(loadPlayerNameField.getText());
                }
            }
        });

        Label loadErrorMsg = new Label("", skinLibgdx, "default-font", Color.RED);

        table.add(loadPlayerNameField).colspan(2).width(300).pad(0,10,0,10).left();
        table.row();
        table.add(loadErrorMsg).colspan(2).pad(0, 10, 5, 0).left();
        labelMap.put("loaderror", loadErrorMsg);
        table.row();

        // optionsfield: create load- and cancel-button
        TextButton btnWinOptLoad = new TextButton("Laden", skinWindows);
        table.add(btnWinOptLoad).width(120).height(45).pad(0,10,0,0).left();

        btnWinOptLoad.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                attachedScreen.onLoadClicked(loadPlayerNameField.getText());
            }
        });

        TextButton btnWinOptCancel = new TextButton("Abbrechen", skinWindows);
        table.add(btnWinOptCancel).width(120).height(45).pad(0,0,0,10);

        btnWinOptCancel.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.instance.play(Assets.instance.sounds.buttonCancelClickedSound);
                attachedScreen.onCancelClicked();
            }
        });

        return table;
    }
    /**
     * Method to build/create a table for set the audio-options
     * @return a table
     */
    private Table buildOptWinAudioSettings() {

        Table table = new Table();

        table.pad(10, 10, 0, 10);

        table.add(new Label("Audio-Einstellungen", skinLibgdx, "default-font", Color.GREEN)).colspan(3);

        table.row();

        table.columnDefaults(0).padRight(10);
        table.columnDefaults(1).padRight(10);

        CheckBox chkSound = new CheckBox("", skinWindows);
        table.add(chkSound);
        chBoxMap.put("sound", chkSound);

        table.add(new Label("Sounds", skinLibgdx));

        Slider sldSound = new Slider(0.0f, 1.0f, 0.1f, false, skinWindows);
        sliderMap.put("sound", sldSound);
        table.add(sldSound);

        table.row();

        final CheckBox chkMusic = new CheckBox("", skinWindows);
        chBoxMap.put("music", chkMusic);
        chkMusic.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                GamePreferences.instance.music = chkMusic.isChecked();
                AudioManager.instance.onSettingsUpdated();
            }
        });
        table.add(chkMusic);

        table.add(new Label("Blubbern", skinLibgdx));

        final Slider sldMusic = new Slider(0.0f, 1.0f, 0.1f, false, skinWindows);
        sliderMap.put("music", sldMusic);
        sldMusic.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                GamePreferences.instance.volMusic = sldMusic.getValue();
                AudioManager.instance.onSettingsUpdated();
            }
        });
        table.add(sldMusic);

        table.row();

        return table;

    }
    /**
     * Method to build/create a  table for select the color of the skin
     * @return a table with options-field
     */
    private Table buildOptWinSkinSelection() {


        Table table = new Table();

        table.pad(10, 10, 0, 10);

        table.add(new Label("Putzis Farbe", skinLibgdx, "default-font", Color.GREEN)).colspan(2);

        table.row();

        selCharSkin = new SelectBox<CharacterSkin>(skinLibgdx);

        // Workaround für Web - warum?
        if (Gdx.app.getType() == Application.ApplicationType.WebGL) {
            Array<CharacterSkin> items = new Array<>();
            CharacterSkin[] arr = CharacterSkin.values();
            for (int i = 0; i < arr.length; i++) {
                items.add(arr[i]);
            }
            selCharSkin.setItems(items);
        } else {
            selCharSkin.setItems(CharacterSkin.values());
        }

        selCharSkin.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                attachedScreen.onCharSkinSelected(((SelectBox<CharacterSkin>) actor).getSelectedIndex());
            }
        });

        table.add(selCharSkin).width(75).height(30).padRight(20);

        imgCharSkin = new Image(Assets.instance.putzi.putzi);
        table.add(imgCharSkin).width(50).height(40);

        return table;

    }

    /**
     * Method to build/create a table for debug-options
     * frame per seconds and monochrome shader
     * @return a table-layer
     */
    private Table buildOptWinDebug() {

        Table table = new Table();

        table.pad(10, 10, 0, 10);
        table.add(new Label("Weitere Einstellungen", skinLibgdx, "default-font", Color.GREEN)).colspan(3);

        table.row();

        table.columnDefaults(0).padRight(10);
        table.columnDefaults(1).padRight(10);

        table.add(new Label("Frames pro Sekunde anzeigen", skinLibgdx));

        CheckBox chkShowFpsCounter = new CheckBox("", skinWindows);
        table.add(chkShowFpsCounter);
        chBoxMap.put("fps", chkShowFpsCounter);

        table.row();

        table.add(new Label("Nostalgie-Modus (Graustufen)", skinLibgdx));

        CheckBox chkUseMonochromeShader = new CheckBox("", skinWindows);
        table.add(chkUseMonochromeShader);
        chBoxMap.put("shader", chkUseMonochromeShader);

        table.row();

        return table;

    }

    /**
     * Method to build/create a table for show the impressumtext
     * @return a table
     */
    private Table buildShowImpressumText() {

        Table table = new Table();

        table.pad(30, 50, 0, 50);
        table.add(new Label("Ueber dieses Spiel", skinLibgdx, "default-font", Color.GREEN)).colspan(3);

        table.row();

//		table.columnDefaults(0).padRight(10);
//		table.columnDefaults(1).padRight(10);
        table.add(new Label("Dieses Spiel entstand als Semesterprojekt", skinLibgdx));
        table.row();
        table.add(new Label("im Modul \"Patterns und Frameworks\"", skinLibgdx));
        table.row();
        table.add(new Label("(Dipl.-Inform. Bettina Meiners)", skinLibgdx));
        table.row();
        table.add(new Label("im Wintersemester 2017/18", skinLibgdx));
        table.row();
        table.add(new Label("im Studiengang \"Medieninformatik Online\"", skinLibgdx));
        table.row();
        table.add(new Label("an der Ostfalia Hochschule Wolfenbüttel.", skinLibgdx));
        table.row();
        table.add(new Label("", skinLibgdx));
        table.row();
        table.add(new Label("Konzept und Programmierung", skinLibgdx, "default-font", Color.GREEN));
        table.row();
        table.add(new Label("Jens Gaebeler", skinLibgdx));
        table.row();
        table.add(new Label("Nils Kretschmer", skinLibgdx));
        table.row();
        table.add(new Label("Laszlo Pal", skinLibgdx));
        table.row();
        table.add(new Label("Jost Schmithals", skinLibgdx));
        table.row();
        table.add(new Label("", skinLibgdx));
        table.row();
        table.add(new Label("Grafiken", skinLibgdx, "default-font", Color.GREEN));
        table.row();
        table.add(new Label("fuer dieses Projekt erstellt von Tobias Schmithals", skinLibgdx));
        table.row();
        table.add(new Label("Fonts", skinLibgdx, "default-font", Color.GREEN));
        table.row();
        table.add(new Label("SharkSoftBites.ttf von www.dafont.com - lizenzfrei", skinLibgdx));
        table.row();
        table.add(new Label("Sounds", skinLibgdx, "default-font", Color.GREEN));
        table.row();
        table.add(new Label("von www.soundsboom.com - lizenzfrei", skinLibgdx));
        table.row();

        return table;

    }

    /**
     * Method to build/create a table for show the controls
     * @return a table
     */
    private Table buildShowGameInfo() {

        Table table = new Table();

        table.pad(0, 50, 0, 50);
        table.add(new Label("", skinLibgdx)).left();
        table.row();
        table.add(new Label("Putzi hat " + Constants.PUTZI_LIVES_START + " Leben und muss in insgesamt " +
                Constants.MAX_LEVEL_NUMBER + " Leveln allen Muell entfernen und gleichzeitig", skinLibgdx)).left();
        table.row();
        table.add(new Label("moeglichst viele Punkte sammeln. - Putzi sammelt alle Dinge nur mit dem Mund auf!", skinLibgdx)).left();
        table.row();
        table.add(new Label("", skinLibgdx)).left();
        table.row();
        table.add(new Label("Tastaturbelegung", skinLibgdx, "default-font", Color.GREEN)).colspan(3);
        table.row();
        table.add(new Label("                Putzi nach rechts . . . . . . D oder Rechtspfeil", skinLibgdx)).left();
        table.row();
        table.add(new Label("                Putzi nach links . . . . . . . . A oder Linkspfeil", skinLibgdx)).left();
        table.row();
        table.add(new Label("                Putzi nach oben . . . . . W oder Aufwaertspfeil", skinLibgdx)).left();
        table.row();
        table.add(new Label("                Putzi nach unten . . . . .  S oder Abwaertspfeil", skinLibgdx)).left();
        table.row();
        table.add(new Label("                Putzi diagonal . . . . . . . 2 Tasten gleichzeitig", skinLibgdx)).left();
        table.row();
        table.add(new Label("                Zusatzbeschleunigung, s.u. . . . . . .  Links-Shift", skinLibgdx)).left();
        table.row();
        table.add(new Label("                Spiel pausieren . . . . . . . . . . . . . . Escape", skinLibgdx)).left();
        table.row();
        table.add(new Label("                Zoom - nur zum Testen . . . . . . Komma/Punkt", skinLibgdx)).left();
        table.row();
        table.add(new Label("", skinLibgdx)).left();
        table.row();
        table.add(new Label("Regeln", skinLibgdx, "default-font", Color.GREEN)).colspan(3);
        table.row();
        table.add(new Label("Muell komplett entfernen . . . . . Uebergang zum naechsten Level, mit " + Constants.LEVEL_COMPLETED_BONUS + " Zusatzpunkten", skinLibgdx)).left();
        table.row();
        table.add(new Label("Muschel einsammeln . . . . . . . . . . . . . . . . . . . erhoeht den Score um " + Constants.SHELL_SCORE_INCREMENT + " Punkte", skinLibgdx)).left();
        table.row();
        table.add(new Label("Schnecke einsammeln . . . . . . . . . ermoeglicht fuer " + (int) Constants.POWER_BOOST_TIME + " Sekunden eine erhoehte Beschleunigung", skinLibgdx)).left();
        table.row();
        table.add(new Label("Krebs einsammeln . . . . . . . . ermoeglicht fuer " + (int) Constants.CRAB_COLLECTION_FREE_CAMERA_TIME + " Sekunden freie Kamerasuche - Putzi blau", skinLibgdx)).left();
        table.row();
        table.add(new Label("Anstoss an einen Felsen . . . . . . . . . . . . . . . . erniedrigt den Score nach und nach", skinLibgdx)).left();
        table.row();
        table.add(new Label("Biss durch einen Hai . . . . . . . . . . . kostet 1 Leben/" + (- Constants.SHARK_BITE_SCORE_DECREMENT) + " Punkte - Putzi kurz rot", skinLibgdx)).left();
        table.row();
        table.add(new Label("Beruehrung eines Haischwanzes . . . . . . . schuetzt vor dem naechsten Haibiss - Putzi gruen", skinLibgdx)).left();
        table.row();
        table.add(new Label("", skinLibgdx)).left();
        table.row();

        return table;

    }

    /**
     * Method to build/create table with buttons to save or cancel
     * the informations for the optionsfield
     * @return a layer with save/cancel-buttons
     */
    private Table buildOptWinButtons() {

        Table table = new Table();

        TextButton btnWinOptSave = new TextButton("Speichern", skinWindows);
        table.add(btnWinOptSave).padRight(15).width(100).height(45).pad(5,10,5,10);

        btnWinOptSave.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                attachedScreen.saveSettings();
                AudioManager.instance.play(Assets.instance.sounds.buttonOkClickedSound);
                attachedScreen.onCancelClicked();
            }
        });

        TextButton btnWinOptCancel = new TextButton("Abbrechen", skinWindows);
        table.add(btnWinOptCancel).padLeft(100).width(105).height(45);

        btnWinOptCancel.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.instance.play(Assets.instance.sounds.buttonCancelClickedSound);
                attachedScreen.onCancelClicked();
                attachedScreen.resetMusic();
            }

        });

        return table;

    }

    /**
     * Method to build/create table with buttons to return to the game menu screen
     * from the highscore layer
     * @return a layer with ok-button
     */
    private Table buildWinOKButtons() {

        Table table = new Table();
        table.row();

        TextButton btnOk = new TextButton("OK", skinWindows);
        table.add(btnOk).center().width(75).height(40).row();

        btnOk.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                attachedScreen.onOkClicked();
            }
        });

        table.row();
        return table;
    }

    /**
     * Method to build/create window-layer for setting the options
     * @return a window layer
     */
    Table buildOptionsWindowLayer() {

        Window winOptions = new Window("Einstellungen", skinWindows);
        winOptions.getTitleTable().pad(60, 10, 25, 0);
        winOptions.add(buildOptWinAudioSettings()).padTop(40).padBottom(20).row();
        winOptions.add(buildOptWinSkinSelection()).padBottom(20).row();
        winOptions.add(buildOptWinDebug()).padBottom(20).row();
        winOptions.add(buildOptWinButtons()).pad(20, 0, 10, 0);


        ButtonsManager.showOptionsWindow(false, false, winOptions);

        if (attachedScreen.getDebug()) {
            winOptions.debug();
        }

        winOptions.pack();

        winOptions.setPosition(Constants.VIEWPORT_GUI_WIDTH/2 - winOptions.getWidth()/2, Constants.VIEWPORT_GUI_HEIGHT/2-winOptions.getHeight()/2);
        windowMap.put("options", winOptions);

        return winOptions;

    }

    /**
     * Method to build/create window-layer for show the game-impressum
     * @return a window layer
     */
    Table buildImpressumWindowLayer() {

        Window winImpressum = new Window("Impressum", skinWindows);
        winImpressum.getTitleTable().pad(60, 10, 25, 0);
        winImpressum.add(buildShowImpressumText()).padTop(30).padBottom(20).row();
        winImpressum.add(buildWinOKButtons()).pad(30, 0, 10, 0);

        ButtonsManager.showOptionsWindow(false, false, winImpressum);

        if (attachedScreen.getDebug()) {
            winImpressum.debug();
        }

        winImpressum.pack();

        winImpressum.setPosition(Constants.VIEWPORT_GUI_WIDTH/2 - winImpressum.getWidth()/2, Constants.VIEWPORT_GUI_HEIGHT/2-winImpressum.getHeight()/2);
        windowMap.put("impressum", winImpressum);

        return winImpressum;

    }

    /**
     * Method to build/create window-layer for create a infofield with game-controlls
     * @return a window layer
     */
    Table buildGameInfoWindowLayer() {

        Window winGameInfo = new Window("Spielinfo", skinWindows);
        winGameInfo.getTitleTable().pad(60, 10, 25, 0);
        winGameInfo.add(buildShowGameInfo()).padTop(30).padBottom(0).row();

        winGameInfo.add(buildWinOKButtons()).pad(30, 0, 50, 0);

        ButtonsManager.showOptionsWindow(false, false,winGameInfo);

        if (attachedScreen.getDebug()) {
            winGameInfo.debug();
        }

        winGameInfo.pack();

        winGameInfo.setPosition(Constants.VIEWPORT_GUI_WIDTH/2 - winGameInfo.getWidth()/2, Constants.VIEWPORT_GUI_HEIGHT/2-winGameInfo.getHeight()/2);
        windowMap.put("gameinfo", winGameInfo);

        return winGameInfo;

    }

    /**
     * Method to build/create window-layer for show the list of highscores
     * @return a window layer
     */
    Table buildHighscoreWindowLayer() {

        Window winHighscore = new Window("Highscore", skinWindows);
        winHighscore.getTitleTable().pad(60, 10, 25, 0);

        winHighscore.add(buildHighscore()).pad(50,10,25,0).row();
        winHighscore.add(buildWinOKButtons()).pad(20, 0, 20, 0).row();

        ButtonsManager.showOptionsWindow(false, false,winHighscore);

        if (attachedScreen.getDebug()) {
            winHighscore.debug();
        }

        winHighscore.pack();
        winHighscore.setPosition(Constants.VIEWPORT_GUI_WIDTH/2 - winHighscore.getWidth()/2, Constants.VIEWPORT_GUI_HEIGHT/2-winHighscore.getHeight()/2);
        windowMap.put("highscores", winHighscore);

        return winHighscore;

    }
    /**
     * Method to build/create window-layer for show the list of highscores
     * the list is loading from the database, list size is max 10
     * @return a window layer
     */
    private Table buildHighscore(){

        //create a list with name and scores

        Table table = new Table();

        UserManagement usrMgmt = UserManagement.instance;

        Label nameLabel = new Label("Name: ", skinLibgdx);
        Label scoreLabel = new Label("Score: ", skinLibgdx);
        table.add(nameLabel).colspan(2).left();
        table.add(scoreLabel).colspan(2).right();
        table.row();
            for (int i = 0; i < usrMgmt.getUserNames().size(); i++) {
                // break out of loop, if too much players are in Database
                if (i > 9) {
                    break;
                }
                String username = usrMgmt.getUserNames().get(i);
                Label newPlayerLabel = new Label(username, skinLibgdx);
                Label newHighscoreLabel = new Label(String.valueOf(usrMgmt.getHighscore(username)), skinLibgdx);
                if (UserManagement.getUser() != null && username.equals(UserManagement.getUser())) {
                    newPlayerLabel.setColor(Color.GOLD);
                    newHighscoreLabel.setColor(Color.GOLD);
                }
                table.add(newPlayerLabel).colspan(2).left().padRight(50);
                table.add(newHighscoreLabel).colspan(2).right().padLeft(50).padRight(10);
                table.row();
            }

        return table;
    }

    /**
     * Get the label represented by its name.
     * @param labelNameKey The label's key as String
     * @return The label from a hashmap containing the labelname as key.
     */
    Label getLabel(String labelNameKey) {
        return labelMap.get(labelNameKey);
    }

    /**
     * Get the button represented by its name.
     * @param buttonNameKey The button's key as String
     * @return The button from a hashmap containing the buttonname as key.
     */
    Button getButton(String buttonNameKey) {
        return buttonMap.get(buttonNameKey);
    }

    /**
     * Get the Window represented by its name.
     * @param windowNameKey The window's key as String
     * @return The Window from a hashmap containing the windowname as key.
     */
    Window getWindow(String windowNameKey) {
        return windowMap.get(windowNameKey);
    }

    /**
     * Get the Textfield with the name as the key
     * @param textfieldNameKey The String representing the Textfield Key
     * @return The Textfield from a hashmap containing the Textfields name as key.
     */
    TextField getTextfield(String textfieldNameKey) {
        return tFieldMap.get(textfieldNameKey);
    }

    /**
     * Get the Checkbox with the name as the key
     * @param checkboxNameKey The String representing the Checkbox key
     * @return The Checkbox from a hashmap containing the CheckBoxes name as key.
     */
    CheckBox getCheckBox(String checkboxNameKey) {
        return chBoxMap.get(checkboxNameKey);
    }

    /**
     * Get the Slider with the name as the key
     * @param sliderNameKey The String representing the Sliders key
     * @return The Slider from a hashmap containing the Sliders name as key.
     */
    Slider getSlider(String sliderNameKey) {
        return sliderMap.get(sliderNameKey);
    }

    /**
     * Get the SelectBox for Skin selection of putzi
     * @return The SelectBox for the skin Selection in the options window.
     */
    SelectBox getCharSelectBox() {
        return selCharSkin;
    }

    /**
     * Get the Image from Skin Selection Option
     * @return the Image for the Character skin selection.
     */
    Image getCharSkin() {
        return imgCharSkin;
    }
}
