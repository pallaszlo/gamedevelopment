package com.putzi.screens;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.putzi.game.Assets;
import com.putzi.game.ExtendedGame;
import com.putzi.game.WorldController;
import com.putzi.screens.transitions.ScreenTransition;
import com.putzi.screens.transitions.ScreenTransitionFade;
import com.putzi.util.AudioManager;
import com.putzi.util.CharacterSkin;
import com.putzi.util.ButtonsManager;
import com.putzi.util.Constants;
import com.putzi.util.DatabaseConnector;
import com.putzi.util.GamePreferences;
import com.putzi.util.UserManagement;

import java.util.concurrent.TimeUnit;

/**
 * Class for menu screen
 * Provides methods to create stage, windows, buttons for new game, exit game, new/load user, options
 * Either from Database or from a local preferences file.
 */
public class MenuScreen extends AbstractGameScreen {

    private static final String TAG = MenuScreen.class.getName();
	private Stage stage;

	private boolean debugEnabled = false;
	private float debugRebuildStage;
	private Boolean ckhMusicOld;
	private Double oldVolMusic;

	private MenuScreenBuilder menuScrBuilder;

	/**constructor for menu screen class
	 * @param game instance of ExtendedGame
	 */
	public MenuScreen(ExtendedGame game) {
		super(game);
		menuScrBuilder = new MenuScreenBuilder(this);
	}

	/**
	 * Method called by the game loop from the application every time rendering should be performed.
	 * Game logic updates are usually also performed in this method.
	 * @param deltaTime time passed between last call
	 */
	@Override
	public void render(float deltaTime) {

		//feel screen with black color
		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		//if Live-Updates lifecycle on do rebuild
		if (debugEnabled) {
			debugRebuildStage -= deltaTime;
			if (debugRebuildStage <= 0) {
				debugRebuildStage = Constants.DEBUG_REBUILD_INTERVAL;
				rebuildStage();
			}
		}
		// update and draw stage
		stage.act(deltaTime);
		stage.draw();
	}

	/**
	 * This method is called every time the game screen is re-sized and the game is not in the paused state.	 *
	 * @param width is the new width
	 * @param height is the new height
	 */
	@Override
	public void resize(int width, int height) {

		//update views Viewports
		stage.getViewport().update(width, height, true);

	}

	/**
	 * Method to create an show the stage with the viewport width and heigth
	 * Called when this screen becomes the current screen for a Game.
	 */
	@Override
	public void show() {

		//create stage
		stage = new Stage(
				new StretchViewport(
						Constants.VIEWPORT_GUI_WIDTH,
						Constants.VIEWPORT_GUI_HEIGHT
				)
		);

		//rebuid stage
		rebuildStage();

	}

	/**
	 * Called when this screen is no longer the current screen for a Game
	 */
	@Override
	public void hide() {

		//Called when this screen should release all resources
		stage.dispose();
	}

	/**
	 * Called when the Application is paused, usually when it's not active or visible on screen
	 */
	@Override
	public void pause() {}

	/**
	 * Method to rebuild the stage in the menu screen
	 */
	private void rebuildStage() {

		//create layers for TableLayout
		Table layerBackground = menuScrBuilder.buildBackgroundLayer();
		Table layerObjects = menuScrBuilder.buildObjectsLayer();
		Table layerLogos = menuScrBuilder.buildLogosLayer();
		Table layerControls = menuScrBuilder.buildControlsLayer();
		Table layerInfos = menuScrBuilder.buildSaveLayer();
		Table layerLoadInfos = menuScrBuilder.buildLoadLayer();
		Table layerOptionsWindow = menuScrBuilder.buildOptionsWindowLayer();
		Table layerImpressumWindow = menuScrBuilder.buildImpressumWindowLayer();
		Table layerHighscoreWindow = menuScrBuilder.buildHighscoreWindowLayer();
		Table layerGameInfoWindow = menuScrBuilder.buildGameInfoWindowLayer();
		Table layerNewPlayerWindow = menuScrBuilder.buildNewPlayerWindowLayer();
		Table layerLoadPlayerWindow = menuScrBuilder.buildLoadPlayerWindowLayer();
		Table layerLoginControls = menuScrBuilder.buildLoginControlsLayer();


		// create stage with two nodes: stack for layers and stacks for optionsfields
		stage.clear();
		Stack stack = new Stack();
		stack.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
		stack.add(layerBackground);
		stack.add(layerObjects);
		stack.add(layerLogos);
		stack.add(layerControls);
		stack.add(layerLoginControls);
		stack.add(layerInfos);
		stack.add(layerLoadInfos);
		stage.addActor(stack);
		stage.addActor(layerOptionsWindow);
		stage.addActor(layerImpressumWindow);
		stage.addActor(layerHighscoreWindow);
		stage.addActor(layerGameInfoWindow);
		stage.addActor(layerNewPlayerWindow);
		stage.addActor(layerLoadPlayerWindow);
	}

	/**
	 * Listener for load-button in load user window.
	 * Load user from the Database if conditions are met
	 * @param username the users name from the database
	 */
	void onLoadClicked(String username) {
		Gdx.app.log("Action", "Trying to load profile from " + username);
		boolean connected = DatabaseConnector.instance.isConnected();
		if (connected && UserManagement.instance.userAlreadyInDB(username)) {
		    UserManagement.setUser(username);
			AudioManager.instance.play(Assets.instance.sounds.buttonOkClickedSound);
			menuScrBuilder.getLabel("load").setVisible(true);
			ScreenTransition transition = ScreenTransitionFade.init(0.75f);
			game.setScreen(new com.putzi.screens.MenuScreen(game), transition);
			// Thread for loading Preferences from Database
			new Thread() {
				@Override
				public void run() {
					GamePreferences.instance.setPreferencesFromDatabase();
					AudioManager.instance.onSettingsUpdated();
					Gdx.app.log("Action", "Profile from " + UserManagement.getUser() +
							" successfully loaded!");
					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						Gdx.app.log(TAG, Constants.INTERRUPTED_EXCEPTION_ERROR + e);
					}
					menuScrBuilder.getLabel("load").setVisible(false);
				}
			}.start();

		} else if (!connected){
			menuScrBuilder.getLabel("loaderror").setText("Keine Serververbindung...");
			AudioManager.instance.play(Assets.instance.sounds.buttonCancelClickedSound);
		} else {
            menuScrBuilder.getLabel("loaderror").setText("Name nicht gefunden");
			AudioManager.instance.play(Assets.instance.sounds.buttonCancelClickedSound);
		}
	}

	/**
	 * Listener for save-button in new user window.
	 * Creates user in the Database if conditions are met
	 * @param username The new users name in the Database
	 */
	void onSaveClicked(final String username) {

		if (username.length() < 4 ||
				username.length() > 10) {
			AudioManager.instance.play(Assets.instance.sounds.buttonCancelClickedSound);
			menuScrBuilder.getLabel("error").setText("Bedingungen einhalten!");
		} else if (!DatabaseConnector.instance.isConnected()) {
			AudioManager.instance.play(Assets.instance.sounds.buttonCancelClickedSound);
            menuScrBuilder.getLabel("error").setText("Keine Serververbindung...");
		} else if (UserManagement.instance.userAlreadyInDB(username)){
			AudioManager.instance.play(Assets.instance.sounds.buttonCancelClickedSound);
            menuScrBuilder.getLabel("error").setText("Name schon vergeben");
		} else {
			// Thread for Database Connection
			AudioManager.instance.play(Assets.instance.sounds.buttonOkClickedSound);
			new Thread() {
				@Override
				public void run() {
					menuScrBuilder.getLabel("save").setVisible(true);
					UserManagement.instance.createUserInDatabase(username);
					GamePreferences.instance.setPreferencesFromDatabase();
                    AudioManager.instance.onSettingsUpdated();
					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						Gdx.app.log(TAG, Constants.INTERRUPTED_EXCEPTION_ERROR + e);
					}
                    menuScrBuilder.getLabel("save").setVisible(false);
				}
			}.start();

			ScreenTransition transition = ScreenTransitionFade.init(0.75f);
			game.setScreen(new com.putzi.screens.MenuScreen(game), transition);
		}
	}

	/**
	 * Listener for play-button in the controls-layer.
	 * Change the screen to game screen.
	 * Set game is running
	 */
	void onPlayClicked() {

		ScreenTransition transition = ScreenTransitionFade.init(0.75f);
		game.setScreen(new com.putzi.screens.GameScreen(game), transition);
		AudioManager.instance.play(Assets.instance.sounds.gameStartUpSound);
		WorldController.setGameIsRunning(true);

	}


	/**
	 * Listener new play-button in the new player window-layer and for load-button in the load player window-layer.
	 * Hide menu buttons and show load player-window.
	 * Set focus for the textfield
	 */
	void onLoginPlayerClicked() {
	    new Thread() {
	        @Override
            public void run() {
                if (!DatabaseConnector.instance.isConnected()) {
                    DatabaseConnector.instance.connectToMongoDB();
            }
        }
		}.start();
		showMenuButtons(false);
		if(menuScrBuilder.getButton("newplayer").isPressed()){
            menuScrBuilder.getLabel("error").setText("");
            menuScrBuilder.getTextfield("playername").setText("");
			ButtonsManager.showOptionsWindow(true, true, menuScrBuilder.getWindow("newplayer"));
			stage.setKeyboardFocus(menuScrBuilder.getTextfield("playername"));
		}else{
            menuScrBuilder.getLabel("loaderror").setText("");
            menuScrBuilder.getTextfield("loadplayer").setText("");
			ButtonsManager.showOptionsWindow(true, true, menuScrBuilder.getWindow("loadplayer"));
			stage.setKeyboardFocus(menuScrBuilder.getTextfield("loadplayer"));
		}

	}

	/**
	 * Listener for option-button in the controls-layer.
	 * Hide menu buttons and show options-window.
	 */
	void onOptionsClicked() {
		loadSettings();
		showMenuButtons(false);
		ButtonsManager.showOptionsWindow(true, true, menuScrBuilder.getWindow("options"));
	}

	/**
	 * Listener for option-button in the show impressum-layer.
	 * Hide menu buttons and show impressum-window.
	 */
	void onImpressumClicked(){
		showMenuButtons(false);
		ButtonsManager.showOptionsWindow(true, true, menuScrBuilder.getWindow("impressum"));
	}

	/**
	 * Listener for option-button in the show game info-layer.
	 * Hide menu buttons and show gameinfo-window.
	 */
	void onGameInfoClicked(){
		showMenuButtons(false);
		ButtonsManager.showOptionsWindow(true, true, menuScrBuilder.getWindow("gameinfo"));
	}

	/**
	 * Listener for option-button in the show highscore-layer.
	 * Hide menu buttons and show highscore-window.
	 */
	void onHighscoreClicked(){
		showMenuButtons(false);
		ButtonsManager.showOptionsWindow(true, true, menuScrBuilder.getWindow("highscores"));
	}

	/**
	 * Listener for cancel-button in the options-layer.
	 * Show menu buttons and hide the windows.
	 * setting for audio manager is updated
	 */
	void onCancelClicked() {

		showMenuButtons(true);
		ButtonsManager.showOptionsWindow(false, true, menuScrBuilder.getWindow("options"));
		ButtonsManager.showOptionsWindow(false, true, menuScrBuilder.getWindow("newplayer"));
		ButtonsManager.showOptionsWindow(false, true, menuScrBuilder.getWindow("loadplayer"));
		AudioManager.instance.onSettingsUpdated();

	}

	/**
	 * Listener for ok-button in the options-layer.
	 * Show menu buttons and hide the windows.
	 * set the audio for clicked ok button
	 */
	void onOkClicked() {

		showMenuButtons(true);
        ButtonsManager.showOptionsWindow(false, true, menuScrBuilder.getWindow("impressum"));
		ButtonsManager.showOptionsWindow(false, true, menuScrBuilder.getWindow("gameinfo"));
		ButtonsManager.showOptionsWindow(false, true, menuScrBuilder.getWindow("highscores"));
		AudioManager.instance.play(Assets.instance.sounds.buttonOkClickedSound);

	}
	/**
	 * Method to select a skincolor for Putzi
	 * @param index for the selected skin
	 */
	void onCharSkinSelected(int index) {

		CharacterSkin skin = CharacterSkin.values()[index];
		menuScrBuilder.getCharSkin().setColor(skin.getColor());

	}

	/**
	 * Loads settings from either Database or local preferences file
	 * Database Options are priorized
	 */
	private void loadSettings() {

		// Database Test
		DatabaseConnector dbConnector = DatabaseConnector.instance;
		menuScrBuilder.getLabel("load").setVisible(true);
		if (dbConnector.isConnected() && null != UserManagement.getUser()) {
			// Thread for loading the Settings from Database
			new Thread() {
				@Override
				public void run() {
					UserManagement usrMgmt = UserManagement.instance;
					menuScrBuilder.getCheckBox("sound").setChecked((Boolean) usrMgmt.getPreference("sound"));
					Double dVolSound = (Double) usrMgmt.getPreference("volSound");
					menuScrBuilder.getSlider("sound").setValue(dVolSound.floatValue());
					ckhMusicOld = (Boolean) usrMgmt.getPreference("music");
					menuScrBuilder.getCheckBox("music").setChecked(ckhMusicOld);
					oldVolMusic = (Double) usrMgmt.getPreference("volMusic");
					menuScrBuilder.getSlider("music").setValue(oldVolMusic.floatValue());
					menuScrBuilder.getCharSelectBox().setSelectedIndex((Integer) usrMgmt.getPreference("charSkin"));
					onCharSkinSelected((Integer) usrMgmt.getPreference("charSkin"));
					menuScrBuilder.getCheckBox("fps").setChecked((Boolean) usrMgmt.getPreference("showFpsCounter"));
					menuScrBuilder.getCheckBox("shader").setChecked((Boolean) usrMgmt.getPreference("useMonochromeShader"));
					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						Gdx.app.log(TAG, Constants.INTERRUPTED_EXCEPTION_ERROR + e);
					}
					menuScrBuilder.getLabel("load").setVisible(false);
				}
			}.start();
		} else {
			GamePreferences prefs = GamePreferences.instance;

			menuScrBuilder.getCheckBox("sound").setChecked(prefs.sound);
            menuScrBuilder.getSlider("sound").setValue((float) prefs.volSound);
			menuScrBuilder.getCheckBox("music").setChecked(prefs.music);
			ckhMusicOld = prefs.music;
            menuScrBuilder.getSlider("music").setValue((float) prefs.volMusic);
			oldVolMusic = prefs.volMusic;
            menuScrBuilder.getCharSelectBox().setSelectedIndex(prefs.charSkin);
			onCharSkinSelected(prefs.charSkin);
			menuScrBuilder.getCheckBox("fps").setChecked(prefs.showFpsCounter);
			menuScrBuilder.getCheckBox("shader").setChecked(prefs.useMonochromeShader);
            menuScrBuilder.getLabel("load").setVisible(false);
		}
	}

	/**
	 * Save settings from either Database or local preferences file
	 */
	void saveSettings() {
        menuScrBuilder.getLabel("save").setVisible(true);
		GamePreferences prefs = GamePreferences.instance;

		prefs.sound = menuScrBuilder.getCheckBox("sound").isChecked();
		prefs.volSound = menuScrBuilder.getSlider("sound").getValue();
		prefs.music = menuScrBuilder.getCheckBox("music").isChecked();
		prefs.volMusic = menuScrBuilder.getSlider("music").getValue();
		prefs.charSkin = menuScrBuilder.getCharSelectBox().getSelectedIndex();
		prefs.showFpsCounter = menuScrBuilder.getCheckBox("fps").isChecked();
		prefs.useMonochromeShader = menuScrBuilder.getCheckBox("shader").isChecked();

		//create and start a Thread for saving the preferences in DB and local
		new Thread() {
			@Override
			public void run() {
				GamePreferences.instance.savePreferences();
				try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e) {
                    Gdx.app.log(TAG, Constants.INTERRUPTED_EXCEPTION_ERROR + e);
				}
                menuScrBuilder.getLabel("save").setVisible(false);
			}
		}.start();
	}

	/**
	 * Helper method to reset music options when cancel is clicked
	 */
	void resetMusic() {
		menuScrBuilder.getCheckBox("music").setChecked(ckhMusicOld);
        menuScrBuilder.getSlider("music").setValue(oldVolMusic.floatValue());
		AudioManager.instance.onSettingsUpdated();
	}

    /**
     * Method to show/animate the control buttons and the label for the user name
     * @param visible is true when visibility is set
     */

	private void showMenuButtons(boolean visible) {

		float moveDuration = 1.0f;
		Interpolation moveEasing = Interpolation.swing;
		float delayOptionsButton = 0.25f;

		float moveX = 0.0f * (visible ? -1 : 1);
		float moveY = -1000.0f * (visible ? -1 : 1);

		float moveXlogin = -700.0f * (visible ? -1 : 1);
		float moveYlogin = 0.0f * (visible ? -1 : 1);

				//label for the user name
		if (null != UserManagement.getUser()) {
			menuScrBuilder.getLabel("playername").addAction(sequence(
					delay(delayOptionsButton),
					moveBy(
							moveX,
							moveY,
							moveDuration,
							moveEasing
					)
			));

		}

		//button for login as new player
		menuScrBuilder.getButton("newplayer").addAction(sequence(
				delay(delayOptionsButton),
				moveBy(
						moveXlogin,
						moveYlogin,
						moveDuration,
						moveEasing
				)
		));
		//button for load a player
		menuScrBuilder.getButton("loadplayer").addAction(
				moveBy(
						moveXlogin,
						moveYlogin,
						moveDuration,
						moveEasing
				)
		);

		//buttons for gameplay
		menuScrBuilder.getButton("menuplay").addAction(
				moveBy(
						moveX,
						moveY,
						moveDuration,
						moveEasing
				)
		);

		//button for show the option menu
		menuScrBuilder.getButton("menuoptions").addAction(
				sequence(
						delay(delayOptionsButton),
						moveBy(
								moveX,
								moveY,
								moveDuration,
								moveEasing
						)
				)
		);
		//button for show impressum window
		menuScrBuilder.getButton("impressum").addAction(
				moveBy(
						moveX,
						moveY,
						moveDuration,
						moveEasing
				)
		);
		//button for show highscore window
		if (null != UserManagement.getUser()) {
			menuScrBuilder.getButton("highscores").addAction(
					moveBy(
							moveX,
							moveY,
							moveDuration,
							moveEasing
					)
			);
		}
		//button for show game info window
		menuScrBuilder.getButton("gameinfo").addAction(
				sequence(
						delay(delayOptionsButton),
						moveBy(
								moveX,
								moveY,
								moveDuration,
								moveEasing
						)
				)
		);
		//button for exit game
		menuScrBuilder.getButton("exitgame").addAction(
				moveBy(
						moveX,
						moveY,
						moveDuration,
						moveEasing
				)
		);

		SequenceAction seq = sequence();

		if (visible) {
			seq.addAction(
					delay(
							delayOptionsButton + moveDuration
					)
			);
		}

		stage.addAction(seq);

	}

    /**
     * If set to true the debug mode is enabled which results in additional developer information.
     * @param mode true if debugmode should be enabled. False if not.
     */
	public void setDebug(boolean mode) {
	    debugEnabled = mode;
    }

    /**
     * Debug mode results in extra output for development.
     * @return True if debugging mode is enabled. False if not.
     */
    public boolean getDebug() {
	    return debugEnabled;
    }

	/**
	 * Method to set the stage as input listener
	 * @return a stage for inputs
	 */
	@Override
	public InputProcessor getInputProcessor() {

		return stage;

	}
}
